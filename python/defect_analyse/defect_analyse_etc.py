#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 10:31:22 2020

@author: hindmars
"""

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import pandas as pd
from copy import deepcopy

from . import defect_analyse_helper as dah
from . import defect_analyse_stats as das

"""
A set of classes and functions for analysing data from Abelian Higgs code LAH and its 
descendants, including ON.  
Developed from original Matlab scripts written by Neil Bevis and others.

Examples of use:

    import defect_analyse as da
    id2kell10 = 'lp10_2k'
    params2kell10 = ('_' + id2kell10 + '-{:03d}'.format(1),  'nf2/', ''st_' + id2kell10 + '.sf')
    ep = da.ETCFrame('tensor', *params2kell10)
    ep.plot([275,550], 't', ylim=(1e-6,100), fig_save_suffix='_foo.pdf')    

        
Plot xi:
    eah.st.plot_xi()

"""

cmap_default = mpl.colors.ListedColormap(['k'] + ['y']*10 + ['b'])

def alpha_map_old(x, thresh=3e-2, alpha_low=0.5, alpha_high=1.0):
    if x<thresh or x>1.0-thresh:
        alpha = alpha_high
    else:
        alpha = alpha_low
    return alpha

def alpha_map_shade(x, alpha_low=0.5, alpha_high=1.0):
    return alpha_low + (alpha_high - alpha_low)*x

def alpha_map_default(x):
    return alpha_map_shade(x, 1.0, 1.0)

def zorder_default(x, thresh=3e-2):
    if x < thresh :
        zorder = 1.0
    elif x > 1.0-thresh:
        zorder = 0.0
    else:
        zorder = -1.0
    return zorder

def get_kwarg(dic, name, default=""):
    """
    Search a dictionary for a key "name".  If it exists, return the contents of 
    the dictionary.  If it does not exist, return the default string. The dictionary 
    is assumed to be keyword arguemnts to a funciton, and the contents strings.

    Parameters
    ----------
    dic : dictionary
        Dictionary, assumed to be keyword arguments supplied to a function.
    name : string
        The keyword for the dictionary entry.
    default : string, optional
        What to return if the keyword is not found. The default is "".

    Returns
    -------
    var : string (assumed)
        Contents of dictionary whose key is "name".

    """
    if name in dic:
        var = dic[name]
    else:
        var = default
    return var


def look_for_data(text, key, position=-1):
    """
    Search a string "text" consisting of a number of lines for a substring "key", 
    split the line by spaces, and return a float converted from the resulting 
    list at index "position".  The default position is the last (-1).
    
    The intention is to scan a simulation log file for numerical output preceded 
    by a key word.

    Parameters
    ----------
    text : string
        Text for searching.
    key : string
        String to search for in text.
    position : integer, optional
        Where to split the text. The default is -1.

    Returns
    -------
    float
        The number found in the text.

    """
    data = []
    for line in text:
        if key in line:
            data.append(float(line.split()[position]))
    return np.array(data)


class ETCFrame:
    """
    Equal Time Correlator object.  Contains ETCs or UETCs for mutiple runs with same 
    parameters and different random seeds,  and functions for interacting with them.
    
    """
    
    
    def __init__(self, *args, **kwargs): 
        """
        Initialise an ETCFrame instance
        
        Uses *args and **kawrgs so that input parameters can be either:
            
       Parameters
        ----------
        corr_type : string
            ETC type: scalar11, scalar12, scalar22, vector, tensor.
        run_id_str : string
            Identifier string for run of set of runs.
        path_data : string
            Path to data file tree. Data file assumed to be here.
        settings : string or dict
            Path to settings file, or a dict containing  
                N, dx, sigma, coreGrowthIndexB, tUETCreference

        OR
        
        An ETCFrame instance, so that a copy method can be executed.
        
        OR
        
        A set of three objects,
        
        etc : pandas.DataFrame
            ETC data and anciliary.
        k_df : pandas.Series
            wavenumber values.
        st : StatsFrame
            Global data in statsfile(s).

        kwargs
        
        runs: n-sequence
            run numbers, can be used to construct file names with formatting strings.
        
        add_path_to_etc: string
            concatenate to path_data to get to etc data
            
        data_id_str: string
            overrides run_id_str to give a DataFrame label to data from a run     
        
        add_path_to_stats: string
            concatenate to path_data to get to stats file

        Returns
        -------
        None.
        
        """
        
        if isinstance(args[0], ETCFrame):
            self.etcf_copy(args[0], *args[1:], **kwargs)
            return
        elif isinstance(args[0], pd.DataFrame) and isinstance(args[1], pd.Series) and isinstance(args[2], das.StatsFrame):
            self.construct_from_dataframes(args[0], *args[1:], **kwargs)
            return
        elif len(args) == 4:
            corr_type, id_str, path_data, settings = args
            if "runs" in kwargs and kwargs['runs'] is not None:
                if '{:' not in id_str and '{:' not in path_data:
                    raise ValueError("ETCFrame: error: multiple runs need " + 
                                     "format specification in id_str or path_data.\n")
                if len(kwargs['runs']) == 0:
                    raise ValueError("ETCFrame: error: no runs given in run list.\n")
                    
                for n, run in enumerate(kwargs["runs"]):
                    kwargs_tmp = kwargs.copy()
                    del kwargs_tmp["runs"]
                    id_this_run = id_str.format(run)
                    # print(id_this_run)
                    if type(settings) == str:
                        sf = settings.format(run)
                    else:
                        sf = settings
                    path_data_this_run = path_data.format(run)
                    if "path_to_log" in kwargs_tmp:
                        if isinstance(kwargs_tmp["path_to_log"], str):
                            kwargs_tmp["path_to_log"] = kwargs_tmp["path_to_log"].format(run)
                        elif isinstance(kwargs_tmp["path_to_log"], list): # Assume it's a list or array
                            kwargs_tmp["path_to_log"] = kwargs_tmp["path_to_log"][n]
                            
                    if "path_to_stats" in kwargs_tmp:
                        kwargs_tmp["path_to_stats"] = kwargs_tmp["path_to_stats"].format(run)
                    if "data_id_str" in kwargs_tmp:
                        kwargs_tmp["data_id_str"] = kwargs_tmp["data_id_str"].format(run)        
                    if run == kwargs["runs"][0]:
                        # self.construct_from_etc_data_file(ids, sf, path)
                        tmp_efr = ETCFrame(corr_type, id_this_run, path_data_this_run, sf, **kwargs_tmp)
                    else: 
                        tmp_efr = tmp_efr.etcf_append(ETCFrame(corr_type, id_this_run, path_data_this_run, sf, **kwargs_tmp))
                    # print(tmp_efr.etc.loc[tmp_efr.etc['id_str'] == id_this_run])
                self.__init__(tmp_efr)
            else:
                self.construct_from_data_files(corr_type, id_str, path_data, settings, **kwargs)
        else:
            raise TypeError("ETCFrame: error: constructor arguments not recognised.\n")
        return


    def construct_from_data_files(self, corr_type, run_id_str, path_data, settings, **kwargs):
        """
        Load etc data and statsfile data to construct an ETCFrame object.

        Parameters
        ----------
        corr_type : string
            correlator type: scalar11, scalar12, scalar22, vector, tensor.
        run_id_str : string
            Identifier string for run of set of runs.
        path_data : string
            Path to data file tree. Data file assumed to be here.
        settings : string or dict
            Path to settings file.

        kwargs
        
        field_type : string
            ETC | UETC | hTTplus | hTTcross | hdotTTplus | hdotTTcross 
        
        add_path_to_etc: string
            concatenate to path_data to get to etc data
            
        data_id_str: string
            overrides run_id_str to give a DataFrame label to data from a run     
        
        add_path_to_stats: string
            concatenate to path_data to get to stats file

        Returns
        -------
        None.

        """
        
# Perhaps need base_path, log_rel_path, settings_rel_path, stats_rel_path, etc_rel_path
# Relative paths can be obtained from the settings file.
        
        if type(settings) == str:
            sf = dah.read_settings_file(path_data + settings)
        else:
            sf = settings
                
        field_type = get_kwarg(kwargs, "field_type", "ETC")
        # Need Filename generator based on field_type, corr_type and sim_type

        # rel_path_etc = ""
        # if "pathRoot" in sf:
        #     if "pathUETC" in sf:
        #         rel_path_etc = sf["pathRoot"] + sf["pathUETC"]
    
        pathRoot = dah.get_kwarg(sf, 'pathRoot', '')    
        pathStats = dah.get_kwarg(sf, 'pathStats', '')    
        add_path_to_etc = dah.get_kwarg(kwargs, 'add_path_to_etc', 
                                       pathRoot + pathStats)
        # add_path_to_etc = get_kwarg(kwargs, "add_path_to_etc", rel_path_etc)
        etc_name_postfix = get_kwarg(kwargs, "etc_name_postfix")
        etc_name_prefix = get_kwarg(kwargs, "etc_name_prefix")

        if "TT" in field_type:
            etc_file_type = get_kwarg(kwargs, "etc_file_type", ".txt")
            path_to_etc = path_data + add_path_to_etc + field_type \
                + run_id_str + corr_type + etc_name_postfix + etc_file_type
        # elif field_type == "UETC":
        #     etc_name_prefix = get_kwarg(kwargs, "etc_name_prefix")
        #     etc_file_type = get_kwarg(kwargs, "etc_file_type", ".dat")
        #     path_to_etc = path_data + add_path_to_etc + etc_name_prefix + field_type + corr_type \
        #         + run_id_str + etc_name_postfix + etc_file_type                    
        else:
            etc_file_type = get_kwarg(kwargs, "etc_file_type", ".dat")
            path_to_etc = path_data + add_path_to_etc + etc_name_prefix + field_type + corr_type \
                + run_id_str + etc_name_postfix + etc_file_type
            if get_kwarg(kwargs,'alt_name_order', False):
                path_to_etc = path_data + add_path_to_etc + etc_name_prefix + run_id_str + field_type \
                    + corr_type + etc_name_postfix + etc_file_type

        # self.etc = pd.DataFrame(np.loadtxt(path_to_etc))
        # rows, cols = self.etc.shape
        # print( 'ETCFrame: loaded data from ' + path_to_etc)
        # print(f'     {rows:} rows')

        # self.etc['id_str'] = get_kwarg(kwargs, "data_id_str", run_id_str)
        # print(f'     id_str = {self.etc["id_str"].unique()[0]:}')
        
        # self.etc['tUETCreference'] = sf['tUETCreference']

        path_to_log  = path_data + get_kwarg(kwargs, "path_to_log")

        # with open(path_to_log) as f:
        #     text = f.readlines()
        #     print("     Read log file", path_to_log, len(text), "lines")
            
        
        if "TT" in field_type:
            info_name_prefix = get_kwarg(kwargs, "info_name_prefix", field_type)
            info_name_postfix = get_kwarg(kwargs, "info_name_postfix","kMean")
            info_file_type = get_kwarg(kwargs, "info_file_type", ".txt")
        else:
            info_name_prefix = get_kwarg(kwargs, "info_name_prefix", "info")
            info_name_postfix = get_kwarg(kwargs, "info_name_postfix")
            info_file_type = get_kwarg(kwargs, "info_file_type", ".dat")


        path_to_info = path_data + add_path_to_etc + info_name_prefix + run_id_str + \
            info_name_postfix + info_file_type
                
        # if len(info_name_postfix) == 0:
        #     # Probabably LAH, ON and descendents
        #     kt_ref = np.loadtxt(path_to_info, skiprows=3, max_rows=1)
        #     print("     Reading k data from", path_to_info)
        #     t_uetc_write = look_for_data(text, "Writing UETC") # The actual writing times
        #     if len(t_uetc_write) == 0:
        #         t_uetc_write = np.array([sf['tUETCreference']])
        #     t_uetc_info = pd.DataFrame(np.loadtxt(path_to_info, skiprows=7, max_rows=1) * t_uetc_write[0],
        #                                    columns=['t_uetc'])
        #     # print("***", t_uetc_write.shape, t_uetc_info.shape, self.etc.shape)
        #     # self.etc['t_uetc'] = t_uetc_write
        #     self.etc = self.etc.join(t_uetc_info)
        #     # print(self.etc)
        #     self.k_df = pd.Series(kt_ref/self.etc['t_uetc'][0] )
        # elif info_name_postfix == "kMean":
        #     # Probably a GW PS file from GWmodule
        #     k_mean = np.loadtxt(path_to_info)
        #     print("     Reading k data from", path_to_info)
        #     self.k_df = pd.Series(k_mean)
        #     self.etc['t_uetc'] = look_for_data(text, "Writing GW")
        #     print("     read t_uetc", len(self.etc['t_uetc']))
        
        # add_path_to_stats = get_kwarg(kwargs, "add_path_to_stats")        

        sim_code = dah.get_kwarg(kwargs, 'sim_code', 'on')
        corr, k_mean, t_uetc_write = dah.get_corr_data(corr_type, field_type, 
                                           path_to_etc, path_to_info, path_to_log,
                                           sim_code)

        # if sim_code in dah.hila_series:
        #     corr *= (sf['N']*sf['dx'])**3
        
        self.etc = pd.DataFrame(corr)
        rows, cols = self.etc.shape
        print( 'ETCFrame: loaded correlator data from ' + path_to_etc)
        print(f'     {rows:} rows')
        print( '          time data length: ')
        print(f'     {len(t_uetc_write):} rows')

        self.etc['id_str'] = get_kwarg(kwargs, "data_id_str", run_id_str)
        print(f'     id_str = {self.etc["id_str"].unique()[0]:}')

        
        if sim_code in dah.hila_series:
            k_mean /= sf['dx']
        self.k_df = pd.Series(k_mean)
        

        self.etc['tUETCreference'] = sf['tUETCreference']

        if t_uetc_write[0] == 1.0: # probably hasn't found UETC write times
            print('Correcting t_uetc_write, multiplying by tUETCreference', sf['tUETCreference'])
            t_uetc_write *= sf['tUETCreference']
            print('Correcting k_mean, dividing by tUETCreference', sf['tUETCreference'])
            k_mean /= sf['tUETCreference']
        # Quite possible that the job ended before the last UETC was written
        self.etc['t_uetc'] = t_uetc_write[:rows]
        if rows != len(t_uetc_write):
            print('warning: number of correlator rows does not match number of times '
                  +  '          in record of correlator writes (log file or info file)')

        
        self.st = das.StatsFrame(run_id_str, path_data, settings, **kwargs)

        t = self.st.t
        a = self.st.a

        if 'SL_r' in self.st:
            len_str = self.st.SL_r
            xi = self.st.xi_r
            self.xi_measure = 'xi_r'
        else:
            len_str = self.st.SLlag
            xi = self.st.xiLag
            self.xi_measure = 'xiLag'            
            
        ws = self.st.ws
        
        # if "TT" in corr_type:
        #     field_type = ""
        self.etc['field_type'] = field_type
        self.etc['corr_type'] = corr_type
        

        self.etc['ws_interp'] = np.interp(self.etc.t_uetc, t , ws)
        self.etc['len_str_interp'] = np.interp(self.etc.t_uetc, t , len_str)
        self.etc['xi_interp'] = np.interp(self.etc.t_uetc, t , xi)
        self.etc['a_interp'] = np.interp(self.etc.t_uetc, t , a)
        
        self.add_total_power()
        
        print(f'ETCFrame: initialised with {len(self.k_df):} wavenumbers.')        
        print( '     Wavenumber Series at ETCFrame.k_df')        
        print( '     Mean string separation estimated with ' + self.xi_measure)        
        print( '     id_str                                ' + self.etc['id_str'].unique()[0])        

        return



    def construct_from_dataframes(self, etc, k_df, st):
        """
        Construct a new ETCFrame instance from data. 

        Parameters
        ----------
        etc : pandas.DataFrame
            ETC data and anciliary.
        k_df : pandas.Series
            wavenumber values.
        st : das.StatsFrame
            Global data in statsfile(s).

        Returns
        -------
        None.

        """
        self.etc = etc.copy()
        self.k_df = k_df.copy()
        self.st = das.StatsFrame(st.copy())
        return


    def etcf_copy(self, orig, *args, **kwargs):
        """
        Copy an ETCFrame instance.
        
        deepcopy idea from
         https://stackoverflow.com/questions/1241148/copy-constructor-in-python


        Parameters
        ----------
        orig : ETCFrame
            ETCFrame object to be copied.
        *args : TYPE
            Not used.
        **kwargs : TYPE
            Not used.

        Returns
        -------
        None.

        """
        for k, v in orig.__dict__.items():
            # if hasattr(orig_type, k) and hasattr(self, k) and getattr(orig_type, k) == getattr(self, k):
            #     continue
            setattr(self, k, deepcopy(v))
            
        self.st = das.StatsFrame(self.st)
        return


    def etcf_append(self, other):
        """
        
        Appends one ETCFrame to calling ETCFrame.  They must have the same wavenumber series, and they can't 
        have the same ID string id_str'
        
        Append means the etc and stats DataFrames are appended, and the same wavenumber series is used.
        
        Returns a new ETCFrame.

        """
        # if not self.k_df.equals(other.k_df):


        if len(self.k_df) != len(other.k_df):
            raise ValueError("etcf_append: error: cannot append ETC objects with different wavenumber series")
        else:            
            diff = self.k_df - other.k_df
            aver = 0.5*(self.k_df + other.k_df)

            if np.max(np.abs(diff/aver)) > 1e-3:       
                raise ValueError("etcf_append: error: wavenumber series are the same length but differ by more than 0.1 %")
                # print(self.k_df[0:5], self.k_df[-6:-1])
                # print(other.k_df[0:5], other.k_df[-6:-1])

        
        if self.etc['id_str'].equals(other.etc['id_str']) and self.etc['corr_type'].equals(other.etc['corr_type']):
            raise ValueError("etcf_append: error: cannot append ETC objects with same id_str and corr_type")
        
        new_etc = pd.concat((self.etc, other.etc))
        new_st = self.st.sfr_append(other.st)
        # print(type(new_etc))
        # print(type(new_st))
        return ETCFrame(new_etc, self.k_df, new_st)


    def etc_groupby(self, key):
        """
        Returns a tuple of the ETC DataFrame and the Stats dataframe selected by groupby(key).
        """
        return self.etc.groupby(key), self.st.groupby(key) 


    def get_by_id(self, id_str):
        
        return ETCFrame(self.etc.loc[self.etc['id_str']==id_str], self.k_df,
                                     das.StatsFrame(self.st.loc[self.st['id_str']==id_str]) )


    def etcf_mean(self):
        """
        
        Returns
        -------
        ETCFrame
            Contains mean of numerical data. id_str is concatenated.

        """
        
        etc_mean = self.etc.groupby(self.etc.index).mean(numeric_only=True)
        st_mean = self.st.groupby(self.st.index).mean(numeric_only=True)
        
        # concat_str = ''.join(self.etc.groupby('id_str').groups.keys())
        concat_str = self.st._get_id_for_title()
        
        df = pd.DataFrame(index=etc_mean.index, 
                          columns=['id_str', 'field_type', 'corr_type'])
        df['id_str'] = concat_str
        df['field_type'] = self.etc['field_type'].unique()[0]
        df['corr_type'] = self.etc['corr_type'].unique()[0]

        etc_mean = pd.concat((etc_mean,df), axis=1)

        # etc_mean['id_str'] = concat_str
        # etc_mean['field_type'] = self.etc['field_type'].unique()[0]
        # etc_mean['corr_type'] = self.etc['corr_type'].unique()[0]
        
        st_mean['id_str'] = concat_str
        
        return ETCFrame(etc_mean, self.k_df, das.StatsFrame(st_mean) )
    
    
    def etcf_std(self):
        """
        

        Returns
        -------
        ETCFrame
            ETC data indexed by wavenumber contains std of data, other numerical 
            data for which std is meaningless or not needed contains mean. 
            id_str is concatenated.

        """
        # first take the mean of everything, so we get correct t_uetc
        etc_std = self.etc.groupby(self.etc.index).mean(numeric_only=True)
        # now overwrite the data for wwhich we want std
        etc_std.loc[:, self.k_df.index] = self.etc.loc[:, self.k_df.index].groupby(self.etc.index).std(numeric_only=True)
        st_mean = self.st.groupby(self.st.index).mean(numeric_only=True)
        
        concat_str = self.st._get_id_for_title()

        df = pd.DataFrame(index=etc_std.index, 
                          columns=['id_str', 'field_type', 'corr_type'])
        df['id_str'] = concat_str
        df['field_type'] = self.etc['field_type'].unique()[0]
        df['corr_type'] = self.etc['corr_type'].unique()[0]

        etc_std = pd.concat((etc_std,df), axis=1)
        
        # etc_std['id_str'] = concat_str
        # etc_std['field_type'] = self.etc['field_type'].unique()[0]
        # etc_std['corr_type'] = self.etc['corr_type'].unique()[0]

        st_mean['id_str'] = concat_str
                
        return ETCFrame(etc_std, self.k_df, das.StatsFrame(st_mean) )


    def etc_add(self, other, add_key="corr_type"):
        
        if not self.k_df.equals(other.k_df):
            raise ValueError("etcf_add: error: cannot add ETC objects with different wavenumber series")
        
        if not self.st.equals(other.st):
            raise ValueError("etcf_add: error: cannot add ETC objects with different statsFiles")
        
        addFrame = ETCFrame(self.etc, self.k_df, self.st)
        if isinstance(other, ETCFrame):
            summed_etc_df = self.etc.add(other.etc)
            addFrame.etc.iloc[:, addFrame.k_df.index] = summed_etc_df.iloc[:, addFrame.k_df.index]
        else:
            addFrame.etc.loc[:, addFrame.k_df.index] = self.get_etc() + other
        # if isinstance(other, ETCFrame):
        #     etc_add_df = self.get_etc().add(other.get_etc())
        # else:
        #     etc_add_df = self.get_etc() + other
        
        # addFrame = ETCFrame(etc_add_df, self.k_df, self.st)      
        addFrame.etc[add_key] = self.etc[add_key].unique()[0] + "+" + other.etc[add_key].unique()[0]
        return addFrame
    

    def etc_sub(self, other, sub_key="corr_type"):
        
        if not self.k_df.equals(other.k_df):
            raise ValueError("etcf_add: error: cannot add ETC objects with different wavenumber series")
        
        if not self.st.equals(other.st):
            raise ValueError("etcf_add: error: cannot add ETC objects with different statsFiles")
        
        subFrame = ETCFrame(self.etc, self.k_df, self.st)
        
        if isinstance(other, ETCFrame):
            subFrame.etc.loc[:, subFrame.k_df.index] = self.get_etc().subtract(other.get_etc())
        else:
            subFrame.etc.loc[:, subFrame.k_df.index] = self.get_etc() - (other)
            
        subFrame.etc[sub_key] = self.etc[sub_key]+"+"+other.etc[sub_key]
        return subFrame
    

    def etc_mul(self, other, mul_key="corr_type"):
        
        
        mulFrame = ETCFrame(self.etc, self.k_df, self.st) # This isn't a copy.
        
        if isinstance(other, ETCFrame):
            mulFrame.etc.loc[:, mulFrame.k_df.index] = self.get_etc().multiply(other.get_etc())
        else:
            mulFrame.etc.loc[:, mulFrame.k_df.index] = self.get_etc() * (other)
        
        mulFrame.etc[mul_key] = self.etc[mul_key]+"*Series"
        return mulFrame
    

    def etc_div(self, other, div_key="corr_type"):
        
        
        divFrame = ETCFrame(self.etc, self.k_df, self.st) # This isn't a copy.
        
        if isinstance(other, ETCFrame):
            divFrame.etc.loc[:, divFrame.k_df.index] = self.get_etc().divide(other.get_etc())
        else:
            divFrame.etc.loc[:, divFrame.k_df.index] = self.get_etc() / (other)
        
        divFrame.etc[div_key] = self.etc[div_key]+"/"+other.etc[div_key]
        return divFrame
    

    def __add__(self, other):
        """
        Overloads + operator to be equivalent to append for ETCFrames.        

        Parameters
        ----------
        other : ETCFrame.

        Returns
        -------
        ETCFrame
            Consistes of ETCFrame other appended to calling ETCFrame.

        """        
        return self.etcf_append(other)


    def make_etc_scale(self, t_fit_range=[-np.inf, np.inf], scaling_type='no'):
        """
        Returns dataframes containing scaled k, ETC in a time range. 
        Possible scaling_types are:
            t - simulation time
            toffset - offset simulation time, obtained by fit to xi_lag over t_fit_range
            xi - network string separation
            no - no scaling
            ws - string width.
            omgw - assumes data is a power spectrum of hdot
            omgw_init - computes GW PS from initial energy-momentum ETC.
        """
        t_fit_start = t_fit_range[0]
        t_fit_end = t_fit_range[-1]
        
        # print('t_fit_start',t_fit_start)
        # print('t_fit_end', t_fit_end)
        # fit_period = np.where(np.logical_and(t_fit_start < self.etc.t_uetc, self.etc.t_uetc < t_fit_end))
        
        k_scale_list = []
        etc_scale_list = []
        
        dx = self.st.get_param('dx') 
        N = self.st.get_param('N')
        
        comov_side = N*dx
        # comov_vol = comov_side**3
        dk_scale = 2*np.pi/comov_side
        
        hila_etc_scale = 1/dk_scale

        
        if 'om_ax' in scaling_type:
            om_ax_df = self.get_ang_freq('m_ax')
            om_ax_g = om_ax_df.groupby('id_str')
        
        etc_g, st_g = self.etc_groupby('id_str')
        
        for (n_etc,  group_etc), (n_st,  group_st) in zip(etc_g, st_g):

            # fit_period = np.logical_and(t_fit_start < group_etc.t_uetc, group_etc.t_uetc < t_fit_end)
            fit_period = (t_fit_start <= group_etc.t_uetc) &  (group_etc.t_uetc <= t_fit_end)
            if not np.any(fit_period):
                raise ValueError('make_etc_scale: error: no data between t_start = {:} and t_end = {:}'.format(t_fit_start,t_fit_end))

            st = das.StatsFrame(group_st)
            t_offset = st.t_offset([t_fit_start, t_fit_end])
            t_network = group_etc.t_uetc - t_offset
            

            
            # print(n_etc, group_etc.t_uetc)
            # Find half lightcrossing time
            t_cross = (st.N*st.dx).unique()
            n_half = (group_etc.t_uetc - t_cross/2).abs().argmin()
            
            if 'om_ax' in scaling_type:
                om_ax_df = om_ax_g.get_group(n_etc)[self.k_df.index]
            # print(t_cross, n_half)
            scaling_type_unrecognised = False
            if scaling_type == 't': 
                x_scale_factor = 1.0 * group_etc.t_uetc.loc[fit_period]
                y_scale_factor = 1.0 #* (t_uetc[0]/t_uetc[nt])**0.5
            elif scaling_type == 'toffset':
                x_scale_factor = 1.0 * t_network[fit_period]
                y_scale_factor = 1.0 * t_network[fit_period]/group_etc.t_uetc[fit_period]
            elif scaling_type == 'xi': 
                x_scale_factor = dx * group_etc.xi_interp.loc[fit_period]
                y_scale_factor = 1.0 * group_etc.xi_interp.loc[fit_period]/group_etc.t_uetc[fit_period]
            elif scaling_type == 'ws':
                x_scale_factor = group_etc.ws_interp[fit_period]
                y_scale_factor = 1.0/group_etc.t_uetc[fit_period]
                y_scale_factor *= (group_etc.len_str_interp.iloc[n_half]/group_etc.len_str_interp[fit_period])
            elif scaling_type == 'ws-xi2':
                x_scale_factor = group_etc.ws_interp[fit_period]
                y_scale_factor = 1.0/group_etc.t_uetc[fit_period]
                y_scale_factor *= group_etc.xi_interp.loc[fit_period]**2/group_etc.ws_interp[fit_period]
            elif scaling_type == 'ws-kxi2':
                x_scale_factor = group_etc.ws_interp[fit_period]
                # y_scale_factor = 1.0/group_etc.t_uetc[fit_period]
                # y_scale_factor *= (group_etc.len_str_interp.iloc[n_half]/group_etc.len_str_interp[fit_period])
                xisf = 1.0 * group_etc.xi_interp.loc[fit_period]
                kxi2_df = pd.DataFrame(1, index = xisf.index, columns=self.k_df.index)
                kxi2_df = kxi2_df.mul(xisf**2/group_etc.t_uetc[fit_period], axis='index')* self.k_df
                y_scale_factor = kxi2_df 
            elif scaling_type == 'omgw': 
                x_scale_factor = pd.Series(dx, index=group_etc.t_uetc[fit_period].index)
                rho_crit_comov = 3/(8*np.pi) * group_etc.t_uetc[fit_period]**(-2) 
                etc_to_rho_gw = 1/(32*np.pi)/(group_etc.a_interp.loc[fit_period])**2
                y_scale_factor = etc_to_rho_gw / rho_crit_comov
            elif scaling_type == 'omgw-kt': 
                x_scale_factor = dx * group_etc.t_uetc.loc[fit_period]
                rho_crit_comov = 3/(8*np.pi) * group_etc.t_uetc[fit_period]**(-2) 
                etc_to_rho_gw = 1/(32*np.pi)/(group_etc.a_interp.loc[fit_period])**2
                y_scale_factor = etc_to_rho_gw / rho_crit_comov
            elif scaling_type == 'omgw-kws': 
                # x_scale_factor = group_st.dx[fit_period]
                # x_scale_factor = dx * group_etc.ws_interp.loc[fit_period]
                x_scale_factor = group_etc.ws_interp.loc[fit_period]
                rho_crit_comov = 3/(8*np.pi) * group_etc.t_uetc[fit_period]**(-2) 
                etc_to_rho_gw = 1/(32*np.pi)/(group_etc.a_interp.loc[fit_period])**2
                y_scale_factor = etc_to_rho_gw / rho_crit_comov
            elif scaling_type == 'etcgw_init': 
                x_scale_factor = pd.Series(dx, index=group_etc.t_uetc[fit_period].index)
                # x_scale_factor = group_st.dx[fit_period]
                num_fac = 3/(2*np.pi**2) * (8*np.pi/3)**2
                xsf = 1.0 * group_etc.t_uetc.loc[fit_period]
                kt_df = pd.DataFrame(1, index = xsf.index, columns=self.k_df.index)
                kt_df = kt_df.mul(xsf, axis='index')* self.k_df
                # print(kt_df)
                y_scale_factor = num_fac * kt_df
            elif scaling_type=='hila': 
                x_scale_factor = pd.Series(1.0, index=group_etc.t_uetc[fit_period].index)
                y_scale_factor = hila_etc_scale #* (t_uetc[0]/t_uetc[nt])**0.5
            elif scaling_type == 'hila:x*ws':
                x_scale_factor = group_etc.ws_interp[fit_period]
                y_scale_factor = hila_etc_scale #* (t_uetc[0]/t_uetc[nt])**0.5
            elif scaling_type == 'hila:x*t':
                x_scale_factor = group_etc.t_uetc[fit_period]
                y_scale_factor = hila_etc_scale #* (t_uetc[0]/t_uetc[nt])**0.5
            elif scaling_type == 'hila:x*t:y*xi':
                x_scale_factor = group_etc.t_uetc[fit_period]
                y_scale_factor = hila_etc_scale * group_etc.xi_interp.loc[fit_period]
            elif scaling_type == 'hila:x*t:y*t':
                x_scale_factor = group_etc.t_uetc[fit_period]
                y_scale_factor = hila_etc_scale * group_etc.t_uetc.loc[fit_period]
            elif scaling_type == 'hila:x*t:y*k*t^2':
                x_scale_factor = group_etc.t_uetc[fit_period]
                xsf = 1.0 * group_etc.t_uetc.loc[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = xsf.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') * self.k_df
                y_scale_factor = y_scale_factor.multiply(group_etc.t_uetc.loc[fit_period]**2, axis='index')
            elif scaling_type == 'hila:x*t:y*t*a^2':
                x_scale_factor = group_etc.t_uetc[fit_period]
                y_scale_factor = hila_etc_scale * group_etc.t_uetc.loc[fit_period]
                y_scale_factor *= group_etc.a_interp.loc[fit_period]**2
            elif scaling_type == 'hila:y*k':
                x_scale_factor = pd.Series(dx, index=group_etc.t_uetc[fit_period].index)
                xsf = 1.0 * group_etc.t_uetc.loc[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = xsf.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') * self.k_df
            elif scaling_type == 'hila:y*k*t^2':
                x_scale_factor = pd.Series(dx, index=group_etc.t_uetc[fit_period].index)
                xsf = 1.0 * group_etc.t_uetc.loc[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = xsf.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') * self.k_df
                y_scale_factor = y_scale_factor.multiply(group_etc.t_uetc.loc[fit_period]**2, axis='index')
            elif scaling_type == 'hila:x*ws:y*k':
                x_scale_factor = group_etc.ws_interp[fit_period]
                xsf = 1.0 * group_etc.t_uetc.loc[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = xsf.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') * self.k_df
            elif scaling_type == 'hila:x*ws:y*a^3*k':
                x_scale_factor = group_etc.ws_interp[fit_period]
                # xsf = 1.0 * group_etc.t_uetc.loc[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = x_scale_factor.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') * self.k_df
                y_scale_factor = y_scale_factor.multiply(group_etc.a_interp.loc[fit_period]**3, axis='index')
            elif scaling_type == 'hila:x*ws:y*k*xi**2': # 'axion-xi2':
                x_scale_factor = group_etc.ws_interp[fit_period]
                # y_scale_factor = 1.0/group_etc.t_uetc[fit_period]
                # y_scale_factor *= (group_etc.len_str_interp.iloc[n_half]/group_etc.len_str_interp[fit_period])
                xisf = 1.0 * group_etc.xi_interp.loc[fit_period]
                kxi2_df = pd.DataFrame(1, index = xisf.index, columns=self.k_df.index)
                kxi2_df = kxi2_df.mul(hila_etc_scale*xisf**2, axis='index')* self.k_df
                y_scale_factor = kxi2_df 
            elif scaling_type == 'hila:x*ws:y*xi**2/ws': # 'axion-xi2':
                x_scale_factor = group_etc.ws_interp[fit_period]
                # y_scale_factor = hila_etc_scale * group_etc.t_uetc.loc[fit_period]
                xisf = 1.0 * group_etc.xi_interp.loc[fit_period]
                y_scale_factor = hila_etc_scale * xisf**2 / group_etc.ws_interp.loc[fit_period]
            elif scaling_type == 'hila:x*ws:y*k/om_ax': # 'axion-j0j0':
                x_scale_factor = group_etc.ws_interp[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = x_scale_factor.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') * self.k_df
                y_scale_factor /= om_ax_df[fit_period]
            elif scaling_type == 'hila:x*ws:y*om_ax/k': # 'axion-jsjs':
                x_scale_factor = group_etc.ws_interp[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = x_scale_factor.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') / self.k_df
                y_scale_factor *= om_ax_df[fit_period]
            elif scaling_type == 'hila:x*ws:y*a^3*om_ax/k': # 'axion-nax-jsjs':
                x_scale_factor = group_etc.ws_interp[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = x_scale_factor.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') / self.k_df
                y_scale_factor *= om_ax_df[fit_period]
                y_scale_factor = y_scale_factor.multiply(group_etc.a_interp.loc[fit_period]**3, axis='index')
            elif scaling_type == 'hila:x*ws:y*a^3*k/om_ax': # 'axion-nax-j0j0':
                x_scale_factor = group_etc.ws_interp[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = x_scale_factor.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') * self.k_df
                y_scale_factor /= om_ax_df[fit_period]
                y_scale_factor = y_scale_factor.multiply(group_etc.a_interp.loc[fit_period]**3, axis='index')
            elif scaling_type == 'hila:x*t:y*a^2*om_ax/k': # 'axion-xi2':
                x_scale_factor = group_etc.t_uetc[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = x_scale_factor.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') / self.k_df
                y_scale_factor *= om_ax_df[fit_period]
                y_scale_factor = y_scale_factor.multiply(group_etc.a_interp.loc[fit_period]**2, axis='index')
            elif scaling_type == 'hila:x*t:y*a^2*k/om_ax': # 'axion-xi2':
                x_scale_factor = group_etc.t_uetc[fit_period]
                k_at_all_times_df = pd.DataFrame(1, index = x_scale_factor.index, columns=self.k_df.index)
                y_scale_factor = k_at_all_times_df.mul(hila_etc_scale, axis='index') * self.k_df
                y_scale_factor /= om_ax_df[fit_period]
                y_scale_factor = y_scale_factor.multiply(group_etc.a_interp.loc[fit_period]**2, axis='index')
            else: # scaling_type == 'unrecognised': 
                scaling_type_unrecognised = True
                x_scale_factor = pd.Series(dx, index=group_etc.t_uetc[fit_period].index)
                y_scale_factor = 1.0/group_etc.t_uetc[fit_period]
    
            k_scale = pd.DataFrame(1, index = x_scale_factor.index, columns=self.k_df.index)
            k_scale = k_scale.mul(x_scale_factor, axis='index') * self.k_df
            k_scale['id_str'] = group_etc.id_str
            
            k_scale_list.append(k_scale)
            
            # etc_scale = group_etc.loc[fit_period].loc[:,self.k_df.index].mul(y_scale_factor, axis='index')
            etc_scale = group_etc.loc[fit_period]
            etc_scale.loc[:,self.k_df.index] = etc_scale.loc[:,self.k_df.index].mul(y_scale_factor, axis='index')
            if group_etc.corr_type.unique()[0] == 'vector':
                xsf = 1.0 * group_etc.t_uetc.loc[fit_period]
                kt2_df = pd.DataFrame(1, index = xsf.index, columns=self.k_df.index)
                kt2_df = kt2_df.mul(xsf, axis='index') * self.k_df
                etc_scale *= (kt2_df)**2
            # etc_scale['id_str'] = group_etc.id_str
            
            
            etc_scale_list.append(etc_scale)
        
        k_scale_all = pd.concat(k_scale_list, axis='rows')
        etc_scale_all = pd.concat(etc_scale_list, axis='rows')
        if scaling_type_unrecognised:
            etc_scale_all.loc[:, "scaling_type"] = 'unrecognised'
        else:
            etc_scale_all.loc[:, "scaling_type"] = scaling_type
        
        return k_scale_all, etc_scale_all


    def plot(self, t_range=[-np.inf, np.inf], k_range=None, scaling_type='no', id_str='all', 
              plot_type='loglog', xmultiplier=1.0, ymultiplier=1.0, 
              xlim=None, ylim=None, power_law_params=None, legend=True, 
              cmap=cmap_default, amap=alpha_map_default, zorder=zorder_default, 
              fig_save_suffix=None, ax=None):
        """
        Plot a time series of scaaled ETCs against scaled wavenumber from one 
        or more simulations.  Calls _plot_one as many times as needed.

        Parameters
        ----------
        t_range : 2-sequence, optional
            The range of times to plot. The default is [-np.inf, np.inf].
        scaling_type : str, optional
            Scaling type: [ t | toffset | xi | no | ws ]. The default is 'no'.
        id_str : string or numpy array of strings, optional
            Identifiers of data to plot from am ETCFrame containing a series 
            of runs. The default is 'all'.
        plot_type : string, optional
            Which matplotlib function is used to plot, from 
            [ loglog | semilogx | semilogy | plot]. The default is 'loglog'.
        ylim : 2-tuple, optional
            y axis limits. The default is None.
        power_law_params : n-tuple, optional
            Paramters for plotting power law comparotors (not yet implemented). 
            The default is None.
        legend : bool, optional
            Whether to put legend on plot. The default is True.
        cmap:
        amap:
        zorder:
        fig_save_suffix : str, optional
            If non None, figure is saved with filename ending in this string. 
            The default is None.
        ax : axes, optional
            The axes into which to plot. The default is None.

        Raises
        ------
        ValueError
            If id_str is not recognised..

        Returns
        -------
        fig : figure
            figure handle.
        ax : axes
            axes handle.

        """
        
        
        
        if id_str == 'all':
            id_strings = self.get_id_strings()
        elif isinstance(id_str, str):
            id_strings = np.array([id_str])
        elif isinstance(id_str, list):
            id_strings = np.array(id_str)
        elif isinstance(id_str, np.ndarray):
            id_strings = id_str
        else:
            raise ValueError("ETCFrame: plot: id_str format not recognised.  Must be string or iterable of strings")
            
        for id_str in id_strings:
            etc_fr = self.get_by_id(id_str)
            fig, ax = self._plot_one(etc_fr, None, t_range=t_range, k_range=k_range, scaling_type=scaling_type, 
                            plot_type=plot_type, xmultiplier=xmultiplier, ymultiplier=ymultiplier, 
                            xlim=xlim, ylim=ylim, power_law_params=power_law_params, legend=legend, 
                            cmap=cmap, amap=amap, zorder=zorder, 
                            fig_save_suffix=fig_save_suffix, ax=ax)
                
        if fig_save_suffix is not None: 
            filename = self.etc.field_type.unique()[0]  + self.etc.corr_type.unique()[0] + ''.join(id_strings) \
            + scaling_type + '_' + 'scaling' + fig_save_suffix 
            print("Saving figure as " + filename)
            fig.savefig(filename)
    

        return fig, ax


    def errorbar(self, t_range=[-np.inf, np.inf], k_range=None, scaling_type='no', 
             plot_type='loglog', title=True, xmultiplier=1.0, ymultiplier=1.0, 
             xlim=None, ylim=None, power_law_params=None, legend=True, 
             cmap=cmap_default, amap=alpha_map_default, zorder=zorder_default, 
             fig_save_suffix=None, ax=None, **kwargs):
        """
        Errorbar plot a time series of scaled ETCs against scaled wavenumber from one 
        or more simulations.  Calls _plot_one as many times as needed.

        Parameters
        ----------
        t_range : Sequence, optional
            The times to plot. The default is [-np.inf, np.inf], which gives the first and the last.
        scaling_type : str, optional
            Scaling type: [ t | toffset | xi | no | ws ]. The default is 'no'.
        plot_type : string, optional
            Which matplotlib function is used to plot, from 
            [ loglog | semilogx | semilogy | plot]. The default is 'loglog'.
        ylim : 2-tuple, optional
            y axis limits. The default is None.
        power_law_params : n-tuple, optional
            Paramters for plotting power law comparotors (not yet implemented). 
            The default is None.
        legend : bool, optional
            Whether to put legend on plot. The default is True.
        cmap:
        amap:
        zorder:
        fig_save_suffix : str, optional
            If non None, figure is saved with filename ending in this string. 
            The default is None.
        ax : axes, optional
            The axes into which to plot. The default is None.

        Returns
        -------
        fig : figure
            figure handle.
        ax : axes
            axes handle.

        """
        id_strings = self.get_id_strings()
        etcf_mean = self.etcf_mean()
        etcf_std = self.etcf_std()
        
        
        fig, ax = self._plot_one(etcf_mean, etcf_std, 
                                 t_range, k_range, 
                                 scaling_type, plot_type, title, 
                                 xmultiplier, ymultiplier, 
                            xlim, ylim, power_law_params, legend, 
                            cmap, amap, zorder, 
                            fig_save_suffix, ax,
                            **kwargs)

        if fig_save_suffix is not None: 
            filename = self.etc.field_type.unique()[0] + self.etc.corr_type.unique()[0] + ''.join(id_strings) \
            + scaling_type + '_' + 'scaling' + fig_save_suffix 
            print("Saving figure as " + filename)
            fig.savefig(filename)
    
        return ax


    def _plot_one(self, etc_fr, etcf_std_fr=None, 
                  t_range=[-np.inf, np.inf], k_range=None, 
                  scaling_type='no', plot_type='loglog', title=True, 
                  xmultipliers=1.0, ymultipliers=1.0, 
                  xlim=None, ylim=None, power_law_params=None, legend=True, 
                  cmap=cmap_default, amap=alpha_map_default, zorder=zorder_default, 
                  fig_save_suffix=None, ax=None,
                  **kwargs):
        """
        Plot a time series of scaled ETCs against scaled wavenumber from one 
        or more simulations.  If etcf_std_fr is present, plot has errorbars.
        Calls _plot_one as many times as needed.

        Parameters
        ----------
        etc_fr: ETCFrame
            Data for lines.
        etcf_std_fr: ETCFrame
            If present, used as data for errorbars.  Generated by method etcf_std(), 
            The default is None.
        t_range : Sequence, optional
            The times to plot. The default is [-np.inf, np.inf], which gives the first and last time.
        k_range : n-sequence, optional
            The range of wavenumbers to plot. The default is None, which means all wavenumbers.
        scaling_type : str, optional
            Scaling type: [ t | toffset | xi | no | ws ]. The default is 'no'.
        plot_type : string, optional
            Which matplotlib function is used to plot, from 
            [ loglog | semilogx | semilogy | plot]. The default is 'loglog'.
        xlim : 2-tuple, optional
            x axis limits. The default is None.
        ylim : 2-tuple, optional
            y axis limits. The default is None.
        power_law_params : n-tuple, optional
            Paramters for plotting power law comparotors (not yet implemented). 
            The default is None.
        legend : bool, optional
            Whether to put legend on plot. The default is True.
        cmap:
        amap:
        zorder:
        fig_save_suffix : str, optional
            If non None, figure is saved with filename ending in this string. 
            The default is None.
        ax : axes, optional
            The axes into which to plot. The default is None.

        Returns
        -------
        fig : figure
            figure handle.
        ax : axes
            axes handle.

        """

        try:
            iterator = iter(xmultipliers)
        except TypeError:
            # not iterable
            xmultipliers = [xmultipliers]*len(t_range)
        else:
            # iterable
            pass

        try:
            iterator = iter(ymultipliers)
        except TypeError:
            # not iterable
            ymultipliers = [ymultipliers]*len(t_range)
        else:
            # iterable
            pass
        
        # print(type(etcf_std_fr), xmultiplier, ymultiplier)
        id_strings = etc_fr.get_id_strings()
        n_datasets = len(etc_fr.get_id_strings())
        if n_datasets > 1:
            raise ValueError("ETCFrame.plot: ETCFrame has {} datasets. \n Can't plot more than one dataset at a time.".format(n_datasets))
        else:
            id_str_tex = id_strings[0]    
        
        print('ETCFrame._plot_one: Plotting ' + id_str_tex)
        
        group_etc = etc_fr.etc
        group_st = etc_fr.st

        if t_range[0] == -np.inf:
            t_range[0] = np.min(group_etc.t_uetc)
            
        if t_range[-1] == np.inf:    
            t_range[-1] = np.max(group_etc.t_uetc)
        
        t_start = t_range[0]
        t_end = t_range[-1]
        
        # print(np.min(group_etc.t_uetc), np.max(group_etc.t_uetc))
        # print(t_start, t_end)

        if (t_start > np.max(group_etc.t_uetc)) or (t_end < np.min(group_etc.t_uetc)):
            raise ValueError("ETCFrame.plot: error in t_range, no data to plot.")
        
        # The following confuses the index of the dataframe with the row number
        # It works if they coincide but should be fixed.
        
        fit_period = np.where(np.logical_and(t_start < group_etc.t_uetc, group_etc.t_uetc < t_end))
        # fit_period = (t_start < group_etc.t_uetc) & (group_etc.t_uetc < t_end)

        # print('fit_period', fit_period)

        # t_uetc_fit = group_etc.t_uetc.loc[fit_period]
        if len(t_range) == 2:
            plot_ind = fit_period[0]
        elif len(t_range) > 2:
            plot_ind = [np.argmin(np.abs(t - group_etc.t_uetc)) for t in t_range]
            plot_ind[0] = fit_period[0][0]
            plot_ind[-1] = fit_period[0][-1]
        elif len(t_range) == 1:
            plot_ind = [np.argmin(np.abs(t - group_etc.t_uetc)) for t in t_range]            
        else:
            raise ValueError("ETCFrame.plot: t_range must be a sequence of floats or integers.")

        k_scale, etc_scale = etc_fr.make_etc_scale(t_range, scaling_type)
        
        if etcf_std_fr is not None:
            k_std_scale, etcf_std_scale = etcf_std_fr.make_etc_scale(t_range, scaling_type)


        if ax is None:
            fig, ax = plt.subplots(figsize=(5*1.05,3*1.05))
        else:
            fig = ax.get_figure() 
        
        
        if k_range is None:
            if etc_fr.k_df[0] == 0:
                i_start = 1
            else:
                i_start = 0
            k_ind = etc_fr.k_df.index[i_start:]
        else:
            k_ind = k_range
        
        # This should be the right way to get indices - need to fix it in make_etc_scale as well
        # plot_ind = [(t - group_etc.t_uetc).abs().idxmin() for t in t_range]

        # print('plot_ind', plot_ind)
        # print('xmultipliers', xmultipliers)
        # print('ymultipliers', ymultipliers)
        
        # print('etcf_std_scale.index', etcf_std_scale.index)
        # 
        t_uetc_fit = group_etc.t_uetc.loc[plot_ind]

        
        for nt, xmultiplier, ymultiplier in zip(plot_ind[::-1], xmultipliers[::-1], ymultipliers[::-1]):
            yerr = None
            lab = None
            # if nt == fit_period[0][0]:
            if nt == plot_ind[0]:
                # colour = 'k'
                # alph = 1.0
                # zord = 1
                if legend:
                    lab = r'$\tau = {:.0f}$'.format(t_uetc_fit.loc[nt])
                if etcf_std_fr is not None:
                    # yerr = etcf_std_scale.loc[nt,etcf_std_fr.k_df.index[i_start:]]
                    yerr = etcf_std_scale.loc[nt, k_ind]*ymultiplier
            # elif nt == fit_period[0][-1]:
            elif nt == plot_ind[-1]:
                # colour = 'b'
                # alph = 1.0
                # zord = 2
                if legend:
                    lab = r'$\tau = {:.0f}$'.format(t_uetc_fit.iloc[-1])
                if etcf_std_fr is not None:
                    # yerr = etcf_std_scale.loc[nt,etcf_std_fr.k_df.index[i_start:]]
                    yerr = etcf_std_scale.loc[nt, k_ind]*ymultiplier
            else:
                # colour = 'y'
                # alph = 0.5
                # zord = -1
                if len(plot_ind) < 9 and legend:
                    lab = r'$\tau = {:.0f}$'.format(t_uetc_fit.loc[nt])
                else:
                    lab = None
            
            colour = self.get_colour(t_uetc_fit.loc[nt], t_range, cmap=cmap)
            alph = self.get_alpha(t_uetc_fit.loc[nt], t_range, amap=amap)
            zord = self.get_zorder(t_uetc_fit.loc[nt], t_range, zmap=zorder)
            # if yerr is not None:
            ax.errorbar(k_scale.loc[nt, k_ind]*xmultiplier, 
                                    abs(etc_scale.loc[nt, k_ind])*ymultiplier, 
                                    yerr=yerr,
                                    color=colour, alpha=alph, zorder=zord, 
                                    label=lab,
                                    **kwargs)
            # else:
            #     ax.plot(k_scale.loc[nt, k_ind], 
            #                             abs(etc_scale.loc[nt, k_ind]), 
            #                             color=colour, alpha=alph, zorder=zord, label=lab)
                
        if plot_type == "loglog":
            ax.set_xscale('log')
            ax.set_yscale('log')
        elif plot_type == "semilogx":
            ax.set_xscale('log')
        elif plot_type == "semilogy":
            ax.set_yscale('log')
            
        if power_law_params is not None:
            dah.plot_power_law(ax, etc_fr.k_df*power_law_params[0], *power_law_params[1:])

        xlab, ylab = self.get_axis_labels(scaling_type)        

        ax.set_xlabel(xlab)
        ax.set_ylabel(ylab)
    
        if  xlim is not None:    
            ax.set_xlim(*xlim)
        if  ylim is not None:    
            ax.set_ylim(*ylim)

        if title is True:
            tit_str = group_st._get_title_string() + '\n' \
                + group_etc.field_type.unique()[0] + group_etc.corr_type.unique()[0]  \
                + ' ' + etc_scale.scaling_type.unique()[0] + '-scaling'
            # ax.set_title((group_etc.field_type.unique()[0] + group_etc.corr_type.unique()[0] + ' N={} dx={} s={} ' 
            #               + etc_scale.scaling_type.unique()[0] 
            #               + '-scaling').format(group_st.N.unique()[0], 
            #                                    group_st.dx.unique()[0], 
            #                                    group_st.s.unique()[0]))
        elif isinstance(title, str):
            tit_str = title
        elif title is False or title is None:
            tit_str = None
        else:
            raise ValueError("ETCFrame._plot_one: error: title must be None, boolean or string")
            
        ax.set_title(tit_str)
            
        ax.grid(True)

        if legend:
            if len(plot_ind) < 6:
                ax.legend(loc='best', fontsize='small')
            else:
                ax.legend(bbox_to_anchor=(1.01, 1.0), loc='upper left', fontsize='small')
                
        # fig.tight_layout()
    
        return fig, ax

    def get_total_power(self, code_series='hila'):
        """ Total power in ETC. Different code series store different spectra, 
        so must multiply by different factors. 
        
        hila series: total of square sum of Fourier compoonents in shell of 
        thickness $\Delta k_n$.
        """
        # comov_side = self.st.get_param('N')*self.st.get_param('dx')        
        # comov_vol = comov_side**3
        # dk_scale = 2*np.pi/comov_side
        
        # all_scale = comov_vol * dk_scale
        
        # tpow = np.trapz(self.etc[self.k_df.index], self.k_df)/all_scale
        tpow = np.sum(self.etc[self.k_df.index], axis=1)
        
        tpow_df = pd.DataFrame(tpow, index=self.etc.index, columns=['power'])
        
        tpow_df['t_uetc'] = self.etc['t_uetc']
        tpow_df['id_str'] = self.etc['id_str']
                
        return tpow_df
    
    def add_total_power(self, code_series='hila')    :
        
        tpow_df = self.get_total_power(code_series)
        self.etc['power'] = tpow_df['power']
        
        return

    def multiplot(self, x_key, y_key, z_key=None, yfun=None, ax=None, mean_std=False, 
                  x_range=[-np.inf, np.inf], **kwargs):
        """
        Plot data from all runs in a StatsFrame, but with only only one label

        Parameters
        ----------
        x_key : string
            key for specifying x data. The default is "t".
        y_key : string
            key for specifying y data.
        z_key : string
            key for specifying optional data for transformation of y data by yfun.
        yfun : function handle, optional
            Transformation f(x,y,[z]) to perform on y data. The default is None.
        ax : axes
            axes object for plotting. New axes created if not present.
        **kwargs : dict
            arguments to pass to matplotlib plot function.

        Returns
        -------
        ax : axes object
            

        """
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))
        
        if y_key not in self.etc.columns:
            print('defect_analyse.multiplot: key ' + y_key + ' not found.')
        else:
            plot_kwargs = dict(kwargs)
            x_list = []
            y_list = []
            
            for name,  group in self.etc.groupby(self.etc.id_str):
                if yfun is not None:
                    if z_key is None:
                        y = yfun(group[x_key], group[y_key])
                    else:
                        y = yfun(group[x_key], group[y_key], group[z_key])                    
                else:
                    y = group[y_key]

                x = group[x_key]
                inrange = np.logical_and(x > x_range[0], x< x_range[1])
                x_list.append(x[inrange])
                y_list.append(y[inrange])
 
            if 'xlim' in plot_kwargs:
                ax.set_xlim(plot_kwargs['xlim'])
                plot_kwargs = dah.deletekey(kwargs, "xlim")
            if 'ylim' in plot_kwargs:
                ax.set_ylim(plot_kwargs['ylim'])
                plot_kwargs = dah.deletekey(kwargs, "ylim")
            if not mean_std:
                for x, y in zip(x_list, y_list):
                    h, = ax.plot(x, y, **plot_kwargs)
                    plot_kwargs = dah.deletekey(kwargs, "label")
            else:
                dah.plot_mean_std_xy(x_list, y_list, ax, **plot_kwargs)
                    
        return ax

    def get_colour(self, t, t_range, cmap=cmap_default):
        return cmap((t - t_range[0])/(t_range[-1] - t_range[0]))
        
    def get_alpha(self, t, t_range, amap=alpha_map_default):
        return amap((t - t_range[0])/(t_range[-1] - t_range[0]))

    def get_zorder(self, t, t_range, zmap=zorder_default):
        # print(t,t_range)
        return zmap((t - t_range[0])/(t_range[-1] - t_range[0]))

    def get_id_strings(self):
        """
        ETCFrame method

        Returns
        -------
        numpy.ndarray of type string
            strings are the unique values of id_str, keys to the different datasets.
        """
        return self.etc['id_str'].unique()

    
    def get_etc(self, t_ind=None, k_ind=None):
        """
        ETCFrame.get_etc

        Returns
        -------
        pandas.DataFrame
            Returns the ETC data specified by indexer t_ind and columns k_ind. If not specified, the 
            default is all.

        """
        if k_ind is None:
            k_ind = self.k_df.index

        if t_ind is None:
            # t_ind = self.etc.index
            ret_val = self.etc.iloc[:, k_ind]
        else:            
            ret_val = self.etc.loc[t_ind, k_ind]
        # return self.etc.iloc[t_ind, k_ind]
        return ret_val
    
    def get_other_data(self, t_ind=None):
        """
        ETCFrame.get_other_data

        Returns
        -------
        pandas.DataFrame
            Returns non-ETC data specified by indexer t_ind.  If not specified, the 
            default is all.

        """
        
        if t_ind is None:
            t_ind = self.etc.index
            
        return self.etc.iloc[t_ind, max(self.k_df.index)+1:]
    
    def get_ang_freq(self, m_key, cs2=1.0):
        if m_key not in ['m_higgs', 'm_ax']:
            raise ValueError('ectFrame: error: m_key must be in ["m_higgs", "m_ax"]')
        
        t_interp = self.etc.t_uetc
        m_interp = np.interp(t_interp, self.st.t, self.st[m_key])
        
        
        df1 = pd.DataFrame(1, index = t_interp.index, columns=self.k_df.index)
        df1 = df1.multiply(self.k_df**2, axis='columns')
        # print(df1.shape)
        df2 = pd.DataFrame(1, index = t_interp.index, columns=self.k_df.index)
        df2 = df2.multiply(self.etc.a_interp**2 * m_interp**2, axis='index')
        # print(df2.shape)
        
        om = np.sqrt(df1*cs2 + df2)
        
        om['id_str'] = self.etc['id_str']
        
        return om
    
    def get_axis_labels(self, scaling_type):
    
        # if self.xi_measure == 'xi_r':
        xi_string = r'{\xi_{\rm r}}'
        # if self.xi_measure == 'xiLag':
        #     xi_string = r'{\xi_{\cal L}}'
    
        if scaling_type == 't': 
            ylab = r'$\tau \, (U(k,\tau) /\phi_0^4) $'
            xlab = r'$k \tau$'
        elif scaling_type == 'toffset': 
            ylab = r'$t_{\rm off} \, (U(k,t) /\phi_0^4) $'
            xlab = r'$k t_{\rm off}$'
        elif scaling_type == 'xi': 
            ylab = r'$' + xi_string + '\, (U(k,t) /\phi_0^4) $'
            xlab = r'$k ' + xi_string + '$'
        elif scaling_type == 'ws':
            ylab = r'$(U(k,t) /\phi_0^4) [\ell_w(t_{1/2} )/\ell_w(t )]  $'
            xlab = r'$k w_s$'        
        elif scaling_type == 'ws-xi2':
            ylab = r'$(U(k,t) /\phi_0^4) (' + xi_string + '^2/w_s) $'
            xlab = r'$k w_s$'        
        elif scaling_type == 'ws-kxi2':
            ylab = r'$(U(k,t) /\phi_0^4) k' + xi_string + '^2  $'
            xlab = r'$k w_s$'
        elif scaling_type == 'omgw':
            ylab = r'$\Omega_{\rm gw}(k,\tau) / (G\phi_0^2)^2 $'
            xlab = r'$k\Delta x$'
        elif scaling_type == 'omgw-kt':
            ylab = r'$\Omega_{\rm gw}(k,\tau) / (G\phi_0^2)^2 $'
            xlab = r'$k \tau$'
        elif scaling_type == 'omgw-kws':
            ylab = r'$\Omega_{\rm gw}(k,\tau) / (G\phi_0^2)^2 $'
            xlab = r'$k w_s$'
        elif scaling_type == 'etcgw_init':
            ylab = r'$\Omega_{\rm gw}(k,\tau) / (G\phi_0^2)^2 $'
            xlab = r'$k\Delta x$'
        elif scaling_type == 'hila':
            ylab = r'$(k^2/2\pi^2)U(k,t) $'
            xlab = r'$k$'
        elif scaling_type == 'hila:x*ws':
            ylab = r'$(k^2/2\pi^2)U(k,t) $'
            xlab = r'$k w_s$'
        elif scaling_type == 'hila:x*ws:y*a^3*k':
            ylab = r'$(k^3/2\pi^2)U(k,t)a^3(\tau) $'
            xlab = r'$k w_s$'
        elif scaling_type == 'hila:x*t':
            ylab = r'$(k^2/2\pi^2)U(k,t) $'
            xlab = r'$k \tau$'
        elif scaling_type == 'hila:x*t:y*xi':
            ylab = r'$\xi (k^2/2\pi^2)U(k,t) $'
            xlab = r'$k \tau$'
        elif scaling_type == 'hila:x*t:y*t':
            ylab = r'$\tau (k^2/2\pi^2)U(k,t) $'
            xlab = r'$k \tau$'
        elif scaling_type == 'hila:x*t:y*t':
            ylab = r'$\tau (k^2/2\pi^2)U(k,t) $'
            xlab = r'$k \tau$'
        elif scaling_type == 'hila:x*t:y*k*t^2':
            ylab = r'$\tau^2 (k^3/2\pi^2)U(k,t) $'
            xlab = r'$k \tau$'
        elif scaling_type == 'hila:x*t:y*t*a^2':
            ylab = r'$a^2(\tau)\tau (k^2/2\pi^2)U(k,t) $'
            xlab = r'$k \tau$'            
        elif scaling_type == 'hila:y*k':
            ylab = r'$(k^3/2\pi^2)U(k,t) $'
            xlab = r'$k\Delta x$'
        elif scaling_type == 'hila:y*k*t^2':
            ylab = r'$\tau^2(k^3/2\pi^2)U(k,\tau) $'
            xlab = r'$k\Delta x$'
        elif scaling_type == 'hila:x*ws:y*k':
            xlab = r'$k w_s$'
            ylab = r'$(k^3/2\pi^2)U(k,t) $'
        elif scaling_type == 'hila:x*ws:y*xi**2/ws':
            xlab = r'$k w_s$'
            ylab = r'$(k^2/2\pi^2)U(k,t) \xi^2/w_s $'
        elif scaling_type == 'hila:x*ws:y*k*xi**2': # 'axion-xi2':
            ylab = r'$(k^3/2\pi^2)U(k,t) \xi_r^2 $'
            xlab = r'$k w_s$'
        elif scaling_type == 'hila:x*ws:y*k/om_ax': # 'axion-xi2':
            xlab = r'$k w_s$'
            ylab = r'$(k^3/2\pi^2)U(k,t)/\omega_{{\rm a}} $'
        elif scaling_type == 'hila:x*ws:y*om_ax/k': # 'axion-xi2':
            xlab = r'$k w_s$'
            ylab = r'$(\omega_{{\rm a}}k^2/2\pi^2)U(k,t) $'
        elif scaling_type == 'hila:x*ws:y*a^3*k/om_ax': # 'axion J0J0':
            xlab = r'$k w_s$'
            ylab = r'$a^3(t)(k^3/2\pi^2)U(k,t)/\omega_{{\rm a}} $'
        elif scaling_type == 'hila:x*ws:y*a^3*om_ax/k': # 'axion JsJs':
            xlab = r'$k w_s$'
            ylab = r'$a^3(t)(\omega_{{\rm a}}k^2/2\pi^2)U(k,t) $'
        elif scaling_type == 'hila:x*t:y*a^2*k/om_ax': # 'axion J0J0':
            xlab = r'$k \tau$'
            ylab = r'$a^3(t)(k^3/2\pi^2)U(k,t)/\omega_{{\rm a}} $'
        elif scaling_type == 'hila:x*t:y*a^2*om_ax/k': # 'axion JsJs':
            xlab = r'$k \tau$'
            ylab = r'$a^2(t)(\omega_{{\rm a}}k^2/2\pi^2)U(k,t) $'
        elif scaling_type == 'raw':
            ylab = r''
            xlab = r'$k$ [code units]'
        else: # scaling_type == 'no':
            ylab = r'$U(k,t) /\phi_0^4 $'
            xlab = r'$k \Delta x$'
    
        return xlab, ylab            
    
    def get_param(self, key):
        return self.st.get_param(key)

