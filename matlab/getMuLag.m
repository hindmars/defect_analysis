function [mu_lag0, fB_lag, fD_lag, fV_lag] = getMuLag(dx)
%
% [mu_lag0, fB_lag, fD_lag, fV_lag] = getMuLag(dx)
% 
% Returns lag-weighted mass per unit length, plus fractions in B, DPhi and V(phi)
%  dx is lattice spacing (row vector)
%  uses numerical data from xA=0.0 wave.

% mu_lag0 = getWaveEnergies(10,'lag')
% mu_lag0(1,:) = [-6.8409   -6.9698   -7.0060]; 

% poly fit coefficients to dx = [0.5,0.25,0.125]
% a = polyfit(dx,mu_lag0(1,:),2) etc

% fB_lag_all = [ 0.2146    0.2074    0.2060 ];
% fD_lag_all = [ 0.5870    0.5886    0.5889 ];
% fV_lag_all = [ 0.1984    0.2040    0.2052 ];

% mu_lag0 = getWaveEnergies(10,'lag') (negative so output is positive)
a_lag(1,:) = -[ 0.6029    0.0635   -7.0233];
a_lag(2,:) = -[-0.1851    0.0496   -1.4464];
a_lag(3,:) = -[ 0.4338    0.0215   -4.1350];
a_lag(4,:) = -[ 0.3542   -0.0076   -1.4419];

mu_lag0 = a_lag(1,1)*dx.^2 + a_lag(1,2)*dx + a_lag(1,3);
fB_lag = (a_lag(2,1)*dx.^2 + a_lag(2,2)*dx + a_lag(2,3))./mu_lag0;
fD_lag = (a_lag(3,1)*dx.^2 + a_lag(3,2)*dx + a_lag(3,3))./mu_lag0;
fV_lag = (a_lag(4,1)*dx.^2 + a_lag(4,2)*dx + a_lag(4,3))./mu_lag0;


