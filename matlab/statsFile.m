%StatsFile reader for LAH code 2005-2016
%
%Usage: [tOffset, dXi_dt] = statsFile(pNum,id,run,tFit,simpars,inPath,parent)
%
%pNum = 0 for all or plot number for single axes set
%       eg. 2 for |fields|, 6 for xi
%       -1 for no plots, output Lagrangian tOffset and dXi_dt only 
%       -2 for winding (S&V) tOffset and dXi_dt  only
%       -3 for Lag-weighted energy tOffset and dXi_dt  only
%       -4 for energy tOffset and dXi_dt  only
%  id = ID string between 'statsFile_' and before '.dat'
%       eg. to load statsFile_6L01 ID is '6L%2' with run=1
% run = realizations(s) to include
%
%Optional parameters are:
%
% tFit = xi fit range (only needed for xi fitting)
% simpars - vector containing
%    dx = lattice spacing (only needed for winding xi)
%     N = lattice size (only needed for winding xi)
%  beta = scalar mass^2/vector mass^2
% ... (vector can be empty vector [])
%        NEW 2016 - can be a Map container with key and value
% path = path to file, including final '/'
%        (if omited gets path from gpath global variable)
%
% Version 2013.6.11 MBH (without default underscore, allows no run no.)
% Version 2013.7.26 MBH 
%    merges |pi|,|phi| with |E| and |B| into subfig 2
%    adds energy conservation (subfig 3). 
%    Adds "hidden" options 10 and 11 for lag-weighted versions of subfigs 7 and 8
%    Adds <E^2>/<B^2> etc as vel^2 estimators to subfig 9
% Version 2015.10.13 MBH
%    correct scalar estimate for <V^2>
%    new interface for simulation parameters 
%    (was statsFile(pNum,id,run,tFit,dx,N,inPath,parent))
% Version 2015.12.22 MBH
%    New plotting pNum =12 for Lag-weighted string length estimators
% Version 2016.05.11 ALE, MBH
%    Plotting for ON statsFiles (number of columns = 13)
%    Start using Map container class for passing simpars
% Version 2016.08.20 MBH
%    Added velLag = 1 + L_lag./e_lag 
% Version 2017.04.14 MBH
%    Added outputs for xi_e(nergy) (nasty kludge, replaces tOffset)
% Version 2017.08.15 MBH
%    Scaled all dimensional quantities with phi0, vev of (complex) field (
%    this may need to be reconsidered - use m_gauge for length dimensions?)
%    Also, plotted comoving string width divided by dx in subplot 1 
%    Plot 14 now gives comoving string width divided by time.
%    More run parameters can be read in from the simpars container

function [tOffset, dXi_dt] = statsFile(pNum,id,run,tFit,simpars,inPath,parent)

verbose = false;

if nargin==0; 
  help statsFile
  return
end

global gpath

if ~exist('inPath','var'); inPath=''; end 

if numel(inPath)>0; 
  path=inPath; 
else
  if numel(gpath)>0
    path=gpath;
  else
    disp(['statsFile: Please set gpath global variable to default path'...
	  ' or specify path in fucntion call'])
    return
  end
end

if verbose
    disp(['statsFile: Current working path: ' path])
end

%if ~exist('dx','var'); dx=0; end
%if ~exist('N','var'); N=0; end
%%% simpars defaults
sB = 0; % Core growth parameter
beta = 1;
N = 0; % Turns off xi plotting
dx = 0;% Turns off xi plotting
phi0 = 1; 
q = 1;
lambda = 2;

%ManFac = sqrt(pi/6);
%ManFac = 1/(pi/4+2/pi); % New Manhattan factor
ManFac = 2/3; % Newer Manhattan factor


% %%% simpars overrides
if isa(simpars,'containers.Map')
    if verbose
        disp('statsFile: simpars is a container')
    end
    if isKey(simpars,'dx'); dx = simpars('dx'); end
    if isKey(simpars,'N'); N = simpars('N'); end
    if isKey(simpars,'lambda'); lambda = simpars('lambda'); end
    if isKey(simpars,'q'); q = simpars('q'); beta = 0.5*lambda/q^2; end
    if isKey(simpars,'beta'); beta = simpars('beta'); lambda = (2*beta/q^2); end  % if specified directly 
    if isKey(simpars,'sB'); sB = simpars('sB'); end
    if isKey(simpars,'m_gauge'); m_gauge = simpars('m_gauge'); end
    if isKey(simpars,'vev'); phi0 = simpars('vev'); end % Complex field vev
else
    if verbose
        disp('statsFile: simpars is a vector')
    end
    if numel(simpars)>3
        sB = simpars(4);
    end
    if numel(simpars)>2
        beta = simpars(3);
        lambda = 2*beta;
    end
    if numel(simpars)>1
        N = simpars(2);
    end
    if numel(simpars)>0
    dx = simpars(1);
    end
end

vol = (phi0*dx*N)^3;
m_gauge = sqrt(2)*q*phi0;
m_scalar = sqrt(lambda)*phi0;

disp(['phi_0         ' num2str(phi0)] )
disp(['m_scalar(end) ' num2str(m_scalar)] )
disp(['m_gauge(end)  ' num2str(m_gauge)] )
disp(['beta          ' num2str(beta)] )


if ~exist('tFit','var'); tFit=0; end

%==============
%Prepare figure
%==============
if pNum==0
  if ~exist('parent','var');
      clf
      ax=multiPlot([3 3]);
  else
      ax=parent;
  end
elseif pNum>0
    if ~exist('parent','var'); 
        clf; 
    else
        axes(parent) 
    end
end

ECol = 'r--';
BCol = 'r';
PiCol = 'b--';
DphiCol = 'b';
VCol = 'g';
PhiCol = 'm';

PiGolCol = ECol;
DphiGolCol = BCol;


%======================
%Form num2str ID string
%======================
s=strfind(id,'%');
if numel(s) > 0
    ID=[id(1:s+1) '.' id(s+1) 'i' id(s+2:end)];
    idString = num2str(run(1),ID);
else
    ID = id;
    idString = ID;
end

%==============
%Loop over runs
%==============
for i=1:numel(run)

  %=========
  %Read file
  %=========
  if numel(s) > 0
      idString = num2str(run(i),ID);
  else
      idString = ID;
  end
  file=[path 'statsFile' idString '.dat'];
  if verbose
      disp(['statsFile: Loading file: ' file]); 
  end
  stats=load(file);
  sizes=size(stats);

  if verbose
      disp(['statsFile: File size: ' num2str(sizes)])
  end
  
  %=========
  %Assign variable values according to file size
  %=========
  if sizes(2)==21

  %====================
  %New LAH files
  %====================
      t=          phi0*stats(:,1);
      a=          stats(:,2);
      q=          stats(:,3);
      modPhi=     stats(:,4)/phi0;
      modPi=      stats(:,5)/phi0^2;
      modFij=     stats(:,6)/phi0^2;
      modF0i=     stats(:,7)/phi0^2;
      gaussCD=    stats(:,8)/phi0^3;
      gaussDE=    stats(:,9)/phi0^3;
      eF0i=       stats(:,10)/phi0^4;
      eF0i_lag=   stats(:,11)/phi0^8;
      eFij=       stats(:,12)/phi0^4;
      eFij_lag=   stats(:,13)/phi0^8;
      ePi=        stats(:,14)/phi0^4;
      ePi_lag=    stats(:,15)/phi0^8;
      eDjPhi=     stats(:,16)/phi0^4;
      eDjPhi_lag= stats(:,17)/phi0^8;
      eV=         stats(:,18)/phi0^4;
      eV_lag=     stats(:,19)/phi0^8;
      SLexp=      phi0*stats(:,20);
      SLwind=     phi0*stats(:,21); 
      
      
  elseif sizes(2)==16

  %====================
  %Old LAH files
  %====================
      t=phi0*stats(:,1);
      a=stats(:,2);
      q=stats(:,3);
      modPhi=stats(:,4)/phi0;
      modPi=stats(:,5)/phi0^2;
      modFij=stats(:,6)/phi0^2;
      modF0i=stats(:,7)/phi0^2;
      gaussCD=stats(:,8)/phi0^3;
      gaussDE=stats(:,9)/phi0^3;
      eF0i=stats(:,10)/phi0^4;
      eFij=stats(:,11)/phi0^4;
      ePi=stats(:,12)/phi0^4;
      eDjPhi=stats(:,13)/phi0^4;
      eV=stats(:,14)/phi0^4;
      SLexp=phi0*stats(:,15);
      SLwind=phi0*stats(:,16);

    elseif sizes(2)==13

  %====================
  %ON files
  %=============
        t=phi0*stats(:,1);
        a=stats(:,2);
        q=stats(:,3); % really sqrt(lambda)
        modPhi=stats(:,4)/phi0;
        modPi=stats(:,5)/phi0^2;
        ePi=stats(:,6)/phi0^4;
        ePi_lag=stats(:,7)/phi0^8;
        eDjPhi=stats(:,8)/phi0^4;
        eDjPhi_lag=stats(:,9)/phi0^8;
        eV=stats(:,10)/phi0^4;
        eV_lag=stats(:,11)/phi0^8;
        SLexp=phi0*stats(:,12);
        SLwind=phi0*stats(:,13);

    elseif sizes(2)==17

  %====================
  %New ON files
  %=============
        t=phi0*stats(:,1);
        a=stats(:,2);
        q=stats(:,3); % really sqrt(lambda)
        modPhi=stats(:,4)/phi0;
        modPi=stats(:,5)/phi0^2;
        ePi=stats(:,6)/phi0^4;
        ePi_lag=stats(:,7)/phi0^8;
        eDjPhi=stats(:,8)/phi0^4;
        eDjPhi_lag=stats(:,9)/phi0^8;
        ePhiPi=stats(:,10)/phi0^4;
        ePhiPi_lag=stats(:,11)/phi0^8;
        ePhiDjPhi=stats(:,12)/phi0^4;
        ePhiDjPhi_lag=stats(:,13)/phi0^8;
        eV=stats(:,14)/phi0^4;
        eV_lag=stats(:,15)/phi0^8;
        SLexp=phi0*stats(:,16);
        SLwind=phi0*stats(:,17);

    else
      error('statsFile: stats file type not recognised')
    
    end

  %===============================
  %Perform Additional Calculations
  %===============================

  if numel(tFit)==1; tFit=[tFit max(t)]; end

  if (sizes(2)==13 || sizes(2)==17)
      e=ePi+eDjPhi+eV;    %Energy density
      L=ePi-eDjPhi-eV;    %Lag. density
      p=ePi - eDjPhi/3 - eV; % Pressure
  else
      e=eF0i+eFij+ePi+eDjPhi+eV;    %Energy density
      L=eF0i-eFij+ePi-eDjPhi-eV;    %Lag. density
      p=eF0i/3+eFij/3+ePi - eDjPhi/3 - eV; % Pressure
  end;

  if sizes(2)==17
      ePiGol = ePi - ePhiPi; % Goldtone kinetic energy
      eDjPhiGol = eDjPhi - ePhiDjPhi; % Goldstone gradient energy
  end
  
  
    % Calculate derived string parameters
    %
    rs = (a(end)*q(end)./(q.*a)); % string width relative to end
    beta = (a(end)*q(end))^2; % Recall that q is now sqrt(lambda/2), and assume that gauge coupling = 1.

    dx_eff = phi0*dx./rs;

    if ( sizes(2)==21 || sizes(2)==16 )        
        [mu,fB,fD,fV] = getMu(dx_eff);
        disp(['mu       ' num2str(mu(end))])
        df = fB-fV;
        mu = mu*beta^0.195; %Abelian Higgs String energy per unit length (beta from Hill Hodges Turner 88)
        disp(['mu (corr)' num2str(mu(end))])
        [mu_lag,fB_lag,fD_lag,fV_lag] = getMuLag(dx_eff);
        disp(['mu_lag       ' num2str(mu_lag(end))])
        df_lag = fB_lag - fV_lag;
        mu_lag = mu_lag*beta^0.7./rs.^2;  % measured from static 3D solution
        disp(['mu_lag (corr)' num2str(mu_lag(end))])
    else % sizes(2) == 16
        mu = pi;    %for global defects, real fields.Definition of mu (needs checking)
        %     mu_lag = 0.84*pi./rs.^2; % Lag-weighted, Calculated with relaxation code global_string.f
        mup   = 0.2839568;
        mup_V = 0.104466;
        mu_lag = mup*pi./rs.^2; % V-weighted, Calculated with relaxation code global_string.f
        fV_lag = mup_V/mup;
        df = 0; %Check
        df_lag = -fV_lag; 

          % fractional contributions to the global string
      % tension from gradient and potential energies.
%      mup_D =mup-mup_V;

    end
    
%mu0 = 6.2054; % dx = 0.5 N=64 static solution
%mu0 = 6.2647; % dx = 0.25 N=128 static solution
%mu0 = 6.2780; % dx = 0.125 N=256 static solution
%mu0 = 6.2832;  % N=infty static solution (2*pi)
%a = [0.3203   -0.0052    0.0002]; % poly fit coefficients

%mu_lag0 = [ 6.8409, 6.9698, 7.006]; 
% dx = [0.5, 0.25, 0.125]; % static solution
%a_lag = [-0.6027   -0.0636    7.0234];

%mu0 = 2*pi - (a(1)*dx^2 + a(2)*dx + a(3));
%mu_lag0 = a_lag(1)*dx^2 + a_lag(2)*dx + a_lag(3);

xiLag = 1./sqrt(-L./mu);
xi_e = 1./sqrt(e./mu);


  if (sizes(2) == 21)
      e_lag=(eF0i_lag+eFij_lag+ePi_lag+eDjPhi_lag+eV_lag);    %Energy density
      L_lag=(eF0i_lag-eFij_lag+ePi_lag-eDjPhi_lag-eV_lag);    %Lag. density
      p_lag=(eF0i_lag/3+eFij_lag/3+ePi_lag - eDjPhi_lag/3 - eV_lag); % Pressure
  end

  if (sizes(2) == 13 || sizes(2) == 17 )
      disp("Setting _lag")
      e_lag=(ePi_lag+eDjPhi_lag+eV_lag);    %Energy density - actually potential weigthed
      L_lag=(ePi_lag-eDjPhi_lag-eV_lag);    %Lag. density
      p_lag=(ePi_lag - eDjPhi_lag/3 - eV_lag); % Pressure
  end


  if dx>0 & N>0
      SLlag = -(L/mu)*vol;
      xiWind = 1./sqrt(SLwind/vol);
  else
      SLlag = zeros(size(SLwind));
      xiWind = zeros(size(SLwind));
  end

  
  if ( abs(tFit)>0 ) & ( pNum==-1 | pNum==0 | pNum==5 | pNum==6 | pNum==20 )
      % striaght line fitting with xiLag
      tSub=t(t>=tFit(1) & t<=tFit(2));
      xiSub=xiLag(t>=tFit(1) & t<=tFit(2));
%       xiLag_All(i,:) = xiLag;
      pLag=leastSquares(tSub,xiSub);
      dXi_dtLag(i)=pLag(1);
      tXi0lag(i)=-pLag(2)/pLag(1);
      if numel(run) == 1;
          disp(['statsFile: Lagrangian xi fitting parameters for t > ' num2str(tFit)]);
          disp(['statsFile: Gradient: ' num2str(pLag(1)) '  xi-intercept: ' num2str(pLag(2)) '  t-intercept: ' num2str(tXi0lag(i))])
      end
      %    if pNum==-1
      %      tOffset(i) = tXi0lag(i);
      %    end
  else
      dXi_dtLag(i)=0;
      pLag=[0 0];
      tXi0lag(i)=0;
  end
  tScaleLag = t - tXi0lag(i);

  if abs(tFit)>0 & dx>0 & N>0 & ( pNum==-2 | pNum==0 | pNum==5 | pNum==6 )
      % striaght line fitting with xiWind
      pWind=leastSquares(t(t>=tFit(1) & t<=tFit(2)),xiWind(t>=tFit(1) & t<=tFit(2)));
      dXi_dtWindSV(i)=pWind(1)/sqrt((pi/6));
      tXi0WindSV(i)=-pWind(2)/pWind(1);
      if numel(run) ==1
          disp(['statsFile: Winding xi (S&V corrected) fitting parameters for t > ' num2str(tFit)])
          disp(['statsFile: Gradient: ' num2str(pWind(1)/sqrt((pi/6))) '  xi-intercept: ' num2str(pWind(2)/sqrt((pi/6))) '  t-intercept: ' num2str(tXi0WindSV(i))])
      end
      %    if pNum==-2
      %      tOffset(i) = tXi0wind(i);
      %    end
  else
      dXi_dtWindSV(i)=0;
      tXi0WindSV(i)=0;
      pWind=[0 0];
  end
  tScaleWind = t - tXi0WindSV(i);

  if abs(tFit)>0 & ( pNum==-4 | pNum==13 )
      % striaght line fitting with xi_e
      p_e=leastSquares(t(t>=tFit(1) & t<=tFit(2)),xi_e(t>=tFit(1) & t<=tFit(2)));
      dXi_dt_e(i)=p_e(1);
      tXi0_e(i)=-p_e(2)/p_e(1);
      if numel(run) ==1
          disp(['statsFile: Energy xi fitting parameters for t > ' num2str(tFit)])
          disp(['statsFile: Gradient: ' num2str(p_e(1)) '  xi-intercept: ' num2str(p_e(2)) '  t-intercept: ' num2str(tXi0_e(i))])
      end
  else
      dXi_dt_e(i)=0;
      tXi0_e(i)=0;
      p_e=[0 0];
  end
  tScale_e = t - tXi0_e(i);
%   rho_All(i,:) = e.*tScale_e.^2;


  % Calculate velocity estimators
  L_e_lag = L_lag./e_lag;
  w_lag = p_lag./e_lag;

  Rgrad = (ePi_lag)./(eDjPhi_lag); %201509 correction
  velGrad = 2*Rgrad./(1 + Rgrad);  %201509 correction

  velEOS = (1 + 3*w_lag - 2*df_lag)./(2 - df_lag.*(1+3*w_lag));
  velLag_lag = (1 + L_e_lag)./(1 - df_lag.*L_e_lag);

  if sizes(2)==21
      velGauge = (eF0i_lag) ./ (eFij_lag);
  end

  % Calculate xi estimators
  SL_e_lag = -vol*(e_lag - df_lag.*L_lag)./((1+df_lag).*mu_lag);
  xi_e_lag = sqrt(vol./SL_e_lag);
  xiInv = sqrt(-mu.*(1-velEOS)./L);
  xiInvLag = sqrt(-mu_lag.*(1-velEOS)./L_lag);

  velLag = 1. + L./e;
  
  if ( sizes(2)==21 || sizes(2)==13 || sizes(2)==17 )
      
%       xi_e_lag_All(i,:) = xi_e_lag;
      if abs(tFit)>0 & ( pNum==-3 | pNum==0 | pNum==5 | pNum==6 | pNum==20)
          % striaght line fitting with xi_e_lag
          p_e_lag=leastSquares(t(t>=tFit(1) & t<=tFit(2)),xi_e_lag(t>=tFit(1) & t<=tFit(2)));
          dXi_dt_e_lag(i)=p_e_lag(1);
          tXi0_e_lag(i)=-p_e_lag(2)/p_e_lag(1);
          if numel(run) ==1
              disp(['statsFile: Lag-weighted energy xi fitting parameters for t > ' num2str(tFit)])
              disp(['statsFile: Gradient: ' num2str(p_e_lag(1)) '  xi-intercept: ' num2str(p_e_lag(2)) '  t-intercept: ' num2str(tXi0_e_lag(i))])
          end
          %    if pNum==-2
          %      tOffset(i) = tXi0_e_lag(i);
          %    end
      else
          dXi_dt_e_lag(i)=0;
          tXi0_e_lag(i)=0;
          p_e_lag=[0 0];
      end
      tScale_e_lag = t - tXi0_e_lag(i);
        

  end

  if (sizes(2)==13 || sizes(2) == 17)
      if abs(tFit)>0 & ( pNum==-3 | pNum==0 | pNum==5 | pNum==6 | pNum==20 )
          % striaght line fitting with xi_e_lag
          p_e_lag=leastSquares(t(t>=tFit(1) & t<=tFit(2)),xi_e_lag(t>=tFit(1) & t<=tFit(2)));
          dXi_dt_e_lag(i)=p_e_lag(1);
          tXi0_e_lag(i)=-p_e_lag(2)/p_e_lag(1);
          if numel(run) ==1
              disp(['statsFile: Lag-weighted energy xi fitting parameters for t > ' num2str(tFit)])
              disp(['statsFile: Gradient: ' num2str(p_e_lag(1)) '  xi-intercept: ' num2str(p_e_lag(2)) '  t-intercept: ' num2str(tXi0_e_lag(i))])
          end
          %       if pNum==-2
          %           tOffset(i) = tXi0_e_lag(i);
          %       end
      else
          dXi_dt_e_lag(i)=0;
          tXi0_e_lag(i)=0;
          p_e_lag=[0 0];
      end
      tScale_e_lag = t - tXi0_e_lag(i);
      
%       Rgrad = (ePi_lag)./(eDjPhi_lag); %201509 correction
%       velGrad = 2*Rgrad./(1 + Rgrad);  %201509 correction
      
  end


  tPos = t;
  tPos(t<0) = -t(t<0);

  %===========================
  %Output initial value of T00
  %===========================
  
  if verbose
      disp(['statsFile: Initial value of T00 = ' num2str(e(1))])
  end
  
  %============
  %PLOT RESULTS
  %============
  
  if pNum==0
    axes(ax(1))
  end
  if pNum==0 | pNum==1
    % Comomving string width (compton wavelength of scalar) in lattice
    % units
    wsc_dx = 1./(sqrt(2)*a.*q*phi0.*dx);
    plot(t,wsc_dx,'r'); hold on  % Note that sqrt(lambda) is written instead of q from 10.14 on 
    xlabel('\tau\phi_0')
    ylabel('w_s/a dx')
    axis tight
    set(gca,'YLim',[min(wsc_dx)-1.0, max(wsc_dx)+1.0])
  end

  if pNum==0
    axes(ax(2))
  end
  if pNum==0 | pNum==2
    if (sizes(2)==13 || sizes(2)==17)
    loglog(t,sqrt(1-modPhi.^2),PhiCol,t,modPi,PiCol); hold on
    ylabel('\surd(1-|\phi|^2), |\pi|')
    else
    loglog(t,sqrt(1 - modPhi.^2),PhiCol,t,modPi,PiCol,t,modFij./a,BCol,t,modF0i./a,ECol); hold on
    ylabel('\surd(1-|\phi|^2), |\pi|, |B| and |E|')
    end;
%     set(gca,'XScale','log')
%     set(gca,'YScale','log')
    xlabel('\tau\phi_0')
%     axis tight
    hold on
  end

  if pNum==0
    axes(ax(3))
  end
  if pNum==0 | pNum==3
    H = [(diff(a)./diff(t))./a(1:(end-1)); 0];
    edot = [diff(e)./diff(t); 0];
    % Covariant conservation for \int d^3x T00 (downstairs)
    plot(t,abs(edot),t,abs(H.*(e+3*p)),t,abs(edot+H.*(e+3*p)),'--k')%,t,abs(2*H.*(eV-eFij-eF0i)),'--m')
    set(gca,'XScale','log')
    set(gca,'YScale','log')
    xlabel('\tau\phi_0')
    ylabel('Global e conservation')
    axis tight
    hold on
  end
  
  if pNum==0
    axes(ax(4))
  end
  if pNum==0 | pNum==4
    if (sizes(2)==13 || sizes(2) == 17 )
    text(0.2,0.3,'NO GAUGE FIELDS')
    else
    plot(t,gaussCD,'b',t,gaussDE,'.r',t,abs(gaussDE-gaussCD),'--k')
    end;
    set(gca,'yscale','log')
    xlabel('\tau\phi_0')
    ylabel('Gauss Law')
    axis tight
    hold on
  end
  
  if pNum==0
    axes(ax(5))
  end
  if pNum==0 | pNum==5
     plot(t(2:end),diff(xiWind)./diff(t)/ManFac,'--r'); hold on
     plot(t(2:end),diff(xiLag)./diff(t),'b'); hold on
     if sizes(2)==21
         plot(t(2:end),diff(xiInv)./diff(t),'Color',[0,0.7,0]); hold on
         plot(t(2:end),diff(xi_e_lag)./diff(t),'Color',[0.7,0,0.7]); hold on
     end
     xlabel('\tau\phi_0')
     ylabel('d\xi_w/d\tau (r), d\xi_L/d\tau (b), ')
     set(gca,'XLim',[tFit(1)*0.75 max(t)])
     set(gca,'YLim',[0 1.0])
     plot(tFit(1)*[1 1],get(gca,'YLim'),'-k')
     plot(tFit(2)*[1 1],get(gca,'YLim'),'-k')
  end

  if pNum==0
    axes(ax(6))
  end
  if pNum==0 | pNum==6
    %ManFac = sqrt(pi/6); Scherrer & Vilenkin correction for GRF
    ManFac = 1/(pi/4+2/pi); % New Manhattan factor
    plot(t,xiWind,'r'); hold on;
%     plot(t,xiWind/sqrt(ManFac),'--r'); 
%     plot(t,xiWind/sqrt(pi/6),'-.k'); 
    plot(t,xiLag,'b'); hold on;
    tMin = min([t(1) 0]);
    plot([tMin t(end)],pWind(1)*[tMin t(end)]+pWind(2),'--','Color',[0.5 0.5 0.5])
    plot([tMin t(end)],pLag(1)*[tMin t(end)]+pLag(2),'--','Color',[0.5 0.5 0.5])
    if ( sizes(2)==21 || sizes(2)==13 || sizes(2)==17 )
         plot(t,xiInv,'Color',[0,0.7,0]); hold on;
        plot(t,xi_e_lag,'Color',[0.7,0,0.7]); hold on;
        plot([tMin t(end)],p_e_lag(1)*[tMin t(end)]+p_e_lag(2),'--','Color',[0.5 0.5 0.5])
    end
    axis tight
    ylim=get(gca,'YLim');    
%     set(gca,'YLim',[0 max(max(xiWind/sqrt(pi/6),xiLag))])
    set(gca,'YLim',[0 0.75*max(t)])
    xlabel('\tau\phi_0')
    ylabel('\xi_w (r), \xi_L (b), \xi_{L,v}, (g) \xi_{eL}, (m)')
    plot(tFit(1)*[1 1],get(gca,'YLim'),'--','Color',[0.5 0.5 0.5])
    plot(tFit(2)*[1 1],get(gca,'YLim'),'--','Color',[0.5 0.5 0.5])
  end

  if pNum==0
    axes(ax(7))
  end
  if pNum==0 | pNum==7
    if (sizes(2)==13)
        plot(t,sqrt(mu./ePi),PiCol,t,sqrt(mu./eDjPhi),DphiCol,t,sqrt(mu./eV),VCol,t,sqrt(mu./e),'k')
        ylabel(['\xi_\pi (' PiCol '), \xi_{D} (' DphiCol '), \xi_V (' VCol '), \xi_e (k)'])
    elseif (sizes(2)==17)
        plot(t,sqrt(mu./ePiGol),PiGolCol,t,sqrt(mu./eDjPhiGol),DphiGolCol,t,sqrt(mu./ePi),PiCol,t,sqrt(mu./eDjPhi),DphiCol,t,sqrt(mu./eV),VCol,t,sqrt(mu./e),'k')
        ylabel(['\xi_{\pi,g} (' PiGolCol '), \xi_{D,g} (' DphiGolCol '),\xi_\pi (' PiCol '), \xi_{D} (' DphiCol '), \xi_V (' VCol '), \xi_e (k)'])
    else
        plot(t,sqrt(mu./eF0i),ECol,t,sqrt(mu./eFij),BCol,t,sqrt(mu./ePi),PiCol,t,sqrt(mu./eDjPhi),DphiCol,t,sqrt(mu./eV),VCol,t,sqrt(mu./e),'k')
        ylabel(['\xi_{E} (' ECol '), \xi_{B} (' BCol '),\xi_\pi (' PiCol '), \xi_{D} (' DphiCol '), \xi_V (' VCol '), \xi_e (k)'])
    end;
    xlabel('\tau\phi_0')
    axis tight
%    set(gca,'YLim',[0 max(sqrt(mu./0.1*e))])
    set(gca,'YLim',[0 max(t)])
    hold on
  end

  if pNum==0
    axes(ax(8))
  end
  if pNum==0 | pNum==8
    if (sizes(2)==13)
        plot(t,ePi./e,PiCol,t,eDjPhi./e,DphiCol,t,eV./e,VCol,t,p./e,'k')
    elseif (sizes(2)==17)
        plot(t,ePiGol./e,PiGolCol,t,eDjPhiGol./e,DphiGolCol,t,ePi./e,PiCol,t,eDjPhi./e,DphiCol,t,eV./e,VCol,t,p./e,'k')
    else
        plot(t,eF0i./e,ECol,t,eFij./e,BCol,t,ePi./e,PiCol,t,eDjPhi./e,DphiCol,t,eV./e,VCol,t,p./e,'k')
    end;
    xlabel('\tau\phi_0')
    ylabel('Energy fraction and EOS')
    axis tight
        set(gca,'YLim',[-0.5 1.0])
    hold on
  end

  if pNum==0
    axes(ax(9))
  end
  if pNum==0 | pNum==9
    plot(t,velGrad,'b'); hold on
    plot(t,velEOS,'Color',[0,0.7,0])
    plot(t,velLag_lag,'k')
    plot(t,velLag,'Color',[0.7,0.7,0.7])
    axis tight
    maxY = 0.2*ceil(max(velGrad)/0.2);
    set(gca,'YLim',[0 maxY])
    if  sizes(2)==21  
        plot(t,velGauge,'r');
        ylabel('v^2_V (r), v^2_S (b), v^2_{eos} (g), v^2_{lag,lag} (k), v^2_{lag} (gy) [2016]')
    end
    if (sizes(2)==13 || sizes(2)==17)
        ylabel(' v^2_S (b), v^2_{eos,V} (g), v^2_{lag,V} (k), v^2_{lag} (gy) [2019]')
    end
    xlabel('\tau\phi_0')

  end
  
  
  if (sizes(2) == 21)      

%   if pNum==0
%     axes(ax(10))
%   end
%   if pNum==0 | pNum==10
    if pNum == 10
        plot(t,1./sqrt(-eF0i_lag),ECol,t,1./sqrt(-eFij_lag),BCol,t,1./sqrt(-ePi_lag),PiCol,t,1./sqrt(-eDjPhi_lag),DphiCol,...
        t,1./sqrt(-eV_lag),VCol,t,1./sqrt(-e_lag),'k')
        xlabel('\tau\phi_0')
        ylabel('1/sqrt(T_\mu_\nu) (Lag weighted)')
        axis tight
      hold on
    end
  
%   if pNum==0
%     axes(ax(11))
%   end
%   if pNum==0 | pNum==11
    if pNum == 11
%         axes(parent)
%         hold on
        plot(t,(eF0i_lag./e_lag),ECol,t,(eFij_lag./e_lag),BCol,t,(ePi_lag./e_lag),PiCol,t,(eDjPhi_lag./e_lag),DphiCol,...
        t,(eV_lag./e_lag),VCol,t,(p_lag./e_lag),'k')
        hold on
        xlabel('\tau\phi_0')
        ylabel('Lag wt-ed e fraction and string EOS')
        axis tight
        set(gca,'YLim',[-0.5 0.75])
        hold on
    end
    
    if pNum == 12
%         fB = 0.208;
%         fDphi = 0.585; % measured from statis string solution
%         mu_lag = 6.84; % beta = 1 only, dx = 0.5
        disp(['statsFile: Core growth parameter: ' num2str(sB)])
        xi_e = 1./sqrt(e/mu);
        xi_e_lag = a.^sB./sqrt(-e_lag./mu_lag);
        xi_B_lag = a.^sB./sqrt(-eFij_lag./(fB.*mu_lag));
        xi_s_lag = a.^sB./sqrt(-(eDjPhi_lag+ePi_lag)./(fD.*mu_lag));
%        plot(t,1./sqrt(-eF0i_lag),ECol,t,1./sqrt(-eFij_lag),BCol,t,1./sqrt(-ePi_lag),PiCol,t,1./sqrt(-eDjPhi_lag),DphiCol,...
%        t,1./sqrt(-eV_lag),VCol,t,1./sqrt(-e_lag),'k')
        plot(t,xi_e_lag,'k',t,xi_B_lag,BCol,t,xi_s_lag,DphiCol,t,xiLag,'b--',t,xiWind,'r--',t,xi_e,'m--')
        xlabel('\tau\phi_0')
        ylabel('\xi (Lag and lag-weighted energy)')
        axis tight
        set(gca,'YLim',[0 0.75*t(end)])
      hold on
    end
  end

if sizes(2) == 13 | sizes(2) == 17

%   if pNum==0
%     axes(ax(10))
%   end
%   if pNum==0 | pNum==10
    if pNum == 10
        plot(t,1./sqrt(ePi_lag),PiCol,t,1./sqrt(eDjPhi_lag),DphiCol,...
             t,1./sqrt(eV_lag),VCol,t,1./sqrt(e_lag),'k')
        xlabel('\tau\phi_0')
        ylabel('1/sqrt(T_\mu_\nu) (Pot weighted)')
        axis tight
        hold on
    end

%   if pNum==0
%     axes(ax(11))
%   end
%   if pNum==0 | pNum==11
    if pNum == 11
%         axes(parent)
%         hold on
        plot(t,(ePi_lag./e_lag),PiCol,t,(eDjPhi_lag./e_lag),DphiCol,...
             t,(eV_lag./e_lag),VCol,t,(-p_lag./e_lag),'k')
        hold on
        xlabel('\tau\phi_0')
        ylabel('Pot wt-ed e fraction and string EOS')
        axis tight
        set(gca,'YLim',[-0.5 0.75])
        hold on
    end
    %% 

    if pNum == 12
        fB = 0.208;
        fDphi = 0.585; % measured from statis string solution
        mu_lag = 6.84; % beta = 1 only, dx = 0.5
        disp(['statsFile: Core growth parameter: ' num2str(sB)])
        xi_e = 1./sqrt(e/mu);
        xi_e_lag = a.^sB./sqrt(e_lag/mu_lag);
        xi_s_lag = a.^sB./sqrt((eDjPhi_lag+ePi_lag)/(fDphi*mu_lag));
        %     plot(t,1./sqrt(-eF0i_lag),ECol,t,1./sqrt(-eFij_lag),BCol,t,1./sqrt(-ePi_lag),PiCol,t,1./sqrt(-eDjPhi_lag),DphiCol,...
           %        t,1./sqrt(-eV_lag),VCol,t,1./sqrt(-e_lag),'k')
        plot(t,xi_e_lag,'k',t,xi_s_lag,DphiCol,t,xiLag,'b--',t,xiWind/sqrt(ManFac),'r--',t,xi_e,'m--')
        xlabel('\tau\phi_0')
        ylabel('\xi (Lag and pot-weighted energy)')
        axis tight
        hold on
    end
    
    if pNum == 13
        zeta_w = 0.25*t.^2.*SLwind/vol;
        plot(t, zeta_w)
        xlabel('\tau\phi_0')
        ylabel('\zeta (winding)')
        set(gca,'XLim',[0 t(end)]);
        set(gca,'YLim',[0 5]);
%         axis tight
        hold on
    end
    
end

  switch pNum
      case {-1,0,5,6,20}
          tOffset(i) = tXi0lag(i);
          dXi_dt(i) = dXi_dtLag(i);
      case {-2}
          tOffset(i) = tXi0WindSV(i);
          dXi_dt(i) = dXi_dtWindSV(i);
      case {-3}
          tOffset(i) = tXi0_e_lag(i);
          dXi_dt(i) = dXi_dt_e_lag(i);
      case {-4,13}
          tOffset(i) = tXi0_e(i);
          dXi_dt(i) = dXi_dt_e(i);
      otherwise
          tOffset(i) = 0;
          dXi_dt(i) = 0;
  end
  
end

if (pNum == -4 | pNum == 14)
   % Compute mean energy density scaling with error band
   %    rho = e.*tScale_e.^2;
   rho_mean = mean(rho_All,1);
   rho_std = std(rho_All,0,1);

    rho_mean_av = mean(rho_mean( t > tFit(1) & t < tFit(2) ));
    rho_std_av = mean(rho_std( t > tFit(1) & t < tFit(2) ));
    
%     % Nasty nasty kludge to get the data out
%     tOffset = mean(rho_All(:,t > tFit(1) & t < tFit(2)),2)';
%     dXi_dt = std(rho_All(:,t > tFit(1) & t < tFit(2)),0,2)';
% Latest - no need, because rho = pi/dXi_dt^2

    if (pNum == 14)
%         Then plot
    pRef = mean(dXi_dt_e)*[1, -mean(tXi0_e)];
   
    X=[t(1:end-1) t(1:end-1) t(2:end) t(2:end)];
    Y=[rho_mean(1:end-1)'-1*rho_std(1:end-1)' rho_mean(1:end-1)'+1*rho_std(1:end-1)' rho_mean(2:end)'+1*rho_std(2:end)' rho_mean(2:end)'-1*rho_std(2:end)'];
    Y2=[rho_mean(1,1:end-1)'-2*rho_std(1:end-1)' rho_mean(1:end-1)'+2*rho_std(1:end-1)' rho_mean(2:end)'+2*rho_std(2:end)' rho_mean(2:end)'-2*rho_std(2:end)'];

    patch(X',Y2',[0.8 0.8 0.8],'EdgeColor',[0.8 0.8 0.8]);hold on;
    patch(X',Y',[0.4 0.4 0.4],'EdgeColor',[0.4 0.4 0.4]);hold on;
    plot(t,rho_mean,'Color',[0 0 0]); 
    hold on;

%     plot([0 t(end)],pRef(1)*[0 t(end)]+pRef(2),'--','Color',[0.5 0.5 0.5]); hold on;
% 
%     x1=linspace(tFit(1),tFit(1),500);
%     x2=linspace(tFit(2),tFit(2),500);
%     y1=logspace(-5.5,4,500);
% 
%     plot(x1,y1,'-.','Color',[0.5 0.5 0.5]); hold on;
%     plot(x2,y1,'-.','Color',[0.5 0.5 0.5]); hold on;


    set(gca,'XLim',[0 t(end)]);
    set(gca,'YLim',[0 2.5*rho_mean(end)]);

    disp(['statsFile: Mean T_00.tau^2 over fit range: ' num2str(rho_mean_av) ' +/- ' num2str(rho_std_av) ] ) 

    end

end

if pNum==15
    % Comomving string width (compton wavelength of scalar) in time units
    % units
    wsc = 1./(sqrt(2)*a.*q*phi0);
    plot(t(t>0),wsc(t>0)./t(t>0),'r'); hold on  % Note that sqrt(lambda) is written instead of q from 10.14 on 
    xlabel('\tau\phi_0')
    ylabel('w_s/a t')
    axis tight
end
  
if pNum == 20
%Plot with error band
% xiRef = xiLag_All;
% pRef = pLag;
    xiRef = xi_e_lag_All;
    pRef = mean(dXi_dt_e_lag)*[1, -mean(tXi0_e_lag)];
    xi_mean = mean(xiRef,1);
    xi_std = std(xiRef,0,1);

    X=[t(1:end-1) t(1:end-1) t(2:end) t(2:end)];
    Y=[xi_mean(1:end-1)'-1*xi_std(1:end-1)' xi_mean(1:end-1)'+1*xi_std(1:end-1)' xi_mean(2:end)'+1*xi_std(2:end)' xi_mean(2:end)'-1*xi_std(2:end)'];
    Y2=[xi_mean(1,1:end-1)'-2*xi_std(1:end-1)' xi_mean(1:end-1)'+2*xi_std(1:end-1)' xi_mean(2:end)'+2*xi_std(2:end)' xi_mean(2:end)'-2*xi_std(2:end)'];

    patch(X',Y2',[0.8 0.8 0.8],'EdgeColor',[0.8 0.8 0.8]);hold on;
    patch(X',Y',[0.4 0.4 0.4],'EdgeColor',[0.4 0.4 0.4]);hold on;
    plot(t,xi_mean,'Color',[0 0 0]); hold on;
    plot([0 t(end)],pRef(1)*[0 t(end)]+pRef(2),'--','Color',[0.5 0.5 0.5]); hold on;

    x1=linspace(tFit(1),tFit(1),500);
    x2=linspace(tFit(2),tFit(2),500);
    y1=logspace(-5.5,4,500);

    plot(x1,y1,'-.','Color',[0.5 0.5 0.5]); hold on;
    plot(x2,y1,'-.','Color',[0.5 0.5 0.5]); hold on;

    set(gca,'XLim',[0 t(end)]);
    set(gca,'YLim',[0 0.75*t(end)]);
end


if ( pNum >= 0)
    mtit(file,'Interpreter','none')
end

if ( pNum==0 & ~exist('parent','var') ) % Don't call if axes already set up
  multiPlotZoom(ax);
end

if numel(run)>1 & (pNum==-1 | pNum==0 | pNum==6)
  disp(['statsFile: Mean dXi/dt = ' num2str(mean(dXi_dtLag)) ' +/- ' num2str(std(dXi_dtLag)) '  (Lag.)'])
  disp(['statsFile: Mean tXi=0 = ' num2str(mean(tXi0lag)) ' +/- ' num2str(std(tXi0lag)) '  (Lag.)'])
end
if numel(run)>1 & (pNum==-2 | pNum==0 | pNum==6)
    disp(['statsFile: Mean dXi/dt = ' num2str(mean(dXi_dtWindSV)) ' +/- ' num2str(std(dXi_dtWindSV)) '  (S&V winding.)'])
    disp(['statsFile: Mean tXi=0 = ' num2str(mean(tXi0WindSV)) ' +/- ' num2str(std(tXi0WindSV)) '  (Winding.)'])
end
if numel(run)>1 & (pNum==-3 | pNum==0 | pNum==6 | pNum==20)
    disp(['statsFile: Mean dXi/dt = ' num2str(mean(dXi_dt_e_lag)) ' +/- ' num2str(std(dXi_dt_e_lag)) '  (Lag. weighted energy)'])
    disp(['statsFile: Mean tXi=0 = ' num2str(mean(tXi0_e_lag)) ' +/- ' num2str(std(tXi0_e_lag)) '  (Lag. weighted energy)'])
end
if numel(run)>1 & (pNum==-4 | pNum==13)
  disp(['statsFile: Mean dXi/dt = ' num2str(mean(dXi_dt_e)) ' +/- ' num2str(std(dXi_dt_e)) '  '])
  disp(['statsFile: Mean tXi=0 = ' num2str(mean(tXi0_e)) ' +/- ' num2str(std(tXi0_e)) '  '])
end

% Hmm, why this?
if pNum==20
    tOffset=mean(dXi_dtLag);
end
