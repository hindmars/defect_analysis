function [k,t,C,sd]=ETCplot(Cname,id,run,tRef,tOffSet,simpars,tLimit,inPath,parent)
%ETC plotting function for UETC.hpp data 2006-2008
%
%Usage: ETCplot(Cname,id,run,tRef,tOffset,simpars,tLimit,path,parent)
%
%Cname = ETC name, eg. scalar11 or vector
%   id = ID string between 'ETCscalar11_' and before '.dat'
%        eg. to load statsFile_6L01 ID is '6L%2' with run=1
%  run = realizations(s) to include
% tRef = UETC reference time
% tOffset = time when xi=0, 
% - if '*' get from statsFile (Lag & tRef<t<2*tRef)
% - if 'noscaling' do not scale k with time (and undo correlator time scaling) 
% - if 'xiscaling' get xi from statsFile using statGet
%
%Optional parameters are:
%
%  tLimit = vector with times between which to plot
%   path = path to file, including final '/'
%          (if omited or '' gets path from gpath global variable)
% parent = destination axes for plot
% simpars - vector containing
%    dx = lattice spacing (only needed for winding xi)
%     N = lattice size (only needed for winding xi)
%  beta = scalar mass^2/vector mass^2
% ... (vector can be empty vector [])
%        NEW 2016 - can be a Map container with key and value
%
% revisions 2013 MBH 
% - plotting without scaling (useful for standing wave sims)
% - plotting with xi scaling
% revisions 2014.11 MBH 
% - corrected errors with interp1 and xiscaling
% - plots now with time even for xi scaling (t = xi/xiSlope)
%Version 2016.05.11 ALE, MBH
%    Start using Map container class for passing simpars
%   Update '*' and '*W' options for the new stats file

if nargin==0; 
  help ETCplot
  return
end

global gpath

if ~exist('inPath','var'); inPath=''; end 

disp(inPath)

if numel(inPath)>0; 
  path=inPath; 
else
  if numel(gpath)>0
    path=gpath;
  else
    disp(['Please set gpath global variable to default path'...
	  ' or specify path in function call'])
    return
  end
end

% %%% simpars overrides
if isa(simpars,'containers.Map')
    disp('simpars is a container')
    if isKey(simpars,'dx'); dx = simpars('dx'); end
    if isKey(simpars,'N'); N = simpars('N'); end
    if isKey(simpars,'lambda'); lambda = simpars('lambda'); end
    if isKey(simpars,'q'); q = simpars('q'); beta = 2.*lambda/q; end
    if isKey(simpars,'beta'); beta = simpars('beta'); end  % if specified directly
    if isKey(simpars,'sB'); sB = simpars('sB'); end
else
    disp('simpars is a vector')
    if numel(simpars)>3
        sB = simpars(4);
    end
    if numel(simpars)>2
        beta = simpars(3);
    end
    if numel(simpars)>1
        N = simpars(2);
    end
    if numel(simpars)>0
        dx = simpars(1);
    end
end

if ~exist('tOffSet','var'); tOffSet=0; end
if ~exist('tLimit','var'); tLimit=[0 9999999]; end

switch tOffSet
    case '*'
        tOffSetString = 'tOffset from xi_lag';
    case '*W'
        tOffSetString = 'tOffset from xi_wind';
    case 'xiscaling'
        tOffSetString = 'xiscaling';
    case 'xiv2scaling'
        tOffSetString = 'xiv2scaling';
    case 'xiWscaling'
        tOffSetString = 'xiWscaling';
    case 'xiscalingYonly'
        tOffSetString = 'xiscaling Y only';
    case 'dSscaling'
        tOffSetString = 'dSscaling';
    case 'noscaling'
        tOffSetString = 'noscaling';
    otherwise
        tOffSetString = ['tOffSet ' num2str(tOffSet)];
end

%Prepare for plot
if exist('parent','var')~=1; clf; else axes(parent); end

%Get number of runs
nRuns=size(run,2);

%Get tOffset from statsFile if necessary
if strcmp(tOffSet,'*')==1
  disp(['** Getting tOffSet from statsFile Lag. fit for ' ...
	'tRef -> 2*tRef **'])
  tOffSet = statsFile(-1,id,run,tRef*[1 2],simpars,path); % dx, N kludge
end
if strcmp(tOffSet,'*W')==1
  disp(['** Getting tOffSet from statsFile Winding. fit for ' ...
	'tRef -> 2*tRef **'])
  tOffSet = statsFile(-2,id,run,tRef*[1 2],simpars,path); % dx, N kludge
end

%Check to see if we want to scale
noscaling = 0;
if strcmp(tOffSet,'noscaling')==1
  disp('** Plotting against k and removing t factor from correlator')
  tOffSet = 0;
  noscaling = 1;
end

%Get xiLag from statsFile 
xiscaling = 0;
if strcmp(tOffSet,'xiscaling')==1
  disp(['** Scaling with xiLag'])
  [xiLag, tStat] = statGet('xiLag',id,run,simpars,path);
  if nRuns > 1
      xiLagAv = mean(xiLag,1);
  else
      xiLagAv = xiLag;
  end
  tOffSet = 0;
  xiscaling = 1;
end

%Get xiLag from statsFile, de Sitter scaling
dSscaling = 0;
if strcmp(tOffSet,'dSscaling')==1
  disp(['** Scaling with xiLag only (de Sitter)'])
  [xiLag, tStat] = statGet('xiLag',id,run,simpars,path);
  if nRuns > 1
      xiLagAv = mean(xiLag,1);
  else
      xiLagAv = xiLag;
  end
  tOffSet = 0;
  dSscaling = 1;
end

%Get xiLag, v^2 from statsFile
xiv2scaling = 0;
if strcmp(tOffSet,'xiv2scaling')==1
  disp(['** Scaling with xiLag and v^2'])
  [xiLag, tStat] = statGet('xiLag',id,run,simpars,path);
  %[eF0i_lag tStat] = statGet('eF0i_lag',id,run,simpars,path);
  %[eFij_lag tStat] = statGet('eFij_lag',id,run,simpars,path);
  [eDjPhi_lag, tStat] = statGet('eDjPhi_lag',id,run,simpars,path);
  [ePi_lag, tStat] = statGet('ePi_lag',id,run,simpars,path);

  %velGauge = (eF0i_lag) ./ (eFij_lag);
  velGrad = (ePi_lag) ./ (eDjPhi_lag);

%  v2 = velGauge*1.5;
  v2 = velGrad*3;
  
  if nRuns > 1
      xiLagAv = mean(xiLag,1);
      v2Av = mean(v2,1);
  else
      xiLagAv = xiLag;
      v2Av = v2;
  end
  tOffSet = 0;
  xiv2scaling = 1;
end

%Get xiWind from statsFile 
xiWscaling = 0;
if strcmp(tOffSet,'xiWscaling')==1
  disp(['** Scaling with xiWind'])
  [xiWind, tStat] = statGet('xiWind',id,run,simpars,path);
  if nRuns > 1
      xiWindAv = mean(xiWind,1);
  else
      xiWindAv = xiWind;
  end
  tOffSet = 0;
  xiWscaling = 1;
end

%Get xiLag from statsFile 
xiscalingYonly = 0;
if strcmp(tOffSet,'xiscalingYonly')==1
  disp(['** Scaling with xiLag, Y only'])
  [xiLag, tStat] = statGet('xiLag',id,run,simpars,path);
  if nRuns > 1
      xiLagAv = mean(xiLag,1);
  else
      xiLagAv = xiLag;
  end
  tOffSet = 0;
  xiscalingYonly = 1;
end



%Duplicate tOffSet if single value (eg. 0) given for many runs
if size(tOffSet,2)==1 && nRuns>1
  tOffSet = ones(1,nRuns)*tOffSet;
end


%Load ETC

[k,t,C,sd]=ETCload(path,Cname,id,run,tRef,tOffSet,tLimit(1),tLimit(2));
C=abs(C);
tOrig = t;

% Get slope from xi_lag
if (xiscaling | xiv2scaling | xiscalingYonly)
    tFitLow = 0.5*(tLimit(1)+tLimit(2));
    tFitHigh = tLimit(2);
    tSub=tStat(tStat>=tFitLow & tStat<=tFitHigh);
    xiSubAv=xiLagAv(tStat>=tFitLow & tStat<=tFitHigh);
    pLag=leastSquares(tSub,xiSubAv);
    xiSlope=pLag(1);
    disp(['xi lag slope mean: ' num2str(mean(xiSlope))])
end

% Get slope from xi_wind
if (xiWscaling)
    tFitLow = 0.5*(tLimit(1)+tLimit(2));
    tFitHigh = tLimit(2);
    tSub=tStat(tStat>=tFitLow & tStat<=tFitHigh);
    xiSubAv=xiWindAv(tStat>=tFitLow & tStat<=tFitHigh);
    pLag=leastSquares(tSub,xiSubAv);
    xiSlope=pLag(1);
    disp(['xi wind slope mean: ' num2str(mean(xiSlope))])
end


if exist('number','var')==1
   C=C(number(1):number(2),:);
   t=t(number(1):number(2));
end

if (noscaling == 1)
    for i=1:size(C,1)
        C(i,:) = C(i,:)/t(i); 
        sd(i,:) = sd(i,:)/t(i);
        t(i) = 1;
    end
end

if (xiscaling == 1)
    for i=1:size(C,1)
        xiScale = spline(tStat,xiLagAv,t(i));
        C(i,:) = xiScale*C(i,:)/(xiSlope*t(i));
        sd(i,:) = xiScale*sd(i,:)/(xiSlope*t(i));
        t(i) = xiScale/xiSlope;
    end
end

if (dSscaling == 1)
    for i=1:size(C,1)
        xiScale = spline(tStat,xiLagAv,t(i));
        C(i,:) = xiScale*C(i,:)/abs(t(i)); 
        sd(i,:) = xiScale*sd(i,:)/abs(t(i));
        t(i) = xiScale;
    end
end

if (xiv2scaling == 1)
    for i=1:size(C,1)
        xiScale = spline(tStat,xiLagAv,t(i));
        v2Scale = spline(tStat,v2Av,t(i));
        if (i==1)
            v2First = v2Scale;
        end
        C(i,:) = xiScale*C(i,:)*v2First/(v2Scale*xiSlope*t(i));
        sd(i,:) = xiScale*sd(i,:)*v2First/(v2Scale*xiSlope*t(i));
        t(i) = xiScale/xiSlope;
    end
end

if (xiscalingYonly == 1)
    for i=1:size(C,1)
        xiScale = spline(tStat,xiLagAv,t(i));
        C(i,:) = xiScale*C(i,:)/(xiSlope*t(i));
        sd(i,:) = xiScale*sd(i,:)/(xiSlope*t(i));
    end
end

%t = tOrig;

%t = abs(t);

%Plot on top of each other (hopefully)
for i=2:size(C,1)-1 %ceil(linspace(1,size(C,1),10)) %floor(size(C,1)/2):size(C,1)
    plot(k*t(i),C(i,:),'.-g'); hold on;
end
hold on

%Highlight lines and plot uncertainties
plotLogNegative(k*t(end),C(end,:),'b',2) 
if nRuns>1 
    plotErrorBars(k*t(end),C(end,:),sd(end,:),'b')
    plotErrorBars(k*t(1),C(1,:),sd(1,:),'k')
end
plotLogNegative(k*t(1),C(1,:),'k',2);

%set(gca,'XLim',[min(kt)*min(r) 40])
%set(gca,'YLim',[1e-4 1e3])
axis tight
set(gca,'XScale','log')
%set(gca,'YScale','log')
set(gca,'YScale','linear')

title(['C=' Cname ', tMin ' num2str(min(tOrig)) ', tMax ' num2str(max(tOrig)) ', ' tOffSetString ', ' inPath ],'interpreter','none')

if (noscaling==1)
    xlabel('k')
    ylabel('C(kt,kt)/t')
elseif (xiscaling==1 || xiscalingYonly==1)
    xlabel('kt')
    ylabel('C(kt,kt)')
elseif (dSscaling==1)
    xlabel('k\xi')
    ylabel('(t/\xi)C(kt,kt)')
    title(['C=' Cname ', xiMin ' num2str(min(xiScale)) ', xiMax ' num2str(max(xiScale)) ', ' tOffSetString ', ' inPath ],'interpreter','none')
elseif (xiv2scaling==1)
    xlabel('kt')
    ylabel('C(kt,kt)*(v^2(t1)/v^2(t)')
else    
    xlabel('kt')
    ylabel('C(kt,kt)')
end

