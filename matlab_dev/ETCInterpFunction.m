function ETCInterpFunction(Cname,s,lcdm,idRM,runRM,pathCell,idCell,runs,tOffset,tLimit_rm,tLimitm,tLimitr,Fit,colour,simpars)
% JL 20/04/2016
% 
% Usage: ETCInteroFunction(Cname,s,lcdm,idRM,runRM,pathCell,idCell,runs,tOffset,tLimit_rm,tLimitm,tLimitr,Fit,colour)
% 
%     Cname = name of the correlator, e.g. 'scalar11'
%     s = value of the fattening parameter, 0 or 1
%     lcdm = 1: Matter-Lambda transition
%            0: Radiation-Matter
%     idRM = usually '00%1'
%     runRM = number of realizations
%     pathCell = {Cell}. 1st path to transition ETC folder, 2nd to Mat and 3rd to Rad. E.g. {pathRM, pathM, pathR}
%     idCell = id's of pure matter and radiation correlators, e.g. {'_tEq0.0001_00%1','_tEq10000_00%1'}
%     runs = number of pure rad and mat realizations
%     tOffset = 
%     tLimit_rm = first and last ETCs of RM and ML simulations, e.g. [150 300]
%     tLimitm = Same for pure mat sims
%     tLimitr = Same for pure rad
%     Fit = 1: Fit computed interpolation function
%           0: Do not fit
%     colour = colour of the fitting function
%     
%     Example: (s=0, Rad-Mat)
%     ETCInterpFunction('vector',0,0,'_00%1',1:4,{pathRM,pathRM,pathRM},{'_tEq0.0001_00%1','_tEq10000_00%1'},1:4,'noscaling',[150 300],[150 300],[150 300],1,'r',simpars)
%     

if nargin==0; 
  help ETCInterpFunction
  return
end
if isa(simpars,'containers.Map')
    'Using containers'
    if isKey(simpars,'dx'); dx = simpars('dx'); end
    if isKey(simpars,'N'); N = simpars('N'); end
    if isKey(simpars,'lambda'); lambda = simpars('lambda'); end
    if isKey(simpars,'q'); q = simpars('q'); beta = 2.*lambda/q; end
    if isKey(simpars,'beta'); beta = simpars('beta'); end  % if specified directly
    if isKey(simpars,'sB'); sB = simpars('sB'); end
else
    'Using simpars'
    if numel(simpars)>3
        sB = simpars(4);
    end
    if numel(simpars)>2
        beta = simpars(3);
    end
    if numel(simpars)>1
        N = simpars(2);
    end
    if numel(simpars)>0
        dx = simpars(1);
    end
end

%Hardcoded
%Specification of transitions runs: Rad-Mat and Mat-Lam

if s==1
    tref_trans=[450 500 550 600];%Transition sim reference times
    tref=[600 450];%Mat and Rad reference times
elseif s==0
    tref_trans=[150 150 150 150 150];%Transition sim reference times
    tref=[150 150];%Mat and Rad reference times
    
    if lcdm~=1
        tEq=[300 150 40 10 3];%RM Equality sim time for each trans. run
        eraB=[1.09 1.17 1.44 1.76 1.91];%Constant intermediate expansion rates
    elseif lcdm==1
        tEq=[33 0];%ML Equality time for each trans. run
    end
end

   for i=1:size(tEq,2)
        
            nRunsRM=size(runRM,2);
            nRuns=size(runs,2);
            
            %Define id's
            
            if lcdm==1 %Mat-Lambda
                id=['_lcdm1k_1.' num2str(tEq(i)) 'tNow' idRM]
                if tEq(i)==0
                    tEq(i)=1.0;
                elseif tEq(i)==33
                    tEq(i)=1.33;
                end
            else %Rad-Mat
                id=['_radmat1k_tEq' num2str(tEq(i)) idRM];
            end
            if lcdm~=1 %Points with constant intermediate exp. rates
              idIns=['_eraB' num2str(eraB(i)) idRM];
            end
            
            %Rescalings

            if(isnumeric(tOffset))
                tOffSet=tOffset;
            end
            %Get tOffset from statsFile if necessary
            if strcmp(tOffset,'*')==1
                disp(['** Getting tOffSet from statsFile Lag. fit for ' ...
                'tRef -> 2*tRef **'])
                tOffSet = statsFile(-1,id,runRM,150*[1 2],0.5,1024,pathCell{1}); % dx, N kludge
                tEq(i)=tEq(i)-tOffSet;
            end

            %Check to see if we want to rescale
            noscaling = 0;
            if strcmp(tOffset,'noscaling')==1
                tOffSet_temp=0;
                tOffSet = ones(1,nRunsRM)*tOffSet_temp;
                noscaling = 1;
            end
        
            %Get xiLag from statsFile 
            xiscaling = 0;
            if strcmp(tOffset,'xiscaling')==1
                disp(['** Scaling with xiLag'])
                [xiLag tStat] = statGet('xiLag',id,runRM,simpars,pathCell{1});
                A = statsFile(20,id,runRM,150*[1 2],simpars,pathCell{1}); % dx, N kludge
                B = statsFile(-1,id,runRM,150*[1 2],simpars,pathCell{1})
                xiEq(i)=A*(tEq(i)-mean(B))

                xiLagAv = mean(xiLag);

                tOffSet = ones(1,nRunsRM)*0;
                xiscaling = 1;
                tEq(i)=xiEq(i);
            end
            
            
            %%%%%%%%%%%%%%%%%
            %Load Correlators
            %%%%%%%%%%%%%%%%%
            [k_rm,t,C_rm,sd_rm]=ETCload(pathCell{1},Cname,id,runRM,tref_trans(i),tOffSet,tLimit_rm(1),tLimit_rm(2));
            
            
            %Save last ETC for the LCDM function
            if strcmp(num2str(tEq(i)),'1.33')==1
               C_last_133 = C_rm(end,:);
               if lcdm==1
                   for h = 1:size(C_rm,1)
                       factor_last = max(C_rm(end,:))/max(C_rm(h,:)); %Relative difference in the height of the peak
                       C_last(h,:) = factor_last * C_rm(h,:); %Correct lcdm ETC with previous factor
                   end
               end
            end
           
            %if lcdm~=1
            %    [kInst,tInst,C_rmInst,sd_rm]=ETCload(pathCell{1},Cname,idIns,runRM,tref_trans(i),tOffSet,150,150.5);
            %    t_teqInst(i)=tInst/tEq(i);
            %end
            
            if (xiscaling == 1)
                for l=1:size(C_rm,1)
                    xiScale = spline(tStat,xiLagAv,t(l));
                    if strcmp(Cname,'vector')~=1
                        C_rm(l,:) = xiScale*C_rm(l,:)/t(l);
                    else
                        C_rm(l,:) = C_rm(l,:)*(t(l)/xiScale);
                    end
                    t(l) = xiScale;
                end
            end
        
            t_rm(i,:)=t;
        
            C_Cell{i}=C_rm;
            
            %if lcdm~=1
            %    C_Inst{i}=C_rmInst;
            %end
            
            t_teq(i,:)=t_rm(i,:)/(tEq(i));
                
        end
   
        for l=1:2 %Load Rad Mat Scaling ETCs
        %Get tOffset from statsFile if necessary
            if strcmp(tOffset,'*')==1
                disp(['** Getting tOffSet from statsFile Lag. fit for ' ...
                'tRef -> 2*tRef **'])
                tOffSet = statsFile(-1,idCell{l},runs,tref(l)*[1 2],0.5,1024,pathCell{l+1}); % dx, N kludge
            end

            %Check to see if we want to rescale
            noscaling = 0;
            if strcmp(tOffset,'noscaling')==1
                disp('** Plotting against k and removing t factor from correlator')
                tOffSet_temp=0;
                tOffSet = ones(1,nRuns)*tOffSet_temp;
                noscaling = 1;
            end
        
            %Get xiLag from statsFile 
            xiscaling = 0;
            if strcmp(tOffset,'xiscaling')==1
                disp(['** Scaling with xiLag'])
                [xiLag tStat] = statGet('xiLag',idCell{l},runs,simpars,pathCell{l+1});
                if runs(end) > 1
                    xiLagAv = mean(xiLag,1);
                else
                    xiLagAv = mean(xiLag);
                end
                    tOffSet_temp=0;
                    tOffSet = ones(1,size(runs,2))*tOffSet_temp;
                    xiscaling = 1;
            end
            
            if l==1
                [k_m,t,C,sd]=ETCload(pathCell{2},Cname,idCell{1},runs,tref(1),tOffSet,tLimitm(1),tLimitm(2));
            elseif l==2
                [k_r,t,C,sd]=ETCload(pathCell{3},Cname,idCell{2},runs,tref(2),tOffSet,tLimitr(1),tLimitm(2));
            end
        
            if (xiscaling == 1)
                for q=1:size(C,1)
                    xiScale = spline(tStat,xiLagAv,t(q));
                    if strcmp(Cname,'vector')~=1
                        C(q,:) = xiScale*C(q,:)/t(q);
                    else
                        C(q,:) = C(q,:)*(t(q)/xiScale);
                    end
                    t(q) = xiScale;
                end
            end
        
            if l==1
                C_m=C;
                t_m=t;
            else
                C_r=C;
                t_r=t;
            end
        end
        
hold on
 
    %Range of k used for the calculation of the interp. function
    k_new=0.5:0.005:2.0;
    
        %Irrelevant in s=0, same t_min and t_max in rad and mat
        min_t=max(t_m(1),t_r(1));
        max_t=min(t_m(end),t_r(end));
    
        t_temp=t_rm(i,:);
    
        t_new=t_m;
    
        %Interpolations to t_new and k_new
        for l=1:size(C_m,2)
            C_m_timeint(:,l)=interp1(t_m,C_m(:,l),t_new);   
        end
        for l=1:size(C_r,2)
            C_r_timeint(:,l)=interp1(t_r,C_r(:,l),t_new);
        end
        
        if lcdm==1
            for l=1:size(C_last,1)
            C_last_int(l,:) = interp1(k_rm,C_last(l,:),k_new);
            end
        end
        
        for l=1:size(C_m,1)
            C_m_int(l,:)=interp1(k_m,C_m_timeint(l,:),k_new);
        end
        for l=1:size(C_r,1)
            C_r_int(l,:)=interp1(k_r,C_r_timeint(l,:),k_new);
        end
        

figure()
for i=1:size(tEq,2)
            
        C_rm=[];
        C_rmInst=[];
        C_rm_int=[];
        f_t=[];
        Mean_f_t=[];
        StD_f_t=[];
        StD_f_t_temp=0;
        f_LAH_lcdm=[];
        
        
        %Interpolate Rad/Mat scaling functions to Matter t's
        C_rm=C_Cell{i};
        
        for l=1:size(C_rm,2)
            C_rm_timeint(:,l)=interp1(t_temp,C_rm(:,l),t_new);   
        end
            
        if lcdm==1
            t_tEqualty{i}=t_new(:)/(300/tEq(i)); %tau/tau_0
            factor_tEq_tNow = (0.00796)^(-1);
        else
            t_tEqualty{i}=t_new(:)/(tEq(i));
      %      C_rmInst=C_Inst{i};
        end
   
        %Interpolate transition correlators to selected k's
        for l=1:size(C_rm,1)
            C_rm_int(l,:)=interp1(k_rm,C_rm_timeint(l,:),k_new);   
        end
        
        %if lcdm~=1
        %    for l=1:size(C_rmInst,1)
        %        C_rmInst_int(l,:)=interp1(kInst,C_rmInst(l,:),k_new);
        %    end
        %end
        
        %Calculate interpolation function
        for l=1:size(t_new,1)
                if lcdm==1
                    f_LAH_lcdm(l) = (1+(1/4)*(t_new(l)/(300/tEq(i))*factor_tEq_tNow))^-1;
                    C_rm_trans(l,:) = f_LAH_lcdm(l)*C_r_int(l,:) + (1-f_LAH_lcdm(l))*C_m_int(l,:);
                    f_t(l,:)= ( (C_rm_int(l,:) - C_last_int(l,:) )) ./ ( C_rm_trans(l,:) - C_last_int(l,:) );
                else
                    f_t(l,:)= ( C_rm_int(l,:) - C_m_int(l,:) ) ./ (C_r_int(l,:) - C_m_int(l,:));
                end
            
            Mean_f_t(l)=mean(f_t(l,:));
            StD_f_t(l)=std(f_t(l,:));
        end
        
        Mean{i}=Mean_f_t;
        StD{i}=StD_f_t;
    
        %if lcdm~=1
        %    for j=1:size(C_rmInst_int,2)
        %        f_t_Inst(j)= ( C_rmInst_int(1,j) - C_m_int(1,j) ) / (C_r_int(1,j) - C_m_int(1,j));
        %    end
        %    Mean_f_t_Inst(i)=mean(f_t_Inst);
        %    StD_ft_Inst(i)=std(f_t_Inst);
        %end

%Plotting

tt=[];



for r=1:size(t,1)
tt=[tt,t(r,1)];
end

kk=k_new';

tt=tt';

figure(5)
surf(kk,tt,f_t); hold on;
ylabel('t');
xlabel('k');


end


for i=1:size(tEq,2)
    figure(2)
    t_tEq=[];
    Mean_f_t=[];
    StD_f_t=[];
    t_tEq=t_tEqualty{i};

    plotMean=Mean{i};
    plotStd=StD{i};
  
    plot(t_tEq,plotMean,'LineWidth',2,'Color',colour); hold on;
    
    X=[t_tEq(1:end-1) t_tEq(1:end-1) t_tEq(2:end) t_tEq(2:end)];
    Y=[plotMean(1:end-1)'-1*plotStd(1:end-1)' plotMean(1:end-1)'+1*plotStd(1:end-1)' plotMean(2:end)'+1*plotStd(2:end)' plotMean(2:end)'-1*plotStd(2:end)'];
    Y2=[plotMean(1,1:end-1)'-2*plotStd(1:end-1)' plotMean(1:end-1)'+2*plotStd(1:end-1)' plotMean(2:end)'+2*plotStd(2:end)' plotMean(2:end)'-2*plotStd(2:end)'];
    
    patch(X',Y2',[0.8 0.8 0.8],'EdgeColor',[0.8 0.8 0.8])
    %patch(X',Y2',[0 0 0],'EdgeColor',[0 0 0])
    patch(X',Y',[0.4 0.4 0.4],'EdgeColor',[0.4 0.4 0.4])
    hold on;
%    plot(t_tEq,Mean_f_t,'LineWidth',2,'Color',colour); hold on;
    %plot(Mean_f_t);hold on;
    %plotErrorBars(t_tEq(10:10:end),plotMean(10:10:end),plotStd(10:10:end),colour);hold on;
%     plot(t_tEq,f_ON(i,:),'k');hold on;
%     plot(t_tEq,f_LAH(i,:),'r');hold on;
    %plot(t_tEq,f_neil(i,:),'g');hold on;
%     scatter(t_teqInst(i),Mean_f_t_Inst(i),'k','filled');hold on;
%     plotErrorBars(t_teqInst(i),Mean_f_t_Inst(i),StD_f_t_Inst(i),'k');
    set(gca,'Xscale','log')
    set(gca,'YLim',[0 1])
end

    %if lcdm~=1
    %    scatter(t_teqInst,Mean_f_t_Inst,'k','filled');hold on;
    %    plotErrorBars(t_teqInst,Mean_f_t_Inst,StD_ft_Inst,'k');hold on;
    %end

    if Fit==1
     
     time=[];
     fitfunction=[];
     
     %%Egin bektore bat mean eta bste bat denborarekin, dena fiteatzeko
     %%funtzioa lortzeko
     for i=1:size(tEq,2)
         if lcdm==1
            if ne(i,1)
                tequ_end=tequ(end);
            end
         end
         tequ=t_tEqualty{i};
         mean_fun=Mean{i};
         if lcdm==1
            if ne(i,1)
                whicht = find (tequ > tequ_end);
                tequ=tequ(whicht);
                mean_fun=mean_fun(whicht);
            end
         end
         for j=1:size(tequ,1)
            time(end+1)=tequ(j);
            fitfunction(end+1)=mean_fun(j);
         end
         if lcdm==1
                which_tnow = find (time<1.2);
                time=time(which_tnow)
                fitfunction=fitfunction(which_tnow);
            end
         end

    
    
    if lcdm~=1
        s = fitoptions('Method','NonlinearLeastSquares','Lower',[0,-3],'Upper',[0.5,1]);
        f = fittype('(1+a*x)^b','options',s);
    else
        s = fitoptions('Method','NonlinearLeastSquares','Lower',[-1,4],'Upper',[0.5,8]);
        f = fittype('(1+a*x^b)','options',s);
    end
    [c2,gof2] = fit(time',fitfunction',f)
    plot(c2,colour);hold on;
    end
    
    if lcdm~=1
        time_teq=0.1:0.001:100;
    
        f_LAH=((1+(1/4)*time_teq).^-1);
        f_ON=((1+(1/4)*time_teq).^-2); %Fenu et al.
        a=(((2^(1/2) - 1) * time_teq + 1).^2 - 1); %LAH 2010
        f_neil=(1+a).^(-2);
    
        plot(time_teq,f_LAH,'r','LineWidth',1.5);hold on;
        plot(time_teq,f_ON,'--b','LineWidth',1.5);hold on;
        plot(time_teq,f_neil,'-.k','LineWidth',1.5);hold on;
        set(gca,'XLim',[time_teq(1) time_teq(end)])
        set(gca,'Xscale','log')
        set(gca,'YLim',[0 1])
    end
  
end
   