function [kt,r,C]=UETCtimeOffSet(id,run,Cname,kt,r,C,tRef,tOffSet,path,simpars,tLimit)
%UETC scaling function for UETC.hpp data 2006-2013
%
% Usage: [kt,r,C]=UETCtimeOffset(Cname,kt,r,C,tRef,tOffset)
%
%  Cname = UETC name, eg. scalar11 or vector
%     kt = wavenumber*time
%      r = time'/time
%      C = Correlator array
%   tRef = UETC reference time
%tOffset = time when xi=0, if '*' get from statsFile (Lag & tRef<t<2*tRef)
%
% 13.10.04 MBH tOffset now performed for each run rather than on average
%
%Can now handle data files without runID
% simpars - vector containing
%    dx = lattice spacing (only needed for winding xi)
%     N = lattice size (only needed for winding xi)
%  beta = scalar mass^2/vector mass^2
% ... (vector can be empty vector [])
%        NEW 2016 - can be a Map container with key and value
%  tLimit = vector with times between which to plot
% Version 2016.05.11 ALE, MBH
%    Start using Map container class for passing simpars
%   Allow the '*' and '*W' options, note that there are more arguments
%   'xiscaling' is implemented


nRuns=size(run,2);


%Get tOffset from statsFile if necessary
if strcmp(tOffSet,'*')==1
disp(['** Getting tOffSet from statsFile Lag. fit for ' ...
      'tRef -> 2*tRef **'])
tOffSet = statsFile(-1,id,run,tRef*[1 2],simpars,path); % Kludge to get path in
end

if strcmp(tOffSet,'*W')==1
disp(['** Getting tOffSet from statsFile Winding. fit for ' ...
      'tRef -> 2*tRef **'])
tOffSet = statsFile(-2,id,run,tRef*[1 2],simpars,path); % dx, N kludge
end


%Get xiLag from statsFile
xiscaling = 0;
if strcmp(tOffSet,'xiscaling')==1
    disp(['** Scaling with xiLag'])
    [xiLag tStat] = statGet('xiLag',id,run,simpars,path);
    if nRuns > 1
        xiLagAv = mean(xiLag,1);
    else
        xiLagAv = xiLag;
    end
tOffSet = 0;
xiscaling = 1;
end


%Adjust times
tSim=tRef*r;
tSimRef=tRef;
t=tSim-tOffSet;
tRef=tRef-tOffSet;

%Form outputs
r=t./tRef;
kt=kt.*tRef/tSimRef;

if (xiscaling == 1)
    %for i=1:size(C,1)
    xiScale = spline(tStat,xiLagAv,t);
    tFitLow = 0.5*(tLimit(1)+tLimit(2));
    tFitHigh = tLimit(2);
    tSub=tStat(tStat>=tFitLow & tStat<=tFitHigh);
    xiSubAv=xiLagAv(tStat>=tFitLow & tStat<=tFitHigh);
    pLag=leastSquares(tSub,xiSubAv');
    xiSlope=pLag(1);
    disp(['xi lag slope mean: ' num2str(mean(xiSlope))])    %end
                  
end
                  
if (xiscaling == 1)
    %Scale UETC with Xi(tRef) and Xi(t')
    %t = xiScale/xiSlope;
    if strcmp(Cname,'vector')~=1
        %C=C.*repmat(sqrt((xiScale./(xiSlope*t)))*sqrt((xiScale(1)/(xiSlope*tRef))),size(kt));
        C=C.*repmat(sqrt((xiScale./(t)))*sqrt((xiScale(1)/(tRef))),size(kt));
                                    
    else
        C=C.*repmat(sqrt(((xiSlope*t)./xiScale))*sqrt(((xiSlope*tRef)/xiScale(1))),size(kt));
    end
    t = xiScale;
    kt=kt.*t(1)/tRef;
                                     
else
    if nargout>2
        if strcmp(Cname,'vector')~=1
            C=C.*repmat(sqrt(t./tSim)*sqrt(tRef/tSimRef),size(kt));
        else
            C=C.*repmat(sqrt(tSim./t)*sqrt(tSimRef/tRef),size(kt));
            disp('** This version has correct vector tOffset corretion **')
        end
    end
end

