% extract a stat from statsFile
% 2013 Version for comparing runs at different resolution
%
%Usage: [stat, t] = statGet(statName,id,run,path,simpars,verbosity)
%
% path = cell array of paths to file, including final '/'
%        (if omitted gets paths from gpath global variable)
% simpars - vector containing
%    dx = lattice spacing (only needed for winding xi)
%     N = lattice size (only needed for winding xi)
%  beta = scalar mass^2/vector mass^2
% ... (vector can be empty vector [])
%        NEW 2016 - can be a Map container with key and value
%
% Version 2016.05.11 ALE, MBH
%    Plotting for ON stasFiles (number of columns = 13)
%    Start using Map container class for passing simpars
%
% Version 2016.08.27 MBH
%   Now returns more kinds of velocity and xi estimators, corrected for 
%   lagrangian weighting formula mistakes (only minor differences found)
%
% Version 2017.07.29 MBH
%   Scaled all dimensional quantities with phi0, vev of (complex) field.

function [stat, t] = statGet(statName,id,run,simpars,inPath,verbosity)

if nargin==0; 
  help statGet
  return
end

global gpath

if ~exist('verbosity','var'); verbosity='verbose'; end 


if ~exist('inPath','var'); inPath=''; end 

if numel(inPath)>0; 
  if ~ischar(inPath) 
      disp('inPath must be a string')
      return
  end
  path=inPath; 
else
  if numel(gpath)>0
  if ~ischar(gpath) 
      disp('gpath must be a string')
      return
  end
    path=gpath;
  else
    disp(['Please set gpath global variable to default path(s)'...
	  ' or specify path(s) in function call'])
    return
  end
end

sB = 0; % Core growth parameter
beta = 1;
phi0 = 1; 
q = 1;
lambda = 2;

% %%% simpars overrides
if isa(simpars,'containers.Map')
    if (strcmp(verbosity,'verbose'))
        disp('simpars is a container')
    end
    if isKey(simpars,'dx'); dx = simpars('dx'); end
    if isKey(simpars,'N'); N = simpars('N'); end
    if isKey(simpars,'lambda'); lambda = simpars('lambda'); end
    if isKey(simpars,'q'); q = simpars('q'); beta = 0.5*lambda/q^2; end
    if isKey(simpars,'beta'); beta = simpars('beta'); end  % if specified directly
    if isKey(simpars,'sB'); sB = simpars('sB'); end
    if isKey(simpars,'m_gauge'); m_gauge = simpars('m_gauge'); end
    if isKey(simpars,'vev'); phi0 = simpars('vev'); end % Complex field vev
else
    if (strcmp(verbosity,'verbose'))
        disp('simpars is a vector')
    end
    if numel(simpars)>3
        sB = simpars(4);
    end
    if numel(simpars)>2
        beta = simpars(3);
    end
    if numel(simpars)>1
        N = simpars(2);
    end
    if numel(simpars)>0
        dx = simpars(1);
    end
end

vol = (phi0*dx*N)^3;

if (strcmp(verbosity,'verbose'))
    disp(['Current working path: ' path])
end

%======================
%Form num2str ID string
%======================
s=strfind(id,'%');
if numel(s) > 0
    ID=[id(1:s+1) '.' id(s+1) 'i' id(s+2:end)];
else
    ID = id;
end
    
    
%==============
%Loop over runs
%==============
for r=1:numel(run)

  %=========
  %Read file
  %=========
  if numel(s) > 0
      file=[path 'statsFile' num2str(run(r),ID) '.dat'];
  else
      file=[path 'statsFile' ID '.dat'];
  end
  if (strcmp(verbosity,'verbose'))
      disp(' ')
      disp(['Loading file: ' file]); 
  end
  stats=load(file);

%  disp(['File size: ' num2str(size(stats))])
  
  sizes=size(stats);
  if (strcmp(verbosity,'verbose'))
      disp(['File size: ' num2str(sizes  )])
  end
  
  if ~exist('stat','var')
      stat = zeros(numel(run),sizes(1));
  end
  
  if sizes(2)==21
  %====================
  %New LAH files
  %====================
      t=          phi0*stats(:,1);
      a=          stats(:,2);
      q=          stats(:,3);
      modPhi=     stats(:,4)/phi0;
      modPi=      stats(:,5)/phi0^2;
      modFij=     stats(:,6)/phi0^2;
      modF0i=     stats(:,7)/phi0^2;
      gaussCD=    stats(:,8)/phi0^3;
      gaussDE=    stats(:,9)/phi0^3;
      eF0i=       stats(:,10)/phi0^4;
      eF0i_lag=   stats(:,11)/phi0^4;
      eFij=       stats(:,12)/phi0^4;
      eFij_lag=   stats(:,13)/phi0^4;
      ePi=        stats(:,14)/phi0^4;
      ePi_lag=    stats(:,15)/phi0^4;
      eDjPhi=     stats(:,16)/phi0^4;
      eDjPhi_lag= stats(:,17)/phi0^4;
      eV=         stats(:,18)/phi0^4;
      eV_lag=     stats(:,19)/phi0^4;
      SLexp=      phi0*stats(:,20);
      SLwind=     phi0*stats(:,21); 
      
      
  elseif sizes(2)==16

  %====================
  %Old LAH files
  %====================
      t=phi0*stats(:,1);
      a=stats(:,2);
      q=stats(:,3);
      modPhi=stats(:,4)/phi0;
      modPi=stats(:,5)/phi0^2;
      modFij=stats(:,6)/phi0^2;
      modF0i=stats(:,7)/phi0^2;
      gaussCD=stats(:,8)/phi0^3;
      gaussDE=stats(:,9)/phi0^3;
      eF0i=stats(:,10)/phi0^4;
      eFij=stats(:,11)/phi0^4;
      ePi=stats(:,12)/phi0^4;
      eDjPhi=stats(:,13)/phi0^4;
      eV=stats(:,14)/phi0^4;
      SLexp=phi0*stats(:,15);
      SLwind=phi0*stats(:,16);

    elseif sizes(2)==13

  %====================
  %ON files
  %=============
        t=phi0*stats(:,1);
        a=stats(:,2);
        q=stats(:,3); % really sqrt(lambda)
        modPhi=stats(:,4)/phi0;
        modPi=stats(:,5)/phi0^2;
        ePi=stats(:,6)/phi0^4;
        ePi_lag=stats(:,7)/phi0^4;
        eDjPhi=stats(:,8)/phi0^4;
        eDjPhi_lag=stats(:,9)/phi0^4;
        eV=stats(:,10)/phi0^4;
        eV_lag=stats(:,11)/phi0^4;
        SLexp=phi0*stats(:,12);
        SLwind=phi0*stats(:,13);

    elseif sizes(2)==17

  %====================
  %New ON files
  %=============
        t=phi0*stats(:,1);
        a=stats(:,2);
        q=stats(:,3); % really sqrt(lambda)
        modPhi=stats(:,4)/phi0;
        modPi=stats(:,5)/phi0^2;
        ePi=stats(:,6)/phi0^4;
        ePi_lag=stats(:,7)/phi0^4;
        eDjPhi=stats(:,8)/phi0^4;
        eDjPhi_lag=stats(:,9)/phi0^4;
        ePhiPi=stats(:,10)/phi0^4;
        ePhiPi_lag=stats(:,11)/phi0^4;
        ePhiDjPhi=stats(:,12)/phi0^4;
        ePhiDjPhi_lag=stats(:,13)/phi0^4;
        eV=stats(:,14)/phi0^4;
        eV_lag=stats(:,15)/phi0^4;
        SLexp=phi0*stats(:,16);
        SLwind=phi0*stats(:,17);

else
error('statsGet: stats file type not recognised')

end


  %===============================
  %Perform Additional Calculations
  %===============================

  % Get data

    if sizes(2)==13 | sizes(2)==17
        e=ePi+eDjPhi+eV;    %Energy density
        L=ePi-eDjPhi-eV;    %Lag. density
        p=ePi - eDjPhi/3 - eV; % Pressure
        eHig=ePhiPi+ePhiDjPhi+eV;    %Energy density
        LHig=ePhiPi-ePhiDjPhi-eV;    %Lag. density
        pHig=ePhiPi - ePhiDjPhi/3 - eV; % Pressure
    else
        e=eF0i+eFij+ePi+eDjPhi+eV;    %Energy density
        L=eF0i-eFij+ePi-eDjPhi-eV;    %Lag. density
        p=eF0i/3+eFij/3+ePi - eDjPhi/3 - eV; % Pressure
    end;

    % Calculate derived string parameters
    %
    rs = (a(end)*q(end)./(q.*a)); % string width relative to end
    beta = (a(end)*q(end))^2; % Recall that q is now sqrt(lambda/2), and assume that gauge coupling = 1.

    dx_eff = dx./rs;
    
    if ( sizes(2)==21 || sizes(2)==16 )        
        [mu,fB,fD,fV] = getMu(dx_eff);
        df = fB-fV;
        mu = mu*beta^0.195; %Abelian Higgs String energy per unit length (beta from Hill Hodges Turner 88)
        [mu_lag,fB_lag,fD_lag,fV_lag] = getMuLag(dx_eff);
        df_lag = fB_lag - fV_lag;
        mu_lag = mu_lag*beta^0.7./rs.^2;  % measured from static 3D solution
    else % sizes(2) == 16
        mu = pi;    %for global defects, real fields.Definition of mu (needs checking)
        %     mu_lag = 0.84*pi./rs.^2; % Lag-weighted, Calculated with relaxation code global_string.f
        mup   = 0.2839568;
        mup_V = 0.104466;
        mu_lag = mup*pi./rs.^2; % V-weighted, Calculated with relaxation code global_string.f
        fV_lag = mup_V/mup;
        df = 0; %Check
        df_lag = -fV_lag; 

    end

    % Get more data
    if sizes(2)~=13 && sizes(2)~=17
        T0iEst_gauge = sqrt(eF0i.*eFij); % Estimate of T0i gauge
        T0iEst_scalar = sqrt(ePi.*eDjPhi); % Estimate of T0i scalar
        T0iEst = sqrt(eF0i.*eFij) + sqrt(ePi.*eDjPhi); % Estimate of T0i
    end

    if sizes(2)==13 || sizes(2)==17
        T0iEst_scalar = sqrt(ePi.*eDjPhi); % Estimate of T0i scalar
    end
  
    if sizes(2)==21

      e_lag=eF0i_lag+eFij_lag+ePi_lag+eDjPhi_lag+eV_lag;    %Energy density
      L_lag=eF0i_lag-eFij_lag+ePi_lag-eDjPhi_lag-eV_lag;    %Lag. density
      p_lag=eF0i_lag/3+eFij_lag/3+ePi_lag - eDjPhi_lag/3 - eV_lag; % Pressure

      T0iEst_gauge_lag = sqrt(eF0i_lag.*eFij_lag); % Estimate of T0i gauge lag weighted
      T0iEst_scalar_lag = sqrt(ePi_lag.*eDjPhi_lag); % Estimate of T0i lag wighted
      T0iEst_lag = sqrt(eF0i_lag.*eFij_lag+ePi_lag.*eDjPhi_lag); % Estimate of T0i lag wighted
  
    end

    if sizes(2) == 13 || sizes(2)==17
        e_lag=(ePi_lag+eDjPhi_lag+eV_lag);    %Energy density
        L_lag=(ePi_lag-eDjPhi_lag-eV_lag);    %Lag. density
        p_lag=(ePi_lag - eDjPhi_lag/3 - eV_lag); % Pressure

        eHig_lag=(ePhiPi_lag+ePhiDjPhi_lag+eV_lag);    %Energy density
        LHig_lag=(ePhiPi_lag-ePhiDjPhi_lag-eV_lag);    %Lag. density
        pHig_lag=(ePhiPi_lag - ePhiDjPhi_lag/3 - eV_lag); % Pressure

        
        T0iEst_scalar_lag = sqrt(ePi_lag.*eDjPhi_lag); % Estimate of T0i lag wighted
    end


  % Calculate velocity estimators
  L_e_lag = L_lag./e_lag;
  w_lag = p_lag./e_lag;

  Rgrad = (ePi_lag)./(eDjPhi_lag); %201509 correction
  velGrad = 2*Rgrad./(1 + Rgrad);  %201509 correction

  velEOS = (1 + 3*w_lag - 2*df_lag)./(2 - df_lag.*(1+3*w_lag));
  velLag_lag = (1 + L_e_lag)./(1 - df_lag.*L_e_lag);

  if sizes(2)==21
      velGauge = (eF0i_lag) ./ (eFij_lag);
  end
  
  velLag = 1. + L./e;

    xiLag = sqrt(-mu./L);
    xiWind = 1./sqrt(SLwind/vol);
    xi_e_lag = 1./sqrt(-e_lag./mu_lag);
    xiInv = sqrt(-mu.*(1-velEOS)./L);
    xiInvLag = sqrt(-mu.*(1-velEOS)./L_lag);

     
if sizes(2)~=13 && sizes(2)~=17
switch statName
    case 't'
        stat(r,:)=stats(:,1);
    case 'a'
        stat(r,:)=stats(:,2);
    case 'q'
        stat(r,:)=stats(:,3);
    case 'modPhi'
        stat(r,:)=stats(:,4);
    case 'modPi'
        stat(r,:)=stats(:,5);
    case 'modFij'
        stat(r,:)=stats(:,6);
    case 'modF0i'
        stat(r,:)=stats(:,7);
    case 'gaussCD'
        stat(r,:)=stats(:,8);
    case 'gaussDE'
        stat(r,:)=stats(:,9);
    case 'eF0i'
        stat(r,:)=stats(:,10);
    case 'eF0i_lag'
        stat(r,:)=stats(:,11);
    case 'eFij'
        stat(r,:)=stats(:,12);
    case 'eFij_lag'
        stat(r,:)=stats(:,13);
    case 'ePi'
        stat(r,:)=stats(:,14);
    case 'ePi_lag'
        stat(r,:)=stats(:,15);
    case 'eDjPhi'
        stat(r,:)=stats(:,16);
    case 'eDjPhi_lag'
        stat(r,:)=stats(:,17);
    case 'eV'
        stat(r,:)=stats(:,18);
    case 'eV_lag'
        stat(r,:)=stats(:,19);
    case 'SLexp'
        stat(r,:)=stats(:,20);
    case 'SLwind'
        stat(r,:)=stats(:,21); 
    case 'e'
        stat(r,:)=e; 
    case 'p'
        stat(r,:)=p; 
    case 'L'
        stat(r,:)=L; 
    case 'e_lag'
        stat(r,:)=e_lag; 
    case 'p_lag'
        stat(r,:)=p_lag; 
    case 'L_lag'
        stat(r,:)=L_lag; 
    case 'xiLag'
        stat(r,:)=xiLag; 
    case 'xiWind'
        stat(r,:)=xiWind;
    case 'velGauge'
        stat(r,:) = velGauge;
    case 'velGrad'
        stat(r,:) = velGrad;
    case 'velEOS'
        stat(r,:) = velEOS;
    case 'velLag'
        stat(r,:) = velLag;
    case 'velLag_lag'
        stat(r,:) = velLag_lag;
    case 'xi_e_lag'
        stat(r,:)=xi_e_lag; 
    case 'xiInv'
        stat(r,:) = xiInv;
    case 'xiInvLag'
        stat(r,:) = xiInvLag;
    otherwise
        error(['statGet error: Unknown stat: ' statName])
end

elseif sizes(2)==13

    switch statName
    case 't'
        stat(r,:)=stats(:,1);
    case 'a'
        stat(r,:)=stats(:,2);
    case 'q'
        stat(r,:)=stats(:,3);
    case 'modPhi'
        stat(r,:)=stats(:,4);
    case 'modPi'
        stat(r,:)=stats(:,5);
    case 'ePi'
        stat(r,:)=stats(:,6);
    case 'ePi_lag'
        stat(r,:)=stats(:,7);
    case 'eDjPhi'
        stat(r,:)=stats(:,8);
    case 'eDjPhi_lag'
        stat(r,:)=stats(:,9);
    case 'eV'
        stat(r,:)=stats(:,10);
    case 'eV_lag'
        stat(r,:)=stats(:,11);
    case 'SLexp'
        stat(r,:)=stats(:,12);
    case 'SLwind'
        stat(r,:)=stats(:,13);    
    case 'e'
        stat(r,:)=e;
    case 'p'
        stat(r,:)=p;
    case 'L'
        stat(r,:)=L;
    case 'e_lag'
        stat(r,:)=e_lag;
    case 'p_lag'
        stat(r,:)=p_lag;
    case 'L_lag'
        stat(r,:)=L_lag;
    case 'xiLag'
        stat(r,:)=xiLag;
    case 'xiWind'
        stat(r,:)=xiWind;
    case 'velGauge'
        stat(r,:) = velGauge;
    case 'velGrad'
        stat(r,:) = velGrad;
    case 'velEOS'
        stat(r,:) = velEOS;
    case 'velLag'
        stat(r,:) = velLag;
    case 'velLag_lag'
        stat(r,:) = velLag_lag;
    case 'xi_e_lag'
        stat(r,:)=xi_e_lag; 
    case 'xiInv'
        stat(r,:) = xiInv;
    case 'xiInvLag'
        stat(r,:) = xiInvLag;
    otherwise
        error(['statGet error: Unknown stat: ' statName])
    end
    

elseif sizes(2)==17

switch statName
    case 't'
        stat(r,:)=stats(:,1);
    case 'a'
        stat(r,:)=stats(:,2);
    case 'q'
        stat(r,:)=stats(:,3);
    case 'modPhi'
        stat(r,:)=stats(:,4);
    case 'modPi'
        stat(r,:)=stats(:,5);
    case 'ePi'
        stat(r,:)=stats(:,6);
    case 'ePi_lag'
        stat(r,:)=stats(:,7);
    case 'eDjPhi'
        stat(r,:)=stats(:,8);
    case 'eDjPhi_lag'
        stat(r,:)=stats(:,9);
    case 'ePhiPi'
        stat(r,:)=stats(:,10);
    case 'ePhiPi_lag'
        stat(r,:)=stats(:,11);
    case 'ePhiDjPhi'
        stat(r,:)=stats(:,12);
    case 'ePhiDjPhi_lag'
        stat(r,:)=stats(:,13);
    case 'eV'
        stat(r,:)=stats(:,14);
    case 'eV_lag'
        stat(r,:)=stats(:,15);
    case 'SLexp'
        stat(r,:)=stats(:,16);
    case 'SLwind'
        stat(r,:)=stats(:,17);    
    case 'e'
        stat(r,:)=e;
    case 'p'
        stat(r,:)=p;
    case 'L'
        stat(r,:)=L;
    case 'e_lag'
        stat(r,:)=e_lag;
    case 'p_lag'
        stat(r,:)=p_lag;
    case 'L_lag'
        stat(r,:)=L_lag;
    case 'eHig'
        stat(r,:)=eHig;
    case 'pHig'
        stat(r,:)=pHig;
    case 'LHig'
        stat(r,:)=LHig;
    case 'eHig_lag'
        stat(r,:)=eHig_lag;
    case 'pHig_lag'
        stat(r,:)=pHig_lag;
    case 'LHig_lag'
        stat(r,:)=LHig_lag;
    case 'xiLag'
        stat(r,:)=xiLag;
    case 'xiWind'
        stat(r,:)=xiWind;
    case 'velGauge'
        stat(r,:) = velGauge;
    case 'velGrad'
        stat(r,:) = velGrad;
    case 'velEOS'
        stat(r,:) = velEOS;
    case 'velLag'
        stat(r,:) = velLag;
    case 'velLag_lag'
        stat(r,:) = velLag_lag;
    case 'xi_e_lag'
        stat(r,:)=xi_e_lag; 
    case 'xiInv'
        stat(r,:) = xiInv;
    case 'xiInvLag'
        stat(r,:) = xiInvLag;
    otherwise
        error(['statGet error: Unknown stat: ' statName])
end

end

end

t = t'; % Switch to match the stat array 

end