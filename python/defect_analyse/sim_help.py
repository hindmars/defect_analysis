#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 28 08:05:50 2023

Core growth time calculator.

Susceptibility time calculator.

Functions for checking params files and writing submission scripts

example:

    pattern = ['N', 'r', 's', 's2', 'lp', 'PhiLength', '-wall', 'WallColl', '-', '%np', '%nt', 'ts', 'tsrt', 'UETCstart', 'UETC start time']
    check_params_file('12krs1lp10-wallZero-np1nt8ts1075UETCstart2500-013-params.txt', 
                      pattern, 
                      nodes=1024, 
                      sub_file_template='sub_script_template_axcess.txt', 
                      write_new=True)


where 'sub_script_template_axcess.txt' is 

#!/bin/bash -l
#SBATCH --job-name=JJJ # Job name
#SBATCH --output=%x.o%j # Name of stdout output file
#SBATCH --error=%x.e%j # Name of stderr error file
#SBATCH --partition=standard-g  # Partition (queue) name
#SBATCH --nodes=NNN             # Total number of nodes 
#SBATCH --ntasks-per-node=8     # 8 MPI ranks per node, 8 total (2x8)
#SBATCH --gpus-per-node=8      # Allocate one gpu per MPI rank
#SBATCH --mem-per-gpu=60G      # Max mem per GPU
#SBATCH --exclude=nid006765,nid006730
#SBATCH --time=01:10:00       # Run time (d-hh:mm:ss)
#SBATCH --account=Project_462000084  # Project for billing


CPU_BIND="map_cpu:48,56,16,24,1,8,32,40"

export MPICH_GPU_SUPPORT_ENABLED=1

module load LUMI/23.03
module load partition/G
module load PrgEnv-cray
module load rocm
module load cray-hdf5-parallel
module load cray-fftw

EXEC_DIR=/projappl/project_462000084/WallString/src/build
PARAMS_DIR=/scratch/project_462000084/12kruns_lumi/

srun ${EXEC_DIR}/axhila -i ${PARAMS_DIR}/${SLURM_JOB_NAME}-params.txt 

# end file

Rules for a filename pattern:
- if list element is in parameter list, insert integer value of parameter. If it's divisible by 1024, shorten with 'k'.
- if preceded by %, interpret both as string and as parameter.
- if preceded by *, just take first character of parameter
- otherwise, pass literally.

@author: hindmars
"""

import os
import shutil
import numpy as np


def t_core_growth(t_diff, t_end, era=1., sa=None, sb=1.0, width_ratio=1.0):
    """
    

    Parameters
    ----------
    t_diff : float
        Time at end of diffusion when core growth starts.
    t_end : float
        Time at end of simulation.
    era : float, optional
        COnformal time power law index of scale factor, where 1.0 is radiation era
        and 2.0 is matter era. The default is 1..
    sa : float, optional
        Scale factor power law of inverse width in core growth era. If None, set 
        to 1/era, so that comoving width grwos linearly. The default is None.
    sb : float, optional
        Scale factor power law of inverse width in evolution era. If 1.0, the 
        comoving width decreases so that physical width is constant. The default is 1.0.
    width_ratio : float, optional
        The ratio of strring core widths in diffusion to that at the end of the simulation. 
        The default is 1.0.

    Returns
    -------
    t_cg : float
        Time at which core growth should end in order that the correct width ratio 
        is acheived at the end of the simulation.

    """
    
    if sa is None:
        sa = -1/era
    t_cg = t_end * (t_diff/t_end)**(sa/(sa-sb)) * width_ratio**(-1/(era*(sb - sa)))
    
    return t_cg


def t_star_min(n, t_end, mass_ratio_end=1.0, ws=1.0):
    """
    

    Parameters
    ----------
    n : float
        Power-law index of suspectibility with conformal time.
    t_end : float
        Conformal time at end of simulation.
    ws : float, optional
        Comoving string width at t_end. The default is 1.0.

    Returns
    -------
    t_star_min : float
        Mininum conformal time for t_star parameter. Determined from equality 
        Higgs mass and axion mass at t_end.

    """
    
    return t_end * (ws/t_end/mass_ratio_end)**(2/(n+4))


def t_end(N, dx, tdif):
    """Add half light-crossing time to diffusion time and round to nearest 50"""
    return np.floor((N*dx/2 + tdif)/50)*50

def width_ratio(t_diff, t_cg, t_end, era=1., sa=None, sb=1.0):
    """
    

    Parameters
    ----------
    t_diff : float
        Time at end of diffusion when core growth starts.
    t_cg : float
        Time at which core growth should end in order that the correct width ratio 
        is acheived at the end of the simulation.
    t_end : float
        Time at end of simulation.
    era : float, optional
        COnformal time power law index of scale factor, where 1.0 is radiation era
        and 2.0 is matter era. The default is 1..
    sa : float, optional
        Scale factor power law of inverse width in core growth era. If None, set 
        to 1/era, so that comoving width grwos linearly. The default is None.
    sb : float, optional
        Scale factor power law of inverse width in evolution era. If 1.0, the 
        comoving width decreases so that physical width is constant. The default is 1.0.

    Returns
    -------
    width_ratio : float, optional
        The ratio of strring core widths in diffusion to that at the end of the simulation. 

    """
    
    if sa is None:
        sa = -1/era
    
    width_ratio = (t_cg/t_diff)**(era*(sa - sb)) * (t_end/t_diff)**(era*sb)
    
    return width_ratio

def read_params_file(path_to_settings):
    """
    Reads in simulation parameters as a dictionary. Each line is assumed to be 
        parameter value
        parameter_word1 parameter_word2 ... value
    The last word is taken to be the value for the dictionary
    If the value can be converted to a float or an integer.
    Parameter words are concatenated to make the dictionary key
    Blank lines are ignored
    Characters after "#" are ignored
    """
    dictionary = {}
    with open(path_to_settings, "r") as file:
        for line in file:
            if len(line.strip()) > 0 and line.strip()[0] != '#':
                try:
                    # words = line.strip().split('#',1)[0].split()
                    # # key = words[0]
                    # key = ' '.join(words[:-1])
                    # value = words[-1]
                    key, value = get_key_value(line)
                    try:
                        v = int(value)
                    except ValueError:
                        try: 
                            v = float(value)
                        except:
                            v = value
                    dictionary[key] = v
                except:
                    pass
        
    return dictionary

def ax_higgs_mass_ratio_end(n, t_star, t_end, ws=1.0):
    ma_mh = (ws/t_end) * (t_end/t_star)**((n+4)/2)
    return ma_mh

def convert(value):
    
    value_string = ''
    suffix = ''
    if isinstance(value, int):
        if value%1024 == 0:
            value //= 1024
            suffix = 'k'
        value_string = str(value) + suffix
    elif isinstance(value, float):
        value_string = str(int(value))
    elif isinstance(value, str):
        value_string = value
    else:
        print('Value', value, ' is not an int, float or str.')

    return value_string

def make_output_jobname(params_dict, key_list):
    
    jobname = ''
    
    for key in key_list:
        if key[0:4] == 'UETC' and params_dict['UETC'] == 'off':
            print('make_output_jobname: key set as ', key, 'but UETC is off - skipping')
        else:
            if key in params_dict:
                value = params_dict[key]
                value_string = convert(value)
            elif key[0] == '%':
                key_strip = key[1:]
                if key_strip in params_dict:
                    value_string = key_strip + convert(params_dict[key_strip])
                else:
                    print('make_output_jobname: %key ' + key_strip + ' not in dictionary, skipped.')    
                    value_string = ''
            elif key[0] == '*':
                key_strip = key[1:]
                if key_strip in params_dict:
                    value_string = convert(params_dict[key_strip])[0]
                else:
                    print('make_output_jobname: *key ' + key_strip + ' not in dictionary, skipped.')    
                    value_string = ''
            else:
                print('make_output_jobname: key ' + key + ' not in dictionary, passed to jobname.')
                value_string = key
            jobname += value_string     
    
    seed_str = '-{:03d}'.format(int(params_dict['seed'])%100)
    
    jobname += seed_str
    
    return jobname

def check_params_file(pfile, key_list, sub_file_template=None, nodes=1024, 
                      write_new=False, pfile_suffix='-params.txt', ofile_suffix='.dat'):
    
    p = read_params_file(pfile)
    jobname = make_output_jobname(p, key_list)

    stats_output_path_list = p['output_file'].split('/')
    stats_output_filename = stats_output_path_list[-1]
    stats_next_dir_up = stats_output_path_list[-2]
    if stats_next_dir_up[0:5] != 'stats':
        print('Warning: stats_output_file dorectory', stats_next_dir_up,' is not "stats".')

    uetc_output_path_list = p['UETC file'].split('/')
    uetc_output_filename = uetc_output_path_list[-1]
    uetc_next_dir_up = uetc_output_path_list[-2]
    if uetc_next_dir_up[0:5] != 'UETC':
        print('Warning: uetc_output_file directory', uetc_next_dir_up,' is not "UETC".')
    uetc_output_filename_success = (uetc_output_filename == jobname)

    stats_output_filename_success = (stats_output_filename == jobname + ofile_suffix)
    
    if not stats_output_filename_success:
        print('Error: stats_output_file in params file', stats_output_filename, 
              'does not match generated:', jobname)
    else:
        print('stats_output_filename_success: stats_output_file in params file', stats_output_filename, 
              'matches generated:', jobname + ofile_suffix)

    if not uetc_output_filename_success:
        print('Error: uetc_output_file in params file', uetc_output_filename, 
              'does not match generated:', jobname)
    else:
        print('uetc_output_filename_success: uetc_output_file in params file', uetc_output_filename, 
              'matches generated:', jobname)

    params_filename_success = (pfile == jobname + pfile_suffix)

    if not params_filename_success:
        print('Error: params filename', pfile, 'inconsistent with "output_file" name', jobname + pfile_suffix)
        # if write_new:
        #     write_new_params_file(jobname, pfile)
    else:
        print('params_filename_success: params file', pfile, 'matches geberated:', jobname + pfile_suffix)        
    
    if write_new:
        write_new_params_file(jobname, pfile)
    
    params_file_is_good = stats_output_filename_success & uetc_output_filename_success & params_filename_success 
    
    if params_file_is_good and sub_file_template is not None:
        print('Writing submission script')
        write_submission_script(jobname, nodes=nodes, sub_file_template=sub_file_template)
    
    return stats_output_filename_success, uetc_output_filename_success, params_filename_success

def get_comment(line):
    words = line.split('#')
    return ''.join(words[1:])

def write_submission_script(jobname, 
                            nodes=1024, 
                            sub_file_template='sub_script_template_axcess.txt', 
                            params_dir=None):

    sub_file = 'sub_' + jobname + '.sh'    

    lines = []

    if params_dir is None:
        params_dir = jobname.split('k')[0] + 'kruns_lumi'

    with open(sub_file_template, 'r') as sft:
        for line in sft.readlines():
            if 'job-name' in line:
                if 'JJJ' in line:
                    line = line.replace('JJJ', jobname)
                else:
                    words = line.split('=')
                    line =  words[0] + '=' + jobname
                    if '#' in words[1]:
                        line += ' #' + get_comment(words[1])
                    # line += '\n'
            if 'nodes' in line:
                if 'NNN' in line:
                    line = line.replace('NNN', str(nodes))
                else:
                    words = line.split('=')
                    line =  words[0] + '=' + str(nodes)
                    if '#' in words[1]:
                        line += ' #' + get_comment(words[1])                    
                    # line += '\n'
                    
            if 'PARAMS_DIR=' in line:
                dirs = line.split('=')[-1].split('/')
                if dirs[-1] == '' or dirs[-1] == '\n':
                    del dirs[-1]
                old_params_dir = dirs[-1]
                if old_params_dir != params_dir:
                    line = line.replace(old_params_dir, params_dir)
                    print('write_submission_script: replaced', old_params_dir, 'with', params_dir)
            lines.append(line)
    
    if os.path.isfile(sub_file):
        shutil.copy(sub_file, sub_file + '~')
    
    with open(sub_file, 'w') as sf:
        sf.writelines(lines)

    return     

def get_key_value(line):
    words = line.strip().split('#',1)[0].split()
    key = ' '.join(words[:-1])
    value = words[-1]

    return key, value


def write_new_params_file(jobname, old_params_file, new_params_dict=None):

    params_file = jobname + '-params.txt'    
    right_stats_output_file = jobname + '.dat'
    right_uetc_output_file = jobname

    params_filename_is_wrong = params_file != old_params_file

    stats_output_file_replaced = False
    uetc_output_file_replaced = False

    lines = []

    print('starting write_new_params_file')
    with open(old_params_file, 'r') as pft:
        for line in pft.readlines():
            if len(line.strip()) > 0 and line.strip()[0] != '#':
                # words = line.strip().split('#',1)[0].split()
                # # key = words[0]
                # key = ' '.join(words[:-1])
                # value = words[-1]
                key, value = get_key_value(line)
                # print(key, value)
                if key == 'output_file':
                    old_output_file = value.split('/')[-1]
                    if old_output_file != right_stats_output_file:
                        line = line.replace(old_output_file, right_stats_output_file)
                        stats_output_file_replaced = True
                # line = line.replace('SSSS', seed)
                if key == 'UETC file':
                    old_output_file = value.split('/')[-1]
                    if old_output_file != right_uetc_output_file:
                        line = line.replace(old_output_file, right_uetc_output_file)
                        uetc_output_file_replaced = True
            lines.append(line)

    
    if params_filename_is_wrong or stats_output_file_replaced or uetc_output_file_replaced:
        print('write_new_params_file: Writing new params file', params_file)
        if os.path.isfile(params_file):
            shutil.copy(params_file, params_file + '~')
    
        with open(params_file, 'w') as pf:
            pf.writelines(lines)
    else:
        print('write_new_params_file: params file is consistent with jobname, no action taken')

    return None

def copy_modify_params_file(old_params_file, key_list, new_params_dict, auto_output_filenames=True, write_new=False):

    old_params_dict = read_params_file(old_params_file)
    
    params_dict = old_params_dict.copy()
    
    for key in new_params_dict:
        if key in old_params_dict:
            params_dict[key] = new_params_dict[key]
        else:
            print('copy_modify_params_file:', key, 'not in old_params_file')
    
    jobname = make_output_jobname(params_dict, key_list)
    
    print('copy_modify_params_file: jobname set as', jobname)
    
    params_file = jobname + '-params.txt'    
    right_stats_output_file = jobname + '.dat'
    right_uetc_output_file = jobname

    # params_filename_is_wrong = params_file != old_params_file

    stats_output_file_replaced = False
    uetc_output_file_replaced = False
    other_param_replaced = False
    lines = []

    # print('copy_modify_params_file: starting copy and modify:')
    with open(old_params_file, 'r') as pft:
        for line in pft.readlines():
            if len(line.strip()) > 0 and line.strip()[0] != '#':
                # words = line.strip().split('#',1)[0].split()
                # # key = words[0]
                # key = ' '.join(words[:-1])
                # value = words[-1]
                key, value = get_key_value(line)
                # print(key, value)
                
                if key in new_params_dict:
                    line = line.replace(value, str(new_params_dict[key]))
                    other_param_replaced = True
                if auto_output_filenames:
                    key, value = get_key_value(line)
                    if key == 'output_file':
                        old_output_file = value.split('/')[-1]
                        print('   old stats filename', old_output_file, 'new', right_stats_output_file)
                        if old_output_file != right_stats_output_file:
                            line = line.replace(old_output_file, right_stats_output_file)
                            stats_output_file_replaced = True
                    # line = line.replace('SSSS', seed)
                    if key == 'UETC file':
                        old_output_file = value.split('/')[-1]
                        print('   old UETC filename root', old_output_file, 'new', right_uetc_output_file)
                        if old_output_file != right_uetc_output_file:
                            line = line.replace(old_output_file, right_uetc_output_file)
                            uetc_output_file_replaced = True
            lines.append(line)

    

    if write_new:
        if stats_output_file_replaced or uetc_output_file_replaced or other_param_replaced:
            print('copy_modify_params_file: Writing new params file', params_file)
            if os.path.isfile(params_file):
                shutil.copy(params_file, params_file + '~')
        
            with open(params_file, 'w') as pf:
                pf.writelines(lines)
        else:
            print('copy_modify_params_file: old params file is consistent with modified parameters, no action taken')
    else:
        print('copy_modify_params_file: dry run, no action taken')
    return lines

# def replace_param_values(lines, new_params_dict):
    
#     for line in lines:
#         key, value = get_key_value(line)
#         if key in new_params_dict:
#             line = line.replace(value, new_params_dict[key])
#             print('replace_param_values: replaced param', key, 'value', value, 'with', new_params_dict )
    
#     return lines
