#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 29 10:31:22 2020

@author: hindmars
"""

import os
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as spi
import pandas as pd
from copy import deepcopy
from scipy.optimize import curve_fit

from . import defect_analyse_helper as dah

"""
A set of classes and functions for analysing data from Abelian Higgs code LAH and its 
descendants, including ON.  Uses pandas DataaFrames.
Developed from original Matlab scripts written by Neil Bevis and others.

Examples of use:

import defect_analyse_stats as das

id4ks0ell20 = '20r4096'

params4ks0ell20 = (id4ks0ell20+'-{:03d}',  '../../axions/4kruns/', 'id4ks0ell20 +'-{:03d}'+ '.sf')

sp_all = das.StatsFrame(*params4ks0ell20, runs=range(1,5))

sp_all.plot_xi()

Can ignore warning from pandas about 
UserWarning: Pandas doesn't allow columns to be created via a new attribute name - see https://pandas.pydata.org/pandas-docs/stable/indexing.html#attribute-access

"""

# Manhattan factor for winding length of strings (Klaer-Moore)
man_fac = 2/3
vev_symbol = '\phi_0'

# DEFAULT_GLOBAL_STRING_TENSION = 'continuum'
DEFAULT_GLOBAL_STRING_TENSION = 'lattice'



class StatsFrame(pd.DataFrame): 
    """
    A class for interacting with data from simulations. Based around the statsFile, which is a 
    (number of saves * number of data elements) array in an ascii file.
    Also reads in settings file, or can specify simulation parameters with a dictionary.
    """

    def __init__(self, *args, **kwargs):
        """
        Use *args so that input parameters can be either:
            id_str - string used to distinguish filenames for a run. 
            path_data - path to data file, name 'id_str+'.dat'
            settings - path to settings file, or a dictionary of settings. Must include
                    tEnd, N, dx, s, phi0.
            
        or a StatsFrame instance, so that a copy method can be executed:
        
        from copy import deepcopy
        # https://stackoverflow.com/questions/1241148/copy-constructor-in-python

        Aimed to construct
        
        - path to settings file
        - path to statsFile
        (- path to logFile)

        """

        if isinstance(args[0], StatsFrame):
            self.sfr_copy(args[0], *args[1:], **kwargs)
            return
        if isinstance(args[0], pd.DataFrame):
            self.from_dataframe(args[0], *args[1:], **kwargs)
            return
        elif len(args) == 3:
            id_str, path_data, settings = args
            if "runs" in kwargs and kwargs["runs"] is not None:
                for run in kwargs["runs"]:
                    ids = id_str.format(run)
                    this_kwargs = dah.deletekey(kwargs, "runs")
                    if type(settings) == str and '{:' in settings:
                        sf = settings.format(run)
                    else:
                        sf = settings
                    path = path_data.format(run)
                    if 'path_to_stats' in kwargs:
                        this_kwargs['path_to_stats'] = kwargs['path_to_stats'].format(run)
                        print('***', this_kwargs['path_to_stats'])
                    if run == kwargs["runs"][0]:
                        # self.construct_from_stats_data_file(ids, sf, path)
                        tmp_sfr = StatsFrame(ids, path, sf, **this_kwargs)
                    else: 
                        tmp_sfr = tmp_sfr.sfr_append(StatsFrame(ids, path, sf, **this_kwargs))
                self.__init__(tmp_sfr)
            else:
                self.construct_from_stats_data_file(id_str, path_data, settings, **kwargs)
        else:
            raise TypeError("StatsFrame: error: constructor arguments not recognised.\n")
        return


    def construct_from_stats_data_file(self, id_str, path_data, settings, **kwargs):
        """ 
        Assemble a DataFrame from simulation data and then convert to StatsFrame. 
        
        """
        if type(settings) == str:
            sf = dah.read_settings_file(path_data + settings)
        else:
            sf = settings

        if len(settings) == 0:
            raise ValueError('StatsFrame.__init__: error, settings dictionary is empty')

        if 'path_to_stats' not in kwargs:
            if "output_file" in sf: # Axhila
                path_to_stats = path_data + sf["output_file"]
            else:
                # rel_path_stats = ""
                # if "pathRoot" in sf:
                #     if "pathStats" in sf:
                #         rel_path_stats = sf["pathRoot"] + sf["pathStats"]
                pathRoot = dah.get_kwarg(sf, 'pathRoot', '')    
                pathStats = dah.get_kwarg(sf, 'pathStats', '')    
                rel_path_stats = dah.get_kwarg(kwargs, 'rel_path_stats', 
                                               pathRoot + pathStats)
                statsFile_prefix = dah.get_kwarg(kwargs, "statsFile_prefix", "statsFile")
                statsFile_type = dah.get_kwarg(kwargs, "statsFile_type", ".dat")
                
                path_to_stats = path_data + rel_path_stats + statsFile_prefix + \
                                id_str + statsFile_type
        else:
            path_to_stats = kwargs['path_to_stats']

        stats_data = np.loadtxt(path_to_stats)
        rows, cols = stats_data.shape
        print( 'StatsFrame: loaded data from ' + path_to_stats) 
        print(f'     {cols:} columns {rows:} rows')
        
        # might be better to load a row from stats file to calculate cols
        # before initialising parent dataframe
        if 'sim_code' in kwargs:
            sim_code = dah.get_kwarg(kwargs, "sim_code")
        else:
            sim_code = guess_sim_code(cols, sf)
        if sim_code == "unknown":
            raise ValueError('StatsFrame: sim_code not guessed or specified in kwargs.')
        df = pd.DataFrame(stats_data)
        df.columns = get_stats_data_col_names(sim_code) # must be first after initialisation
        df = fix_data(df, sim_code) # fixes some problems with output files (e.g. onsim in admas runs)
        df["stats_data_rows"] = rows      
        df["stats_data_cols"] = cols
        df["sim_code"] = sim_code
        print("     sim_code set as", df.sim_code.unique()[0])

        for k, v in sf.items():
            df[k] = v

        # self.set_sim_code(cols) # sim_code could also be set by a kwarg
        # if "onsim" in self.sim_code.unique():
        #     print("sim_code onsim detected, renaming SLexp column to modPhi2")
        #     self.rename(columns={"SLexp": "modPhi2"}, inplace=True)


        try:
            if df['nf'].all() == 2:
                df['is_string'] = True
            else:
                df['is_string'] = False
                
        except KeyError:
            df.nf = None
            df['is_string'] = True
        
        df['id_str'] = dah.get_kwarg(kwargs, "data_id_str", id_str)
        print(f'     id_str = {df["id_str"].unique()[0]:}')

        # df['id_str'] = id_str
        # df['settings'] = settings
        df['path_data'] = path_data
    
    
    
        try:
            s1 = sf['coreGrowthIndexA']
            s2 = -sf['coreGrowthIndexB']
            df['s2'] = s2
            df['s1'] = s1
        except KeyError:
            s1 = sf['s1']
            s2 = sf['s2']
            
        try:
            tcg = sf['tCoreGrowth']
            df['tcg'] = tcg
        except KeyError:
            tcg = sf['tcg']
        
        

        cg_era = df['t'] < tcg
        df['s'] = s1*np.int32(cg_era) +s2*np.int32(~cg_era) 
        
        try:
            df['phi0']=float(sf['sigma'])
        except:
            df['phi0']=1.0    
        df['vol'] = (df.N*df.dx)**3
        
        if 'LAH' in sim_code:
            df['m_higgs'] = np.sqrt(2)*df['root_lam_2']*sf['sigma']
        else:
            df['m_higgs'] = 2*df['root_lam_2']*sf['sigma']
        
        if 'Te' in df and 'chi' not in df:
            df['chi'] = df['Te']**(-df['nt'])
        if 'chi' in df and 'Te' not in df:
            df['Te'] = df['chi']**(-1/df['nt'])
        
        if 'chi' in df:
            df['m_ax'] = df['chi']**0.5/df['phi0']
        else:
            df['m_ax'] = df['m_higgs']*0.0
        
        df['t_cosmic'] = get_t_cosmic(df)
        df = scale_data(df, df.phi0)
        df = add_fluctuations(df)
        df = add_string_width(df)
        df = add_energy_pressure_lagrangian(df)
        df = add_velocity_estimators(df)
        df = add_length_estimators(df)
        if 'Nlink' in df and 'Awall' in df:
            df = add_area_estimators(df)

        self.from_dataframe(df)

        print(f'     StatsFrame initialised with {len(self.columns):} columns.')        
        print( '     For column names list use StatsFrame.all_columns()')        
        return

                                       
    def from_dataframe(self, other, *args, **kwargs):
        # construct a parent DataFrame instance and deepcopy it
        # it will miss any non-DataFrame atrributes
        # parent_type = super(StatsFrame, self)
        parent_type = super()
        parent_type.__init__(other)
        for k, v in other.__dict__.items():
            if hasattr(parent_type, k) and hasattr(self, k) and getattr(parent_type, k) == getattr(self, k):
                continue
            setattr(self, k, deepcopy(v))
        return


    def sfr_copy(self, orig, *args, **kwargs):
        # construct a new StatsFrame instance and deepcopy it
        # gives warning. Uncommenting extra doesn't help.
            # UserWarning: Pandas doesn't allow columns to be created via a new attribute name - see https://pandas.pydata.org/pandas-docs/stable/indexing.html#attribute-access
            # setattr(self, k, deepcopy(v))
        # orig_type = type(orig)
        # print(orig_type)
        for k, v in orig.__dict__.items():
            # if hasattr(orig_type, k) and hasattr(self, k) and getattr(orig_type, k) == getattr(self, k):
            #     continue
            setattr(self, k, deepcopy(v))
        return


    def sfr_append(self, other):
        # Uses pandas.DataFrame method
        return StatsFrame(pd.concat((self, other)))


    def sfr_mean(self):
        return StatsFrame(self.groupby(self.index).mean(numeric_only=True))


    def sfr_std(self):
        return StatsFrame(self.groupby(self.index).std(numeric_only=True))


    def __add__(self, other):
        
        return self.sfr_append(other)


    def all_columns(self):
        
        return self.columns.tolist()

        
    def _get_id_for_title(self):
        tit_str = os.path.commonprefix(list(self["id_str"].unique()))
        if len(self.id_str.unique()) > 1:
            tit_str += "*"
            
        return tit_str

    def _get_id_string(self):
        return self._get_id_for_title()

    def _get_title_string(self, line_break=False):   
        tit_str = self._get_id_for_title()
        if line_break:
            tit_str += '\n'
        tit_str += 'N={} dx={} s={}'.format(int(self.N.unique()[0]), 
                                                self.dx.unique()[0], 
                                                self.s.unique()[-1])  
        return tit_str

    def multiplot(self, x_key, y_key, z_key=None, 
                  xfun=None, yfun=None, ax=None, 
                  mean_std=False, 
                  x_range=[-np.inf, np.inf], 
                  y_range=[-np.inf, np.inf], 
                  z_range=[-np.inf, np.inf], 
                  **kwargs):
        """
        Plot data from all runs in a StatsFrame, but with only only one label

        Parameters
        ----------
        x_key : string
            key for specifying x data. The default is "t".
        y_key : string
            key for specifying y data.
        z_key : string
            key for specifying optional data for transformation of y data by yfun.
        yfun : function handle, optional
            Transformation f(x,y,[z]) to perform on y data. The default is None.
        ax : axes
            axes object for plotting. New axes created if not present.
        **kwargs : dict
            arguments to pass to matplotlib plot function.

        Returns
        -------
        ax : axes object
            

        """
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))
        
        if y_key not in self.columns:
            print('defect_analyse.multiplot: key ' + y_key + ' not found.')
        else:
            plot_kwargs = dict(kwargs)
            x_list = []
            y_list = []
            
            for name,  group in self.groupby(self.id_str):
                if yfun is not None:
                    if z_key is None:
                        y = yfun(group[x_key], group[y_key])
                    else:
                        y = yfun(group[x_key], group[y_key], group[z_key])                    
                else:
                    y = group[y_key]

                if xfun is not None:
                    if z_key is None:
                        x = xfun(group[x_key], group[y_key])
                    else:
                        x = xfun(group[x_key], group[y_key], group[z_key])                    
                else:
                    x = group[x_key]

                # x = group[x_key]
                inrange_x = np.logical_and(x > x_range[0], x< x_range[1])
                inrange_y = np.logical_and(y > y_range[0], y< y_range[1])
                inrange = np.logical_and(inrange_x, inrange_y)
                if z_key is not None:
                    z = group[z_key]
                    inrange_z = np.logical_and(z > z_range[0], z < z_range[1])
                    inrange = np.logical_and(inrange, inrange_z)
                    
                x_list.append(x[inrange])
                y_list.append(y[inrange])
 
            if 'xlim' in plot_kwargs:
                ax.set_xlim(plot_kwargs['xlim'])
                plot_kwargs = dah.deletekey(kwargs, "xlim")
            if 'ylim' in plot_kwargs:
                ax.set_ylim(plot_kwargs['ylim'])
                plot_kwargs = dah.deletekey(kwargs, "ylim")
            if 'err_every' in kwargs:
                err_every = kwargs['err_every']
                plot_kwargs = dah.deletekey(kwargs, "err_every")
            else:
                err_every = 5
            if not mean_std:
                for x, y in zip(x_list, y_list):
                    h, = ax.plot(x, y, **plot_kwargs)
                    plot_kwargs = dah.deletekey(kwargs, "label")
            else:
                dah.plot_mean_std_xy(x_list, y_list, ax, err_type='y', 
                                     err_every=err_every, **plot_kwargs)
                    
        return ax

    def dplot(self, x, y, fig_save_suffix=None, title=True, ax=None, **kwargs):
        """Equivelant to dfplot, included for backward compatibility"""
        return self.dfplot(x, y, fig_save_suffix, title, ax, **kwargs)

    def dfplot(self, x, y, fig_save_suffix=None, title=True, ax=None, **kwargs):
        """
        Plots one data series against another with pandas.DataFrame.plot, 
        and annotates.
        x, y are either indices for arrays, or strings giving names of variables.
        fig_save_suffix - if present, saves figure in format given by last 4 chars (e.g. .pdf)
        title - whether or not to have a title.
        **kwargs - keyword arguments to be passed to plot method.
        """

        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))
        else:
            fig = ax.get_figure()

        for name,  group in self.groupby(self.id_str):
            group.plot(x, y, ax=ax, **kwargs)
            
        ax.grid()
        # fig = ax.get_figure()
        
        # ax.legend(loc='best')
        ax.get_legend().remove()
        
        if type(x) is int:
            x_name = self.columns[x]
        else:
            x_name = x
        if type(y) is int:
            y_name = self.columns[y]
        else:
            y_name = y

        if title:
            ax.set_title(self._get_title_string() + " " + y_name)
        # else:
        #     ax.set_title('')

        fig.tight_layout()
        
        if not fig_save_suffix is None:
            fig.savefig(y_name + x_name + fig_save_suffix)

        return ax

    
    def plot_phase_plane(self, t_range = [-np.inf, np.inf], 
                         fig_save_suffix=None, ax=None, title=True, **kwargs):
        """
        (x,v) plot from mean of data.  Uses velEOS - this could be a choice.
        """

        
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,4))
        else:
            fig = ax.get_figure()

        t_start = t_range[0]
        t_end = t_range[1]
       
        data_mean = self.sfr_mean()
        t_period = np.where(np.logical_and(t_start < data_mean.t, data_mean.t < t_end))

        x = 4*data_mean.xi_r.loc[t_period]/data_mean.t.loc[t_period]
        v2 = data_mean.velGrad.loc[t_period]
        v2[v2<0] = np.nan
        v = np.sqrt(v2)
#        v = np.sqrt(self.velGrad[t_period])
        ax.plot(x, v, **kwargs)

        ax.set_xlabel(r'$x$')
        ax.set_ylabel(r'$v$')
        ax.grid()
        if title:
            ax.set_title(self._get_title_string())
        else:
            ax.set_title('')
        fig.tight_layout()
        
        return ax


    def plot_xi(self, ax=None, xi_type_list = ["xiWind", "xiLag", "xi_r"], t_fit_range=None, mean_std=False, 
                col_list = ['r', 'b', 'g'], legend=True, title=True, fig_save_suffix=None):
        """
        Plots defect separation xi against t
        t_fit_range - 2-sequence of times over which linear fit is performed
        fig_save_suffix - string, if present, saves figure in format given by last 4 chars (e.g. .pdf)
        """
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))
        else:
            fig = ax.get_figure()
        
        # p_lag_list = []
        # p_wind_list= []
        
        label_dict = dict(zip(["xiWind", "xiLag", "xi_r"], [r'$\xi_{\rm w}$', r'$\xi_{\cal L}$', r'$\xi_{\rm r}$']) )
        
        col_dict = dict(zip(xi_type_list, col_list))
        
        for xi_type, col in zip(xi_type_list, col_list):
            self.multiplot("t", xi_type, mean_std=mean_std, ax=ax, color=col, label=label_dict[xi_type])
        
            
        # self.multiplot("t", "xiWind", mean_std=mean_std, ax=ax, color='r', label=r'$\xi_{\rm w}$')
        # self.multiplot("t", "xiLag", mean_std=mean_std, ax=ax, color='b', label=r'$\xi_{\cal L}$')
        # self.multiplot("t", "xi_r", mean_std=mean_std, ax=ax, color='g', label=r'$\xi_{\rm r}$')

        
        if t_fit_range is not None:

            # for name,  group in self.groupby(self.id_str):
            p_lag_list, p_wind_list, p_xi_r_list = self.polyfit_xi(t_fit_range)
            # p_lag_list.append(p_lag)
            # p_wind_list.append(p_wind)

            p_lag_arr = np.array(p_lag_list)
            p_wind_arr = np.array(p_wind_list)
            p_xi_r_arr = np.array(p_xi_r_list)
            p_lag = np.mean(p_lag_arr, axis=0)
            p_wind = np.mean(p_wind_arr, axis=0)
            p_xi_r = np.mean(p_xi_r_arr, axis=0)
            
            line_lag = np.poly1d(p_lag)
            line_wind = np.poly1d(p_wind)
            line_xi_r = np.poly1d(p_xi_r)

            lines_dict = dict(zip(["xiWind", "xiLag", "xi_r"],[np.poly1d(p_wind), np.poly1d(p_lag), np.poly1d(p_xi_r)]))
            

            # It's a fit to the mean, so doesn't matter which time we choose
            run_ids = self.id_str.unique()
            t = self.t[self.id_str==run_ids[0]]

            print('max(t)', np.max(t))

            # fit_handles = []
            if "xiWind" in xi_type_list:
                ax.plot(t, line_wind(t), 'm--', zorder=10, 
                                       label=r'$\xi_{{\rm w}} = {:5.3f}\tau + {:4.1f}$'.format(*p_wind))
                # fit_handles.append(h_xiWind_fit)
            if "xiLag" in xi_type_list:
                ax.plot(t, line_lag(t),  'c--', zorder=10, 
                                      label=r'$\xi_{{\cal L}} = {:5.3f}\tau + {:4.1f}$'.format(*p_lag))
                # fit_handles.append(h_xiLag_fit)
            if "xi_r" in xi_type_list:
                ax.plot(t, line_xi_r(t),  color=col_dict["xi_r"], ls='--', zorder=10, 
                                     label=r'$\xi_{{\rm r}} = {:5.3f}\tau + {:4.1f}$'.format(*p_xi_r))
                # fit_handles.append(h_xi_r_fit)
    
        ax.set_xlim(0, max(self.tEnd))
        ax.set_ylim(0, max(0.5*self.tEnd))
        ax.set_ylabel(r'$\xi \eta$')
        
        # This is confusing
        if title:
            ax.set_xlabel(r'$\tau\eta$')

        # handles = [h_xiWind, h_xiLag]
        # legend_strings = [r'$\xi_{\rm w}$', r'$\xi_{\cal L}$']
        legend_strings = []
        handles = []
        if t_fit_range is not None:
            ax.plot([t_fit_range[0]]*2, ax.get_ylim(), 'k--', alpha=0.5)
            ax.plot([t_fit_range[1]]*2, ax.get_ylim(), 'k--', alpha=0.5)
            # ax.plot([t_fit_range[1]]*2, ax.get_ylim(), 'k--', label='Fit end')
            # handles = fit_handles + handles
            # legend_strings = [r'$\xi_{{\rm w}} = {:5.3f}t + {:5.3f}$'.format(*p_wind), 
            #           r'$\xi_{{\cal L}} = {:5.3f}t + {:5.2f}$'.format(*p_lag),
            #           'Fit start'] + legend_strings
    
        # ax.legend(handles, legend_strings, loc='upper left', fontsize='small', ncol=2)
        ax.grid(True)

        if legend:
            ax.legend(loc='lower right', fontsize='smaller')

        if title:
            ax.set_title(self._get_title_string())

        fig.tight_layout()
        
        if not fig_save_suffix is None:
            fig.savefig('xi' + fig_save_suffix)

        return ax


    # def plot_vel(self, ax=None, title=True, fig_save_suffix=None):
        
    #     if '_old' not in self.sim_code.unique()[0]:
    #         self._plot_vel(ax, title, fig_save_suffix)
    #     else:
    #         # raise UserWarning("StatsFrame: old stats data, no velocities to plot.")
    #         print("StatsFrame: old stats data, no velocities to plot.")

    #     return


    def plot_vel(self, ax=None, vel_type_list = ['velEOS', 'velLag_wt', 'velGrad'], 
                 mean_std=False, legend=True, title=True, fig_save_suffix=None):
        """
        Plots network mean square velocity against t
        fig_save_suffix - if present, saves figure in format given by last 4 chars (e.g. .pdf)
        """
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))
        else:
            fig = ax.get_figure()
        
        if 'velEOS' in vel_type_list:
            self.multiplot("t", "velEOS", yfun=(lambda x,y: np.sqrt(abs(y))), 
                           mean_std=mean_std, ax=ax, color='r', label=r"$v_{\rm eos}$")
        if 'velLag_wt' in vel_type_list:
            self.multiplot("t", "velLag_wt", yfun=(lambda x,y: np.sqrt(abs(y))), 
                           mean_std=mean_std, ax=ax, color='b', label=r"$v_{{\cal L},{\rm w}}$")
        if 'velGrad' in vel_type_list:
            self.multiplot("t", "velGrad", yfun=(lambda x,y: np.sqrt(abs(y))), mean_std=mean_std, 
                           ax=ax, color='g', label=r"$v_{\rm D}$")

#        ax.set_xlim(0,self.tEnd)
        ax.set_ylim(0,1)
        ax.set_ylabel(r'$v$')
    
        if legend:
            ax.legend(fontsize="smaller", loc='lower right')
        ax.grid()
        
        if title:
            ax.set_title(self._get_title_string())
            ax.set_xlabel(r'$\tau\eta$')

        fig.tight_layout()
        
        if not fig_save_suffix is None:
            fig.savefig('vel' + fig_save_suffix)

        return ax


    def plot_eq_funs(self, ax=None, legend=True, 
                     root_lam_2_fun=(lambda x,y: 1/y),
                     root_lam_2_label=r'$\sqrt{2/\lambda}$',
                     **plot_kwargs):
        
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))
        
        self.multiplot("t",  "a", ax=ax, color='b', label=r'$a$')
        self.multiplot("t",  "root_lam_2", yfun=root_lam_2_fun, 
                       ax=ax, color='r', label=root_lam_2_label)
        self.multiplot("t",  "ws", yfun=(lambda x,y: y*self.phi0.unique()), 
                       ax=ax, color='g', label=r'$w_s ' + vev_symbol + '$')
        if legend:
            ax.legend(fontsize='smaller')
        ax.grid()
        
        return ax


    def plot_density_params(self, ax=None, mean_std=False, legend=True, title=False, **kwargs):
        ax = self.plot_zeta(ax, mean_std, legend, title, **kwargs)
        # self.multiplot("t",  "e_wt", yfun=scale, ax=ax, color='b', label=r'$e_{\cal W} t^2/(\pi\phi_0^2)$', **kwargs)
        # self.multiplot("t",  "e", yfun=scale, ax=ax, color='b', label=r'$e t^2/(\pi\phi_0^2)$', **kwargs)
        return ax

    
    def plot_zeta(self, ax=None, mean_std=False, legend=True, title=False, **kwargs):
        
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))

        def scale(x,y):
            return y * x**2 / (np.pi*self.phi0.unique()**2) 
        def scale_wt(x,y):
            return y * x**2 / (np.pi*self.phi0.unique()**2)
        
        self.multiplot("t",  "zetaWind", mean_std=mean_std, ax=ax, color='r', label=r'$\zeta_{\rm w}$', **kwargs)
        self.multiplot("t",  "zeta_r", mean_std=mean_std, ax=ax, color='g', label=r'$\zeta_{\rm r}$', **kwargs)
        ax.set_ylabel(r'$\zeta$')
        if title:
            ax.set_title(self._get_title_string())
            ax.set_xlabel(r'$\tau\eta$')
        if legend:
            ax.legend(fontsize='smaller')
        if 'ylim' in kwargs:
            ax.set_ylim(kwargs['ylim'])
        else:            
            ax.set_ylim(0, 2*np.ceil(self['zetaWind'].iloc[-1]))
        ax.grid()

        return ax


    def plot_eos(self, ax=None, mean_std=False, legend=True, **kwargs):
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))

        self.multiplot("t",  "w", mean_std=mean_std, ax=ax, color='b', label=r'$p/e$')
        self.multiplot("t",  "w_wt", mean_std=mean_std, ax=ax, color='r', label=r'$p_{\cal W}/e_{\cal W}$')
        self.multiplot("t",  "w_higgs", mean_std=mean_std, ax=ax, color='g', label=r'$p_{h}/e_{h}$')
        self.multiplot("t",  "t", mean_std=mean_std, yfun=(lambda x,y: (1/3)*np.ones_like(y)), ax=ax, color='k', ls='--', label=r'$\pm 1/3$')
        self.multiplot("t",  "t", mean_std=mean_std, yfun=(lambda x,y: -(1/3)*np.ones_like(y)), ax=ax, color='k', ls='--')
        ax.set_ylabel(r'$\omega$')
        if legend:
            ax.legend(fontsize='smaller', loc="upper left")
        ax.set_ylim(-0.5, 0.5)
        ax.grid()
        return ax


    def plot_energy_fractions(self, ax=None, mean_std=False, legend=True, **kwargs):
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))

        self.multiplot("t",  "f_Pi", mean_std=mean_std, ax=ax, color='b', label=r"$e_\Pi/e$")
        self.multiplot("t",  "f_D", mean_std=mean_std, ax=ax, color='r', label=r"$e_D/e$")
        self.multiplot("t",  "f_V", mean_std=mean_std, ax=ax, color='g', label=r"$e_V/e$")
        
        # sc = self.sim_code.unique()[0]
        # if sc == "on" or sc == "onsim":
        #     self.multiplot("t",  "f_PhiPi", mean_std=mean_std, ax=ax, color='b', ls='--', label=r"$e_{\Pi}^{h}/e$")
        #     self.multiplot("t",  "f_PhiD", mean_std=mean_std, ax=ax, color='r', ls='--', label=r"$e_{D}^{h}/e$")
        #     self.multiplot("t",  "f_higgs", mean_std=mean_std, ax=ax, color='k', ls='--', label=r"$e^{h}/e$")
        for f_e, label_str, col in zip(["f_PhiPi", "f_PhiD", "f_higgs"],
                                     [r"$e_{\Pi}^h/e$",r"$e_{D}^h/e$","$e^h/e$"], 
                                     ['b', 'r', 'g']):
            if f_e in self.columns:
                self.multiplot("t",  f_e, mean_std=mean_std, ax=ax, color=col, ls='--', label=label_str)
            
        if legend:
            ax.legend(fontsize='smaller', ncol=2, loc="upper right")
        ax.set_ylabel(r'$e_x/e$')
        ax.set_ylim(0.0,1.0)
        ax.grid()

        return ax
    
    
    def plot_energy_fractions_wt(self, ax=None, mean_std=False, legend=True, **kwargs):
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))

        self.multiplot("t",  "f_Pi_wt", mean_std=mean_std, ax=ax, color='b', label=r"$e_{\Pi,{\cal W}}/e_{\cal W}$")
        self.multiplot("t",  "f_D_wt", mean_std=mean_std, ax=ax, color='r', label=r"$e_{D,{\cal W}}/e_{\cal W}$")
        self.multiplot("t",  "f_V_wt", mean_std=mean_std, ax=ax, color='g', label=r"$e_{V,{\cal W}}/e_{\cal W}$")
        # sc = self.sim_code.unique()[0]
        # if sc == "on" or sc == "onsim":
        #     self.multiplot("t",  "f_PhiPi_wt", mean_std=mean_std, ax=ax, color='b', ls='--', label=r"$e_{\Pi, {\cal W}}^h/e_{\cal W}$")
        #     self.multiplot("t",  "f_PhiD_wt", mean_std=mean_std, ax=ax, color='r', ls='--', label=r"$e_{D, {\cal W}}^h/e_{\cal W}$")
        #     self.multiplot("t",  "f_higgs_wt", mean_std=mean_std, ax=ax, color='k', ls='--', label=r"$e_{\cal W}^h/e_{\cal W}$")
        # sc = self.sim_code.unique()[0]
        for f_e_wt, label_str, col in zip(["f_PhiPi_wt", "f_PhiD_wt", "f_higgs_wt"],
                                     [r"$e_{\Pi, {\cal W}}^h/e_{\cal W}$",r"$e_{D, {\cal W}}^h/e_{\cal W}$","$e_{\cal W}^h/e_{\cal W}$"], 
                                     ['b', 'r', 'g']):
            if f_e_wt in self.columns:
                self.multiplot("t",  f_e_wt, mean_std=mean_std, ax=ax, color=col, ls='--', label=label_str)
            
        if legend:
            ax.legend(fontsize='x-small', ncol=2, loc="upper right")
        ax.set_ylabel(r'$e_{x, {\cal W}}/e_{\cal W}$')
        ax.set_ylim(0.0,1.0)
        ax.grid()

        return ax

    
    def plot_energy_decay(self, ax=None, mean_std=False, weighted=False, 
                          legend=True, **kwargs):
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))

        if 'color' in kwargs:
            col = kwargs['color']
        else:
            col = 'b'
            
        if 'label' in kwargs:
            lab = kwargs['label']
        else:
            if weighted: 
                lab = 'Weighted'
            else:
                lab = 'Total'
                
        
        
        
        self.multiplot("t",  "f_e_decay", mean_std=mean_std, ax=ax, color=col, label=lab)
        if weighted:
            self.multiplot("t",  "f_e_decay_wt", mean_std=mean_std, ax=ax, color='r', label=lab)
        # ymin = min(min(self.f_e_decay), min(self.f_e_decay_wt))
        # ax.set_ylim(dah.log_round(ymin), 1e1)
        # ax.set_ylabel(r'$t|\dot{e} + H (e+3p)~/e$', fontsize='smaller')
        # ax.set_ylabel(r'$|\dot{e} + H (e+3p) + X|/H e$', fontsize='smaller')
        ax.set_ylabel(r'$(\dot{e} - R)/{\rm max}(|\dot{e}|, |R|)$', fontsize='smaller')
        if legend:
            ax.legend(fontsize='smaller')
        ax.grid()
        
        if 'xlim' in kwargs:
            xlim = kwargs['xlim']
            ax.set_xlim(*xlim)
        if 'ylim' in kwargs:
            ylim = kwargs['ylim']
            ax.set_ylim(*ylim)
        
        return ax

    
    def plot_fluctuations(self, ax=None, mean_std=False, legend=True, **kwargs):
        if ax is None:
            fig, ax = plt.subplots(figsize=(5,3))
        
        yfun = lambda x,y: np.sqrt(np.abs(y))
        
        self.multiplot("t",  "dphi1", mean_std=mean_std, yfun=yfun, ax=ax, color='b', label=r"$\bar\phi = \langle|\phi|\rangle$" )
        self.multiplot("t",  "dphi2", mean_std=mean_std, yfun=yfun, ax=ax, color='r', label=r"$\bar\phi^2 = \langle|\phi|^2\rangle$" )
        ax.set_ylabel(r'$|\bar\phi^2 - ' + vev_symbol + '^2|^{1/2}/' + vev_symbol + '$')
        # ymin = min(min(abs(self.dphi1)), min(abs(self.dphi2)))
        # ax.set_ylim(dah.log_round(np.sqrt(ymin)),1.0)
        if legend:
            ax.legend(fontsize='smaller')
        ax.grid()
        return ax
        

    def dashboard(self, ax=None, mean_std=False, fig_save_suffix=None):
        """
        Plots a 3x3 grid of useful data from a statsFile, returns an axes handle
        
        [0,0] - a (scale factor), $1/\sqrt(\lambda)$ (inverse of sqrt of self-coupling), ws, comoving string width
        [0,1] - xi, mean string separation (Lagrangian, Manhattan-corrected winding length)
        [0,2] - energy fractions, string length density fraction
        [1,0] - Equation of state parameter (p/e) - ordinary, weighted.
        [1,1] - v, RMS string velocity (scalar, lagrangian, eqn of state measures)
        [1,2] - Field fluctuations from potential minmimum
        [2,0] - Eneergy fractions (kinetic, gradient, potential)
        [2,1] - Weighted energy fractions (kinetic, gradient, potential)
        [2,2] - Relative difference of left and right sides of energy conservation equation

        Returns
        -------
        ax : axes handle
            All the usual properties.

        """
        if ax is None:
            fig, ax = plt.subplots(3,3, sharex='col', figsize=(8,7))
        else:
            fig = ax[0,0].get_figure()
        
        # Sacle factor, coupling, string width
        self.plot_eq_funs(ax[0,0])

        # xi (mean string spacing)
        self.plot_xi(ax[0,1], title=False, mean_std=mean_std)

        # Density parameters
        self.plot_density_params(ax[0,2], mean_std=mean_std)
        # ax[0,2].set_yscale('log')
        ax[0,2].set_xscale('log')

        # Equation of state
        self.plot_eos(ax[1,0], mean_std=mean_std)
        
        # v (mean string spacing)
        self.plot_vel(ax[1,1], mean_std=mean_std, title=False)

        # Fluctuations
        self.plot_fluctuations(ax[1,2], mean_std=mean_std)
        ax[1,2].set_yscale('log')

        # Energy fractions
        self.plot_energy_fractions(ax[2,0], mean_std=mean_std)

        # Weighted energy fractions
        self.plot_energy_fractions_wt(ax[2,1], mean_std=mean_std)

        # Energy conservation
        self.plot_energy_decay(ax[2,2], mean_std=mean_std)
        ax[2,2].set_yscale('log')


        for col in range(0,3):
            ax[2,col].set_xlabel(r'$\tau' + vev_symbol + '$')
        fig.subplots_adjust(hspace=0)
        fig.suptitle(self._get_title_string())
        fig.tight_layout()
        
        if not fig_save_suffix is None:
            fig.savefig('db_' + fig_save_suffix)

        return ax


    def polyfit_xi(self, t_fit_range):
        """
        Performs linear fit to xi against t over the specifited range (2-tuple).
        Three different xi measures used: xiLag, xiWind, xi_r.
        Returns a tuple of parameter lists.
        """

        p_lag_list, p_wind_list, p_xi_r_list = [], [], []

        for id_str in self['id_str'].unique():
            p_lag, p_wind, p_xi_r = polyfit_xi_df(self[self['id_str'] == id_str], t_fit_range)
            p_lag_list.append(p_lag)
            p_wind_list.append(p_wind)
            p_xi_r_list.append(p_xi_r)

        
        return p_lag_list, p_wind_list, p_xi_r_list 

    def poly1d_xi(self, t_fit_range):
        """
        Performs linear fit to xi against t over the specifited range (2-tuple), 
        returning functions (poly1d objects).
        Three different xi measures used: xiLag, xiWind, xi_r.
        Returns a list of poly1d objects.
        Works only for single runs - should check number of unique id_str
        """
        p_tuple = self.polyfit_xi(t_fit_range)

        poly1d_list = []
        for p in p_tuple:
            poly1d_list.append(np.poly1d([*p]))

        return poly1d_list
        

    def t_offset(self, t_fit_range, fit_to='xiLag'):
        """
        Returns x axis intercept of xi (Lagrangian) against t
        Works only for single runs - should check number of unique id_str
        """
        # p_lag, p_wind = self.polyfit_xi(t_fit_range)
        p_tuple = self.polyfit_xi(t_fit_range)
        if fit_to == 'xiLag':
            p_list = p_tuple[0]
        elif fit_to == 'xiWind':
            p_list = p_tuple[1]            
        elif fit_to == 'xi_r':
            p_list = p_tuple[2]            

        t_off_list = []
        for p in p_list:
            t_off_list.append(- p[1]/p[0])
        
        if len(t_off_list) == 1:
            t_off = t_off_list[0]
        else:
            t_off = np.array(t_off_list)
        return np.array(t_off_list)
 
    def data_fit(self, fun, xcol, ycol, p0, bounds=None, x_range=[-np.inf, np.inf], mean_std=False):
        """Fits data in column xcol, ycol with function fun, in range x_range, 
        using scipy.optimize.curve_fit.  Parameters p0 and bounds are passed to 
        curve_fit.
        
        Returns: dictionary of fit parameters, with key id_str of StatsFrame. """
        
        st_g = self.groupby('id_str')
        
        popt_list = []
        copt_list = []
        id_list = []
                
        for id_g, g in st_g:
            st = StatsFrame(g)
            x = st[xcol]
            y = st[ycol]
            
            fit_interval = np.logical_and(x_range[0] < x, x < x_range[1])
            
            popt, copt = curve_fit(fun, x[fit_interval], y[fit_interval], 
                                   p0=p0)
            popt_list.append(popt)
            copt_list.append(copt.flatten())
            id_list.append(id_g)
        
        
        # return pd.DataFrame(zip(id_list, popt_list, copt_list), 
        #                     columns=['id_str', 'popt', 'copt'])
        
        id_df = pd.DataFrame(np.array(id_list), columns=['id_str'])
        popt_df = pd.DataFrame(np.array(popt_list), columns=['popt' + str(n) for n in range(len(popt_list[-1]))])
        copt_df = pd.DataFrame(np.array(copt_list), columns=['copt' + str(n) for n in range(len(copt_list[-1]))])
        
        return pd.concat((id_df, popt_df, copt_df), axis=1)#)
    
    def get_param(self, key):
        """Get a unique value for a simulation parameter, instead od a pandas Series."""
        return self[key].unique()[0]

# end class StatsFrame



def guess_sim_code(ncols, sf):

    if ncols == 17:
        if "output_file" in sf:
            sim_code = "onsim"
        else:
            sim_code = "on"                
        warn = ''
    elif ncols == 13:
        sim_code = "on_old"
        warn = "column headers not set"
    elif ncols == 21: # This is now both LAH and axhila
        sim_code = "LAH"
        warn = 'StatsFrame.guess_sim_code: warning: 21 columns counted, sim_code=LAH assumed.\n For axhila set kwarg sim_code="axhila".'
    elif ncols == 16:
        sim_code = "LAH_old"
        warn = ''
    elif ncols == 24:
        sim_code = "axhila_walltest"
        warn = ''
    elif ncols == 36:
        sim_code = "axhila_wall"
        warn = ''
    else:
        sim_code = "unknown"
        raise UserWarning("StatsFrame.guess_sim_code: warning: statsFile data file not understood.\n Set sim_code as kwarg.")
    
    message = "     guess_sim_code: " + sim_code
    print(message + warn)

    return sim_code

def get_stats_data_col_dims(sim_code):

    # if sim_code is None:
    #      sim_code = df.sim_code.unique()[0]
         
    # print('get_stats_data_col_names:', sim_code)
    
    if sim_code =='axhila':
        data_dim = np.array( [-1,0,0,1,2,4,8,4,8,4,8,4,8,4,8,-1,-1, 4,8,4,8])
    elif sim_code =='axhila_walltest':
        data_dim = np.array( [-1,0,0,1,1,2,4,8,4,8,4,8,4,8,4,8,-1,-1, -2, -2, 4,8,4,8])
    elif sim_code =='axhila_wall':
        data_dim = np.array( [-1,0,0,4,1,2,4,8,
                              4,8,4,8,4,8,4,8,-1,-1, -2, -2, 
                              4,8,4,8, 
                              4,4,8,8, 
                              8,8,8,8, 
                              1,2,1,2])
    elif sim_code =='on':
        data_dim = np.array( [-1,0,0,1,2,4,8,4,8,4,8,4,8,4,8,-1,-1])
    elif sim_code =='onsim':
        data_dim = np.array( [-1,0,0,1,2,4,8,4,8,4,8,4,8,4,8,2,-1])
    elif sim_code == "on_old":
        pass
    elif sim_code == "LAH":
        data_dim = np.array( [-1,0,0,1,2,2,2,2,2,4,8,4,8,4,8,4,8,4,8,-1,-1])
    elif sim_code == "LAH_old":
        data_dim = np.array( [-1,0,0,1,2,2,2,2,2,4,4,4,4,4,-1,-1])
    else:
        raise ValueError("Stats: error: statsFile sim_code not recognised.")         
    
    return data_dim

def get_stats_data_col_names(sim_code):

    # if sim_code is None:
    #      sim_code = self.sim_code.unique()[0]
         
    # print('get_stats_data_col_names:', sim_code)
    if sim_code == 'axhila':
        stats_data_col_names = ['t','a','root_lam_2','modPhi','modPi', 'ePi','ePi_wt',
                      'eDjPhi','eDjPhi_wt','ePhiPi','ePhiPi_wt','ePhiDjPhi',
                      'ePhiDjPhi_wt', 'eV', 'eV_wt', 'modPhi2', 'SLwind', 
                      'ePhiPi_ax', 'ePhiPi_ax_wt','ePhiDjPhi_ax', 'ePhiDjPhi_ax_wt']
    elif sim_code == 'axhila_walltest':
        stats_data_col_names = ['t','a','root_lam_2', 'Te', 'modPhi','modPi', 'ePi','ePi_wt',
                      'eDjPhi','eDjPhi_wt','ePhiPi','ePhiPi_wt','ePhiDjPhi',
                      'ePhiDjPhi_wt', 'eV', 'eV_wt', 'modPhi2', 'SLwind', 'Nlink', 'Awall',
                      'ePhiPi_ax', 'ePhiPi_ax_wt','ePhiDjPhi_ax', 'ePhiDjPhi_ax_wt']
    elif sim_code == 'axhila_wall':
        stats_data_col_names = ['t','a','root_lam_2', 'chi', 'modPhi','modPi', # write_moduli()
                      'ePi','ePi_wt','eDjPhi','eDjPhi_wt','ePhiPi','ePhiPi_wt','ePhiDjPhi', 
                                    'ePhiDjPhi_wt', 'eV_higgs', 'eV_higgs_wt', 'modPhi2', # write_energies()
                      'SLwind',                                                 # write_windings()
                      'Nlink', 'Awall',                                         # write_wallarea()
                      'ePhiPi_ax', 'ePhiPi_ax_wt','ePhiDjPhi_ax', 'ePhiDjPhi_ax_wt',
                      'eVa1', 'eVa2', 'eVa1_wt', 'eVa2_wt', 
                      'ePhiPi_ax_wax', 'ePhiDjPhi_ax_wax', 'eVa1_wax', 'eVa2_wax',
                      'RePhi', 'RePhi2', 'ImPhi', 'ImPhi2']                     # write_energies_axion()
    elif sim_code == 'on':
        stats_data_col_names = ['t','a','root_lam_2','modPhi','modPi', 'ePi','ePi_wt',
                      'eDjPhi','eDjPhi_wt','ePhiPi','ePhiPi_wt','ePhiDjPhi',
                      'ePhiDjPhi_wt','eV','eV_wt','SLexp','SLwind']
    elif sim_code == "onsim":
        stats_data_col_names = ['t','a','root_lam_2','modPhi','modPi', 'ePi','ePi_wt',
                      'eDjPhi','eDjPhi_wt','ePhiPi','ePhiPi_wt','ePhiDjPhi',
                      'ePhiDjPhi_wt','eV','eV_wt','modPhi2','SLwind']
    elif sim_code == "on_old":
        # not yet implemented
        raise ValueError("get_stats_data_col_names: on_old, 13 data columns, not yet implemented")
    elif sim_code == 'LAH':
        stats_data_col_names = ['t','a','root_lam_2','modPhi','modPi', 'modFij','modF0i', 
                           'gaussCD','gaussDE','eF0i','eF0i_wt','eFij','eFij_wt','ePi','ePi_wt',
                      'eDjPhi','eDjPhi_wt','eV','eV_wt','SLexp','SLwind']
    elif sim_code == 'LAH_old':
        stats_data_col_names = ['t','a','root_lam_2','modPhi','modPi', 'modFij','modF0i', 
                           'gaussCD','gaussDE','eF0i','eFij','ePi',
                      'eDjPhi','eV','SLexp','SLwind']
    else:
        raise ValueError("StatsFrame: error: statsFile data does not have 13, 16, 17 or 21 columns.")
    
    return stats_data_col_names

def defect_soln_params(df):
    """
    Returns parameters for string and other defect solutions. 
    Used in string length estimation.
    
    conditions for testing sim_code not safe
    """
    
    # if self.sim_code.unique()[0][0:2] == "on":
    code_name = df.sim_code.unique()[0]
    if code_name[0:2] == "on" or "ax": 
        # mu = np.pi    #for global defects, complex fields, factor 1/2 in energy. Definition of mu.
        mu = 2.11 # from approx fit to 3D data with dx = 0.5, root_lam_2 = 1.0
        # Following from code global_string_sol.py, R= 20 n_points = 20480
        # mu = 2.445277738119934
        # mup   = (0.2839401852476251) * df["lambda"]/2 # V-weighted total energy
        # mup_V = (0.1045836704991133) * df["lambda"]/2  # V-weighted potential energy

        if DEFAULT_GLOBAL_STRING_TENSION == 'continuum':
            # Continuum - deprecated
            mu_wt = (0.8911975476403653)* df["lambda"]/2/df.rs**2  # unnormalised weighted total energy
            fV_wt = 0.3683299368418231
            df_wt = -fV_wt 
        elif DEFAULT_GLOBAL_STRING_TENSION == 'lattice':
            # dx_eff = df.dx/df.ws
            dx_eff = df.dx/df.ws
            (mu_wt, fD_wt, fV_wt) = dah.getMu_V_global(dx_eff)
            df_wt = - fV_wt
            mu_wt *= df["lambda"]/2/df.rs**2 # Numbers were worked out for lambda=2
            ret_tuple = (mu, mu_wt, df_wt)
            
    elif code_name[0:3] == "LAH":
        # df["beta"] = (df.a*df.root_lam_2)**2             
        df["beta"] = df['lambda']/(2 * df['charge']**2)    
        print('defect_analysis.defect_soln_params: beta not yet computed properly for beta/=1 sims')
#            dx_eff = df.phi0*df.dx/df.rs
        dx_eff = df.dx/df.ws # Is this right?
        (mu,fB,fD,fV) = dah.getMu(dx_eff)
        mu = mu*df.beta**0.195; #Abelian Higgs String energy per unit length (beta from Hill Hodges Turner 88)
        if code_name == 'LAH': # has weighted quantities
            (mu_wt,fB_wt,fD_wt,fV_wt) = dah.getMuLag(dx_eff)
            df_wt = fB_wt - fV_wt
            mu_wt = mu_wt*df.beta**0.7 * (df.a*df.root_lam_2)**2  # measured from static 3D solution
        else:
            mu_wt = np.nan
            df_wt = np.nan
            
    ret_tuple = (mu, mu_wt, df_wt)

    return ret_tuple

def scale_data(df, data_scale):
    """
    Scales data with vev, according to dimensions in get_stats_data_col_dims.
    Only works for initial data read in from file.
    """

    sim_code = df.sim_code.unique()[0]
    data_dim = get_stats_data_col_dims(sim_code)

    for n, name in enumerate(get_stats_data_col_names(sim_code)):
        df[name] /= np.power(data_scale, data_dim[n])

    return df

def add_fluctuations(df):
    """
    Adds scalar field fluctuations to dataframe.
    modPhi has been scaled with phi0 so that it is 1 in equilibrium.
    """
    tmp = dict()
    tmp['dphi1'] =  abs(df.modPhi**2 - 1)
    if 'modPhi2' in df:
        tmp['dphi2'] =  abs(df.modPhi2 - 1)
    else:
        tmp['dphi2'] =  df.modPhi*np.nan # Series of nans with right shape

    df = pd.concat([df] + [tmp[x].rename(x) for x in tmp.keys()], axis=1)
    
    return df

def add_string_width(df):
    """ 
    Adds string width (length scale) parameters to dataframe.
    Don't really need rs, as it is ws/ws[-1]
    """
    tmp = dict()
    # Changed 1.7.23
    # tmp['rs'] = (df.a*df.root_lam_2).iloc[-1]/(df.root_lam_2*df.a) # string width relative to end
    # tmp['ws'] = 1/(df.root_lam_2*df.a*np.sqrt(2)*df.phi0)     # actual comoving string width
    tmp['ws'] = 1/(df['m_higgs'] * df['a'])
    tmp['rs'] = tmp['ws']/tmp['ws'].iloc[-1]
    
    df = pd.concat([df] + [tmp[x].rename(x) for x in tmp.keys()], axis=1)
    return df

def add_energy_pressure_lagrangian(df):
    """
    Add energy, pressure and Lagrangian from input data to dataframe
    
    conditions for testing sim_code not safe
    """
    tmp = dict()
    
    sim_code = df.sim_code.unique()[0]
    
    if sim_code == 'axhila_wall':
        df['eV'] = df.eV_higgs + df.eVa1 + df.eVa2
        df['eV_wt'] = df.eV_higgs_wt + df.eVa1_wt + df.eVa2_wt
        df['eV_ax'] = df.eVa1 + df.eVa2
        df['eV_ax_wt'] = df.eVa1_wt + df.eVa2_wt
    else:
        df['eV_higgs'] = df['eV']
        if '_old' not in sim_code:
            df['eV_higgs_wt'] = df['eV_wt']
        # df['eVa'] = 0.0*df['eV']
        # df['eVa_wt'] = 0.0*df['eV_wt']
    
    if  sim_code[0:2] == "on" or sim_code[0:2] == "ax":
        tmp['e'] = df.ePi + df.eDjPhi + df.eV      #Energy density
        tmp['L'] = df.ePi - df.eDjPhi - df.eV      #Lag. density
        tmp['p'] = df.ePi - df.eDjPhi/3 - df.eV    #Pressure
        if "_old" not in sim_code:
            tmp['e_wt'] = (df.ePi_wt + df.eDjPhi_wt + df.eV_wt)        #Energy density - potential weigthed
            tmp['L_wt'] = (df.ePi_wt - df.eDjPhi_wt - df.eV_wt)      #Lag. density - weighted
            tmp['p_wt'] = (df.ePi_wt - df.eDjPhi_wt/3 - df.eV_wt)    # Pressure - weighted
            tmp["e_higgs"] = df.ePhiPi + df.ePhiDjPhi + df.eV_higgs
            tmp["p_higgs"] = df.ePhiPi - df.ePhiDjPhi/3 - df.eV_higgs
            tmp["e_higgs_wt"] = df.ePhiPi_wt + df.ePhiDjPhi_wt + df.eV_higgs_wt
            tmp["p_higgs_wt"] = df.ePhiPi_wt - df.ePhiDjPhi_wt/3 - df.eV_higgs_wt
            tmp['w_higgs'] = tmp['p_higgs']/tmp['e_higgs'] # EOS parameter
        if 'ePhiPi_ax' in df:
            tmp["e_ax"] = df.ePhiPi_ax + df.ePhiDjPhi_ax
            tmp["p_ax"] = df.ePhiPi_ax - df.ePhiDjPhi_ax/3 
            tmp["e_ax_wt"] = df.ePhiPi_ax_wt + df.ePhiDjPhi_ax_wt
            tmp["p_ax_wt"] = df.ePhiPi_ax_wt - df.ePhiDjPhi_ax_wt/3
            if 'eV_ax' in df:
                tmp["e_ax"] += df['eV_ax']
                tmp["p_ax"] -= df['eV_ax']
            if 'eVa_ax_wt' in df:
                tmp["e_ax_wt"] += df['eV_ax_wt']
                tmp["p_ax_wt"] -= df['eV_ax_wt']
            tmp['w_ax'] = tmp['p_ax']/tmp['e_ax'] # EOS parameter
            
    elif sim_code[0:3] == "LAH":
        tmp['e'] = df.ePi + df.eDjPhi + df.eV + df.eF0i + df.eFij    #Energy density
        tmp['L'] = df.ePi - df.eDjPhi - df.eV + df.eF0i - df.eFij     #Lag. density
        tmp['p'] = df.ePi - df.eDjPhi/3 - df.eV + df.eF0i/3+df.eFij/3    #Pressure
        if "_old" not in sim_code:
            tmp['e_wt'] = (df.ePi_wt + df.eDjPhi_wt + df.eV_wt + df.eF0i_wt + df.eFij_wt)        #Energy density - potential weigthed
            tmp['L_wt'] = (df.ePi_wt - df.eDjPhi_wt - df.eV_wt + df.eF0i_wt - df.eFij_wt)        #Lag. density - weighted
            tmp['p_wt'] = (df.ePi_wt - df.eDjPhi_wt/3 - df.eV_wt + df.eF0i_wt/3+df.eFij_wt/3)    # Pressure - weighted
    # useful ratios
    tmp['w'] = tmp['p']/tmp['e'] # EOS parameter
    tmp['f_Pi'] = df.ePi/tmp['e']
    tmp['f_D'] = df.eDjPhi/tmp['e']
    tmp['f_V'] = df.eV/tmp['e']
    
    H = np.gradient(df.a, df.t)/df.a
    e_dot = np.gradient(tmp['e'], df.t) 
    rhs = H*(tmp['e'] + 3*tmp['p']) - 2*(df.s - 1.0)*H*df['eV_higgs']
    if 'eV_ax' in df and 'chi' in df:
        chi_dot = np.gradient(df.chi, df.t)
        rhs -= df.eV_ax * chi_dot/df.chi 
    # tmp["f_e_decay"] = df.t*abs(e_dot + rhs )/tmp['e']
    # tmp['H'] = H
    # tmp['e_dot'] = e_dot
    tmp["e_dot"] = pd.Series(e_dot, index=df.index)
    tmp["e_dot_rhs"] = rhs
    tmp["f_e_decay"] = (e_dot + rhs )/np.max(np.vstack((np.abs(e_dot), np.abs(rhs)) ), axis=0)

    if "_old" not in sim_code:
        tmp['L_e_wt'] = tmp['L_wt']/tmp['e_wt']
        tmp['w_wt'] = tmp['p_wt']/tmp['e_wt']
        tmp['f_Pi_wt'] = df.ePi_wt/tmp['e_wt']
        tmp['f_D_wt'] = df.eDjPhi_wt/tmp['e_wt']
        tmp['f_V_wt'] = df.eV_wt/tmp['e_wt']
        e_dot_wt = np.gradient(tmp['e_wt'], df.t)
        rhs_wt = H*(tmp['e_wt'] + 3*tmp['p_wt'])- 2*(df.s - 1.0)*H*df['eV_higgs_wt']
        # tmp["f_e_decay_wt"] = df.t*abs(e_dot_wt + rhs_wt )/tmp['e_wt']
        tmp["f_e_decay_wt"] = abs((e_dot_wt + rhs_wt )/rhs_wt)
        if sim_code[0:2] == "on" or sim_code[0:2] == "ax":
            tmp['f_PhiPi'] = df.ePhiPi/tmp['e']
            tmp['f_PhiD'] = df.ePhiDjPhi/tmp['e']
            tmp['f_PhiPi_wt'] = df.ePhiPi_wt/tmp['e_wt']
            tmp['f_PhiD_wt'] = df.ePhiDjPhi_wt/tmp['e_wt']
            tmp['f_higgs'] = tmp['e_higgs']/tmp['e']
            tmp['f_higgs_wt'] = tmp['e_higgs_wt']/tmp['e_wt']
    
        
    df = pd.concat([df] + [tmp[x].rename(x) for x in tmp.keys()], axis=1)
    return df

def add_velocity_estimators(df):
    """
    Add velocity estimators to dataframe.
    """
    tmp = dict()
    
    if "_old" not in df.sim_code.unique()[0]:
        (mu, mu_wt, delta_f_wt) = defect_soln_params(df)
        # Calculate rms velocity estimatots
        
        Rgrad = (df.ePi_wt)/(df.eDjPhi_wt) #201509 correction
        tmp = dict() # constucting dict of Series avoids annoying "fragmentationn" errors
        tmp['velGrad'] = 2*Rgrad/(1 + Rgrad)     #201509 correction
        tmp['v_s'] = np.sqrt(tmp['velGrad'])
        tmp['velEOS'] = (1 + 3*df.w_wt - 2*delta_f_wt)/(2 - delta_f_wt*(1+3*df.w_wt))
        tmp['velLag_wt'] = (1 + df.L_e_wt)/(1 - delta_f_wt*df.L_e_wt)
        tmp['v_L'] = np.sqrt(tmp['velLag_wt'])
        tmp['velLag'] = 1. + df.L/df.e
    else:
        # raise UserWarning("StatsFrame: old stats data, velocities not set.")
        print("StatsFrame: old stats data, velocities not set.")    

    df = pd.concat([df] + [tmp[x].rename(x) for x in tmp.keys()], axis=1)
    return df

def add_length_estimators(df):
    """
    Add string length estimators to dataframe.
    """
    tmp = dict()
    
    def zeta_xi(x,y):
        return (x/(2*y))**2

    (mu, mu_wt, delta_f_wt) = defect_soln_params(df)
    
    tmp['SLlag'] = -(df.L/mu)*df.vol
    tmp['xiWind'] = 1/np.sqrt(man_fac*df.SLwind/df.vol)
    tmp['xiWind_phys'] = tmp['xiWind']*df['a']
    tmp['xiLag'] = 1./np.sqrt(-df.L/mu)
    tmp['xi_e'] = 1./np.sqrt(df.e/mu)
    # tmp["zetaWind"] = (man_fac * df.SLwind) * (df.t/2)**2 / df.vol
    tmp["zetaWind"] = (df['t_cosmic']/tmp['xiWind_phys'])**2
    tmp["xWind_phys"] = tmp['xiWind_phys']/df['t_cosmic']

    df = pd.concat([df] + [tmp[x].rename(x) for x in tmp.keys()], axis=1)
    tmp = dict()

    if '_old' not in df.sim_code.unique()[0]:
        tmp['SL_r'] = abs(df.vol*(df.e_wt - delta_f_wt*df.L_wt)/((1+delta_f_wt)*mu_wt)) 
        tmp['xi_r'] = np.sqrt( df.vol/tmp['SL_r'])
        tmp['xi_r_phys'] = tmp['xi_r']*df['a']
        tmp['xiInv'] = np.sqrt(-mu*(1-df.velEOS)/df.L)
        tmp['xiInv_wt'] = np.sqrt(-mu_wt*(1-df.velEOS)/df.L_wt)
        # tmp["zeta_r"] = (tmp['SL_r']) * (df.t/2)**2  / df.vol
        tmp["zeta_r"] = (df['t_cosmic']/tmp['xi_r_phys'])**2
        tmp["x_r_phys"] = tmp['xi_r_phys']/df['t_cosmic']
    else:
        # raise UserWarning("StatsFrame: old stats data, weighted length estimators not set.")
        print("StatsFrame: old stats data, weighted length estimators not set.")
 
    df = pd.concat([df] + [tmp[x].rename(x) for x in tmp.keys()], axis=1)
    return df

def add_area_estimators(df):
    """
    Add wall area estimators to dataframe.
    """
    tmp = dict()
    
    def zeta_xi(x,y):
        return (x/(2*y))**2
    
    tmp['zetaWallLink'] = (man_fac * df.Nlink) * (df.t/2) / df.vol
    tmp['zetaWallGrad'] = (man_fac * df.Awall) * (df.t/2) / df.vol

    df = pd.concat([df] + [tmp[x].rename(x) for x in tmp.keys()], axis=1)
    return df

def fix_data(df, sim_code):
    """Fixes data inconsistencies in ouput files.
    
    onsim 2021/2022: factor 2 in higgs gradient energies, ePhiDjPhi and ePhiDjPhi_wt
    """
    
    if sim_code == 'onsim':
        df["ePhiDjPhi"] *= 2
        df["ePhiDjPhi_wt"] *= 2

    return df

def polyfit_xi_df(df, t_fit_range):
    """
    Performs linear fit to xi against t over the specifited range (2-tuple).
    Three different xi measures used: xiLag, xiWind, xi_r.
    Returns a tuple of parameter lists.
    Works only for single runs - should check number of unique id_str
    """
    t_fit_start = t_fit_range[0]
    t_fit_end = t_fit_range[1]
   
    t_fit_period = np.where(np.logical_and(t_fit_start < df.t, df.t < t_fit_end))
    
    # print(df.t.loc[t_fit_period].shape, df.xiLag.loc[t_fit_period].shape)
    
    try:
        p_wind = np.polyfit(df.t.loc[t_fit_period], df.xiWind.loc[t_fit_period],1)
    except:
        print('Fit to xiWind in t_range',t_fit_range, 'failed.')
        p_wind = np.array([np.nan]*2)

    try:
        p_lag = np.polyfit(df.t.loc[t_fit_period], df.xiLag.loc[t_fit_period],1)
    except:
        print('Fit to xiLag in t_range',t_fit_range, 'failed.')
        p_lag = np.array([np.nan]*2)

    try:
        p_xi_r = np.polyfit(df.t.loc[t_fit_period], df.xi_r.loc[t_fit_period],1)
    except:
        print('Fit to xi_r in t_range', t_fit_range, ' failed.')
        p_xi_r = np.array([np.nan]*2)
    
    return p_lag, p_wind, p_xi_r
    
def get_t_cosmic(df):
    """
    Cosmic time $t$ in a FLRW universe, from $dt = a(\tau)d\tau$.

    Parameters
    ----------

    Returns
    -------
    t_cos : pandas Series
        Cosmic time in the simulation.

    """
    if 'era' in df:
        era=df.era.unique()[0]
    elif 'eraB' in df:
        era=df.eraB.unique()[0]
    else:
        print('get_t_cosmic: FLRW era not found, defaulting to radiation era=1')
        era = 1.0
        
    
    if isinstance(era, float) or isinstance(era,int):
        t_cos = df.a * df.t/(era + 1)
    else:
        try:
            t_cos = pd.Series(spi.cumtrapz(df.a, df.t, initial=0.), index=df.index)              
        except:
            t_cos = pd.Series(spi.cumulative_trapezoid(df.a, df.t, initial=0.), index=df.index)              
            
    
    return t_cos

