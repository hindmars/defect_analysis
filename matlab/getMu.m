function [mu0, fB, fD, fV] = getMu(dx)
%
% [mu0, fB, fD, fV] = getMu(dx)
% 
% Returms mass per unit length, plus fractions in B, DPhi and V(phi)
%  dx is lattice spacing (row vector)
%  uses numerical data from xA=0.0 wave.

% mu0 = getWaveEnergies(10)
%mu0(1,1) = 6.2054; % dx = 0.5 N=64 static solution
%mu0(1,2) = 6.2647; % dx = 0.25 N=128 static solution
%mu0(1,3) = 6.2780; % dx = 0.125 N=256 static solution
%mu0 = 6.2832;  % N=infty static solution (2*pi)
%a  = [0.3203   -0.0052    0.0002]; 
% poly fit coefficients to dx = [0.5,0.25,0.125]
% a = polyfit(dx,mu0(1,:),2) etc

% fB_all = [ 0.2149    0.2092    0.2080 ];
% fD_all = [ 0.5836    0.5845    0.5847 ];
% fV_all = [ 0.2015    0.2063    0.2074 ];
% 

a(1,:) = [-0.3484   0.0242    6.2804]; % Total
a(2,:) = [ 0.1324  -0.0081    1.3046]; % B
a(3,:) = [-0.2392   0.0188    3.6719]; % Dphi
a(4,:) = [-0.2416   0.0135    1.3039]; % V

mu0 = a(1,1)*dx.^2 + a(1,2)*dx + a(1,3);
fB = (a(2,1)*dx.^2 + a(2,2)*dx + a(2,3))./mu0;
fD = (a(3,1)*dx.^2 + a(3,2)*dx + a(3,3))./mu0;
fV = (a(4,1)*dx.^2 + a(4,2)*dx + a(4,3))./mu0;

