function [kt,r,C]=UETCload(filenamePre,Cname,id,run,tRef,tOffSet,simpars,tLimit)
%UETC loading function for UETC.hpp data 2006-2013
%
% Usage: [kt,r,C]=UETCload(filenamePre,Cname,id,run,tRef,tOffset)
%
% filenamePre = path to data
%  Cname = UETC name, eg. scalar11 or vector
%     id = ID string between 'UETCscalar11_' and before '.dat'
%          eg. to load statsFile_6L01 ID is '6L%2' with run=1
%    run = realizations(s) to include
%   tRef = UETC reference time
%tOffset = time when xi=0, if '*' get from statsFile (Lag & tRef<t<2*tRef)
%
% 13.10.04 MBH tOffset now performed for each run rather than on average
%              Can now handle data files without runID
%
% simpars - vector containing
%    dx = lattice spacing (only needed for winding xi)
%     N = lattice size (only needed for winding xi)
%  beta = scalar mass^2/vector mass^2
% ... (vector can be empty vector [])
%  tLimit = vector with times between which to plot
%        NEW 2016 - can be a Map container with key and value
% Version 2016.05.11 ALE, MBH
%    Start using Map container class for passing simpars
%   Allow the '*' and '*W' options, note that there are more arguments
%   'xiscaling' is implemented




%Get number of runs to load
nRuns=size(run,2);


%Make id into num2str format
s=strfind(id,'%');
for nr=1:nRuns
    if numel(s) > 0
        ID=[id(1:s+1) '.' id(s+1) 'i' id(s+2:end)];
        idString{nr} = num2str(run(nr),ID);
    else
        ID = id;
        idString{nr} = ID;
    end
end


if exist('tOffSet','var')
    %Duplicate tOffSet if single value (eg. 0) given for many runs
    if size(tOffSet,1)==1 && nRuns>1 && isnumeric(tOffSet)==1
        toffset = ones(1,nRuns)*tOffSet;
     end
    if size(tOffSet,1)==1 && nRuns>1 && isnumeric(tOffSet)~=1
        for i=1:nRuns
            toffset(i,:)=tOffSet;
        end
    end
    if nRuns==1 && isnumeric(tOffSet)==1
        toffset(1,:)=tOffSet;
    end
end


%Load info file for first number 
[ktRaw,rRaw]=UETCinfoRead(filenamePre,idString{1});

%Load and sum correlators
for i=1:nRuns
    fileName=[filenamePre 'UETC' Cname idString{i} '.dat'];
    disp(['Loading UETC: ' fileName]);
    Ci=load(fileName);
    
    if size(Ci,1)>size(rRaw,1); 
        disp('Warning correlator has more rows than expected. Truncating...')
        Ci=Ci(1:size(rRaw,1),:);
    end

%    if exist('tOffSet','var')==1
        disp(['Initial max time ratio: ' num2str(max(rRaw))])
% [kt,r,Ci]=UETCtimeOffSet(id,run,Cname,ktRaw,rRaw,Ci,tRef,toffset(i,:),filenamePre,simpars,tLimit); %Apply time offset correction
% Surely tOffset is just a vector? MBH 06.11.16
[kt,r,Ci]=UETCtimeOffSet(id,run,Cname,ktRaw,rRaw,Ci,tRef,toffset(i),filenamePre,simpars,tLimit); %Apply time offset correction
        disp(['Post-offset max time ratio: ' num2str(max(r))])
%    end

    
    if i==1; 
        C=Ci;
    else
        C=C+Ci;
    end
end

%Average
if nRuns>1
    disp('Averaging correlators');
    C=C/nRuns;
end

