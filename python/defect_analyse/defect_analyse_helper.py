#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 16 12:40:07 2021

Helper functions for the defect_analyse module.

read_settings_file

getMu

getMuLag


@author: hindmars
"""

import numpy as np
import matplotlib.patches as mp
import matplotlib.colors as mc
import colorsys

import scipy.optimize as spo
from . import defect_analyse_etc as dae

latfield_series = ['LAH_old', 'LAH', 'on_old', 'on']
hila_series = ['onsim', 'axhila', 'axhila_wall']
gw_series = ['gwModule']


#        
def read_settings_file(path_to_settings):
    """
    Reads in simulation parameters as a dictionary. Each line is assumed to be 
        parameter=value
        parameter value
        parameter_word1 parameter_word2 ... value
    The last word is taken to be the value for the dictionary
    If the value can be converted to a float or an integer.
    Parameter words are concatenated to make the dictionary key
    Blank lines are ignored
    Characters after "#" are ignored
    """
    dictionary = {}
    with open(path_to_settings, "r") as file:
        for line in file:
            if len(line.strip()) > 0 and line.strip()[0] != '#':
                try:
                    key, value = line.strip().split('#',1)[0].split("=")
                    try:
                        v = int(value)
                    except ValueError:
                        try: 
                            v = float(value)
                        except:
                            v = value
                    dictionary[key] = v
                except:
                    pass
                try:
                    words = line.strip().split('#',1)[0].split()
                    # key = words[0]
                    key = ' '.join(words[:-1])
                    value = words[-1]
                    try:
                        v = int(value)
                    except ValueError:
                        try: 
                            v = float(value)
                        except:
                            v = value
                    dictionary[key] = v
                except:
                    pass
    
    if 'tUETCreference' not in dictionary.keys():
        try:
            dictionary['tUETCreference'] = dictionary['UETC start time']
        except:
            pass
        
    return dictionary

def get_path_to_etc(sf, corr_type, run_id_str, path_data, **kwargs):
    """

    Parameters
    ----------
    sf : setting file/params file
        dictionary containing run parameters ("settings")
    corr_type : str
        correlator type: scalar11, scalar12, scalar22, vector, tensor.
    run_id_str : str
        Identifier string for run of set of runs.
    path_data : str
        Path to data file tree. Data file assumed to be here.

    kwargs
    
    field_type : str
        ETC | UETC | hTTplus | hTTcross | hdotTTplus | hdotTTcross 
    
    add_path_to_etc: string
        concatenate to path_data to get to etc data
        
    data_id_str: string
        overrides run_id_str to give a DataFrame label to data from a run     
    
    add_path_to_stats: string
        concatenate to path_data to get to stats file

    Returns
    -------
    path_to_etc : str
        Path to the file containing the correlator data.

    """
    
    field_type = get_kwarg(kwargs, "field_type", "ETC")
    pathRoot = get_kwarg(sf, 'pathRoot', '')    
    pathStats = get_kwarg(sf, 'pathStats', '')    
    add_path_to_etc = get_kwarg(kwargs, 'add_path_to_etc', 
                                   pathRoot + pathStats)
    # add_path_to_etc = get_kwarg(kwargs, "add_path_to_etc", rel_path_etc)
    etc_name_postfix = get_kwarg(kwargs, "etc_name_postfix")
    etc_name_prefix = get_kwarg(kwargs, "etc_name_prefix")

    if "TT" in field_type:
        etc_file_type = get_kwarg(kwargs, "etc_file_type", ".txt")
        path_to_etc = path_data + add_path_to_etc + field_type \
            + run_id_str + corr_type + etc_name_postfix + etc_file_type
    # elif field_type == "UETC":
    #     etc_name_prefix = get_kwarg(kwargs, "etc_name_prefix")
    #     etc_file_type = get_kwarg(kwargs, "etc_file_type", ".dat")
    #     path_to_etc = path_data + add_path_to_etc + etc_name_prefix + field_type + corr_type \
    #         + run_id_str + etc_name_postfix + etc_file_type                    
    else:
        etc_file_type = get_kwarg(kwargs, "etc_file_type", ".dat")
        path_to_etc = path_data + add_path_to_etc + etc_name_prefix + field_type + corr_type \
            + run_id_str + etc_name_postfix + etc_file_type
    
    return path_to_etc

def get_corr_data(corr_type, field_type, path_to_corr, path_to_info, path_to_log, 
                  sim_code='on'):
    
    if sim_code in hila_series:
        corr, k_mean, t_uetc_write = get_corr_data_hila_series(corr_type, field_type, 
                                                  path_to_corr, path_to_info)
    elif sim_code in latfield_series:        
        corr, k_mean, t_uetc_write = get_corr_data_latfield_series(corr_type, field_type, 
                                                  path_to_corr, path_to_info, path_to_log)
        # corr = (corr.T*t_uetc_write).T
    else:
        raise ValueError('error: sim_code not recognised. Valid values are: \n' 
                         + latfield_series + '\n'
                         + hila_series)
                         
    
    return corr, k_mean, t_uetc_write

def get_corr_data_latfield_series(corr_type, field_type, path_to_corr, path_to_info, path_to_log):
    
    corr = np.loadtxt(path_to_corr)        
    
    if path_to_log is not None:
        with open(path_to_log) as f:
            text = f.readlines()
            print("     Read log file", path_to_log, len(text), "lines")
    else:
        print("     No log file")
        text = ''

    if "kMean" in path_to_info:
        # Probably a GW PS file from GWmodule
        k_mean = np.loadtxt(path_to_info)
        print("     Reading k data from", path_to_info)
        t_uetc_write = look_for_data(text, "Writing GW")
        print("     read t_uetc", len(t_uetc_write))
    else:
        # Probabably LAH, ON and descendents
        kt_ref = np.loadtxt(path_to_info, skiprows=3, max_rows=1)
        print("     Reading k data from", path_to_info)
        # t_tref_info = np.loadtxt(path_to_info, skiprows=7, max_rows=1)
        t_uetc_write = look_for_data(text, "Writing UETC") # The actual writing times
        print("     read t_uetc", len(t_uetc_write))
        if len(t_uetc_write) == 0:
            print('UETC write times not found in log file, using info file')
            t_uetc_write = np.loadtxt(path_to_info, skiprows=7, max_rows=1)
        k_mean = kt_ref/t_uetc_write[0]

    return corr, k_mean, t_uetc_write

def get_corr_data_hila_series(corr_type, field_type, path_to_corr, path_to_info):
    """Loads correlation function (etc, uetc) and associated data written by axhila.
    

    Parameters
    ----------
    corr_type : str
        Correlator type, one of axhila_corr_types.
    path_to_corr : str
        Path to file containing k and correlator data
    path_to_info : str
        Path to file containg times correlator is written

    Returns
    -------
    corr : numpy arrray, shape (n_k_bins, n_times)
        Array contining correlator data.
    k : numpy array, shape (n_k_bins, )
        Mean wave numbers in bins.
    times : numpy array, shape (n_times, )
        Times correlator was written.

    """
    
    d_corr = np.loadtxt(path_to_corr)
    d_shape = d_corr.shape
    # Infer number of times correlator was written - first column is bin number
    n_times = len(d_corr[d_corr[:,0] == 0, 0])
    n_bins = d_shape[0]//n_times
    if corr_type in ['J0Js', 'JsJ0', 'JsPsi', 'PsiJs', 'JsPi', 'PiJs']:
        corr = d_corr[:,3].reshape(n_times,n_bins) # Imaginary part
    else:
        corr = d_corr[:,2].reshape(n_times,n_bins) # Real part
        
    k = d_corr[:,1].reshape(n_times,n_bins)
    
    print('Loading (U)ETC times from', path_to_info)
    times = np.loadtxt(path_to_info)

    # Write only one row of k array - all rows identical
    return corr, k[0, :], times

def get_kwarg(dic, name, default=""):
    if name in dic:
        var = dic[name]
    else:
        var = default
    return var


def look_for_data(text, key, position=-1):
    data = []
    for line in text:
        if key in line:
            data.append(float(line.split()[position]))
    return np.array(data)

def power_law(x, A, n):
    """
    Function $Ax^n$

    Parameters
    ----------
    x : float or array of floats
        Argument of function.
    A : float
        Amplitude.
    n : float or int
        power law index.

    Returns
    -------
    float or array of floats
        Value of function..

    """
    return A * x**n

def fit_power_law(x, y, x_ref, A0, n0, **kwargs):
    """
    Fits data in x and y to a power law of the form $y = A(x/x_{ref})^n$, 
    using scipy.optimize.curve_fit, and returns optimal parameters and covariances.

    Parameters
    ----------
    x : array-like
        x data.
    y : array-like
        y data.
    x_ref : float
        reference value for x.
    A0 : float
        Initial guess for amplitude A.
    n0 : float or int
        Initial guess for power law index n.
    **kwargs : keyword arguments
        Keyword arguments passed to curve_fit.

    Returns
    -------
    2-tuple of arrays
        Optimal parameters popt and covariance matrix copt.

    """
    return spo.curve_fit(power_law, x/x_ref, y, p0=(A0, n0), **kwargs)
    

def plot_power_law(ax, k, A, k0, n, **kwargs):
    ax.loglog(k, A*(k/k0)**(n),'k--', **kwargs)
#    plt.semilogx(k,A*(k/k0)**(n),'k--', label=r'$k^{{{:}}}$'.format(n))
    return ax

def get_mean_std(y, axis=0):
    y_mean = np.mean(y, axis=axis)
    y_std = np.std(y, axis=axis)
    return y_mean, y_std

def get_mean_std_xy(x, y, axis=0):
    return get_mean_std(x, axis) + get_mean_std(y, axis)

def plot_mean_std(x, y, ax, **kwargs):
    """Plots mean and standard deviation of an array of y data. Mean is taken 
    along axis 0. STD is shown as a band with high transparency."""
    y_mean = np.mean(y, axis=0)
    y_std = np.std(y, axis=0)
    y_plus = y_mean + y_std
    y_minus = y_mean - y_std
    line_mean = ax.plot(x, y_mean, **kwargs)
    line_std = ax.fill_between(x, y_minus, y_plus, color=line_mean[0]._color, alpha=0.25)
    return line_mean, line_std


def plot_mean_std_xy(x, y, ax, err_type=None, err_every=5, **kwargs):
    """Plots mean and standard deviation of an array of y data, against the mean of 
    an array of x data. Means are taken along axis 0. STD is shown as a 
    
    - y band, if string 
    
    error ellipses with a paler colour, every err_every point."""

    x_mean = np.mean(x, axis=0)
    x_std = np.std(x, axis=0)
    x_plus = x_mean + x_std
    x_minus = x_mean - x_std

    
    y_mean = np.mean(y, axis=0)
    y_std = np.std(y, axis=0)
    y_plus = y_mean + y_std
    y_minus = y_mean - y_std

    line_mean = ax.plot(x_mean, y_mean, **kwargs)
    
    print('err_type', err_type)
    
    if isinstance(err_type, str):
        if 'y' in err_type:
            line_std_y = ax.fill_between(x_mean, y_minus, y_plus, color=line_mean[0]._color, alpha=0.25)
        if 'x' in err_type:
            line_std_x = ax.fill_betweenx(y_mean, x_minus, x_plus, color=line_mean[0]._color, alpha=0.25)
        # line_std_y = ax.fill_between(x_mean, y_minus, y_plus, 
        #                               color=adjust_lightness(line_mean[0]._color, 1.5))
        # line_std_x = ax.fill_betweenx(y_mean, x_minus, x_plus, 
        #                               color=adjust_lightness(line_mean[0]._color, 1.5))
        if err_type == 'ellipse':
            for x, y, dx, dy in zip(x_mean[::err_every], y_mean[::err_every], 
                                    x_std[::err_every], y_std[::err_every]):
                ax.add_patch(mp.Ellipse((x,y), 2*dx, 2*dy, 
                                        linestyle=None,
                                        color=adjust_lightness(line_mean[0]._color, 1.5),
                                        zorder=line_mean[0].zorder - 1)
                         )
    # return line_mean, line_std_y, line_std_x
    return line_mean


def log_round(x):
    return 10**np.round(np.log10(x))


def deletekey(d, key):
    r = dict(d)
    if key in d:
        del r[key]
    return r


def getMu(dx):
    """
    Returns a tuple (mu0, fB, fD, fV) of solution data for Abelian Higgs string.
    Used in computation of string lengths.
    mu0 - mass/length
    fB  - fraction from B-field
    fD  - fraction from scalar covariant derivative
    fV  - fraction from scalar potential
    """
    a = np.zeros((4,3))
    a[0,:] = np.array([-0.3484,   0.0242,    6.2804]) #% Total
    a[1,:] = np.array([ 0.1324,  -0.0081,    1.3046]) #% B
    a[2,:] = np.array([-0.2392,   0.0188,    3.6719]) #% Dphi
    a[3,:] = np.array([-0.2416,   0.0135,    1.3039]) #% V
    
    mu0 = a[0,0]*dx**2 + a[0,1]*dx + a[0,2]
    fB = (a[1,0]*dx**2 + a[1,1]*dx + a[1,2])/mu0
    fD = (a[2,0]*dx**2 + a[2,1]*dx + a[2,2])/mu0
    fV = (a[3,0]*dx**2 + a[3,1]*dx + a[3,2])/mu0
    
    return mu0, fB, fD, fV


def getMuLag(dx):
    """
    Returns a tuple (mu0, fB, fD, fV) of solution data for Abelian Higgs string.
    Used in computation of string lengths. This Lagragian weighted
    mu0 - mass/length (lagrangian weighted)
    fB  - fraction from B-field (lagrangian weighted)
    fD  - fraction from scalar covariant derivative (lagrangian weighted)
    fV  - fraction from scalar potential (lagrangian weighted)
    """
    a = np.zeros((4,3))

    a[0,:] = -np.array([ 0.6029,    0.0635,   -7.0233]) #% Total
    a[1,:] = -np.array([-0.1851,    0.0496,   -1.4464]) #% B
    a[2,:] = -np.array([ 0.4338,    0.0215,   -4.1350]) #% Dphi
    a[3,:] = -np.array([ 0.3542,   -0.0076,   -1.4419]) #% V

    mu0 = a[0,0]*dx**2 + a[0,1]*dx + a[0,2]
    fB = (a[1,0]*dx**2 + a[1,1]*dx + a[1,2])/mu0
    fD = (a[2,0]*dx**2 + a[2,1]*dx + a[2,2])/mu0
    fV = (a[3,0]*dx**2 + a[3,1]*dx + a[3,2])/mu0
    
    return mu0, fB, fD, fV

def getMu_V_global(dx):
    """
    Returns a tuple (mu0, fD, fV) of solution data for global string (1 field).
    Used in computation of string lengths. This Lagragian weighted
    mu0 - mass/length (potential weighted)
    fD  - fraction from scalar covariant derivative (potential weighted)
    fV  - fraction from scalar potential (potential weighted)
    """
    # a = np.zeros((3,3))

    # a[0,:] = -np.array([ 0.6029,    0.0635,   -7.0233]) #% Total
    # a[1,:] = -np.array([ 0.4338,    0.0215,   -4.1350]) #% Dphi
    # a[2,:] = -np.array([ 0.3542,   -0.0076,   -1.4419]) #% V


    # mu0 = a[0,0]*dx**2 + a[0,1]*dx + a[0,2]
    # fD = (a[1,0]*dx**2 + a[1,1]*dx + a[1,2])/mu0
    # fV = (a[2,0]*dx**2 + a[2,1]*dx + a[2,2])/mu0

    c_mu_wt = np.array([-0.03412308, -0.01049499,  1.00011813])*0.8911975476403653
    c_f_D   = np.array([-0.59561854,  0.00425254,  0.40190176])
    c_f_V   = np.array([ 1.28200629e-02, -6.24794256e-05,  1.00037053e+00])*0.3683299368418231
    
    mu0 = np.poly1d(c_mu_wt)(dx)
    fD = np.poly1d(c_f_D)(dx)
    fV = np.poly1d(c_f_V)(dx)
    
    return mu0, fD, fV

def remap_fun(fun, x_range):
    """Remaps x so that 0 becomes x_range[0] and 1 becomes x_range[-1]. 
    Gives to function fun in order to make a new function. """
    def ret_fun(x):
        x_new = x_range[0] + x*(x_range[-1] - x_range[0])
        return fun(x_new)
    return ret_fun


def get_omgw(*args, **kwargs):
    """
    Convenience function for loading data needed to construct graviational wave power spectrum.

    Parameters
    ----------
    *args : 
        run_id_str : string
            Identifier string for run of set of runs.
        path_data : string
            Path to data file tree. Data file assumed to be here.
        settings : string or dict
            Path to settings file.


    **kwargs : 
        
        ps_types: string
            if set to 'all', returns all gwps types
        add_path_to_etc: string
            concatenate to path_data to get to etc data
            
        data_id_str: string
            overrides run_id_str to give a DataFrame label to data from a run     
        
        add_path_to_stats: string
            concatenate to path_data to get to stats file.

    Returns
    -------
    
    3-tuple of
    gw : ETCFrame
        Contains the total gravitational wave energy spectral density, the sum of the kinetic and gradient parts.
    gw_kin : ETCFrame
        Contains the kinetic part of the gravitational wave energy spectral density.
    gw_gra : ETCFrame
        Contains the kinetic part of the gravitational wave energy spectral density.
        
    if ps_types == 'all', the above plus 
    gw_dot_cross, gw_cross, gw_dot_plus, gw_plus, the polarisation components of gw_kin and gw_gra

    """
    gw_dot_cross = dae.ETCFrame('pCross', field_type='hdotTT', *args, **kwargs)
    gw_cross = dae.ETCFrame('pCross', field_type='hTT', *args, **kwargs)
    gw_dot_plus = dae.ETCFrame('pplus', field_type='hdotTT', *args, **kwargs)
    gw_plus = dae.ETCFrame('pplus', field_type='hTT', *args, **kwargs)

    gw_kin = gw_dot_cross.etc_add(gw_dot_plus)
    gw_gra = gw_cross.etc_add(gw_plus)

    gw_kin.etc["field_type"] = "omgw"
    gw_kin.etc["corr_type"] = "Kin"
    gw_gra.etc["field_type"] = "omgw"
    gw_gra.etc["corr_type"] = "Gra"

    gw = gw_kin.etc_add(gw_gra)
    gw.etc["field_type"] = "omgw"
    gw.etc["corr_type"] = "Full"
    
    if "ps_types" in kwargs and kwargs["ps_types"] == "all":
        return gw, gw_kin, gw_gra, gw_dot_cross, gw_cross, gw_dot_plus, gw_plus
    else:
        return gw, gw_kin, gw_gra
    

def get_decoh_by_k(uetc_t, etc_t, ki, xvar='ktminus', decoh_model='sqrt', t_fit_range = [0, np.inf],
              scaling_type='latfield_series'):

    corr_type = uetc_t.etc.corr_type.unique()[0]
    sim_code = uetc_t.st.sim_code.unique()[0]

    t = uetc_t.etc[['id_str', 't_uetc']].copy()
    run_ids = t['id_str'].unique()
    n_runs = len(run_ids)
    t_len = t.shape[0]//n_runs
    t_ref = uetc_t.etc[['id_str', 'tUETCreference']].copy()
    for id_str in run_ids:
        t_ref[t['id_str']==id_str] = t[t['id_str']==id_str].loc[0]
    t_ref_orig = t_ref.copy()
    
    t_arr = (t['t_uetc'].values).reshape(n_runs, t_len).T
    t_ref_arr = (t_ref['tUETCreference'].values).reshape(n_runs, t_len).T
    
    kt_ref_list = []
    kt_list = []
    d_kt_list = []
    # freqs_list = []
    # d_kt_ft_list = []
    
    # fig1, ax1 = plt.subplots()

    # print(t_ref_arr.shape)
    # print(t_arr.shape)

    for n, nk in enumerate(ki):
        k_nk = uetc_t.k_df[nk]
        # e_kt = (etc_t.get_etc(k_ind = nk)[0:t_len]/t['t_uetc']).values
        # u_kt = (uetc_t.get_etc(k_ind = nk)[0:t_len]/np.sqrt(t_ref['tUETCreference'] * t['t_uetc'])).values
        e_kt = (etc_t.get_etc(k_ind = nk)).values
        u_kt = (uetc_t.get_etc(k_ind = nk)).values
        e_kt = e_kt.reshape(n_runs, t_len).T
        u_kt = u_kt.reshape(n_runs, t_len).T
        if scaling_type == 'latfield_series':
            e_kt /= t_arr
            u_kt /= np.sqrt(t_ref_arr*t_arr)
        e_ktref = np.full_like(e_kt,e_kt[0,:])
        if decoh_model == 'sqrt' or decoh_model == 'geomean':
            d_kt = u_kt/np.sqrt(e_ktref*e_kt)
        elif decoh_model == 'initial':
            d_kt = u_kt/e_ktref
        elif decoh_model == 'B':
            d_kt = k_nk*np.sqrt(t_ref_arr * t_arr) * u_kt
        else:
            raise ValueError('plot_fit: decoh_model = ["sqrt" | "alt" | "B"]')

        kt_ref = k_nk*t_ref_arr[0,0]
        kt_ref_list.append(kt_ref) 
        # dx = kt[1] - kt[0]
        # d_kt_ft = fft(d_kt.values)
        # print(len(d_kt), dx)
        # freqs = fftfreq(len(d_kt), dx)
        # print(len(freqs))

        kt = k_nk*t_arr[:,0]
        kt_list.append(kt) 
        d_kt_list.append(d_kt) 
        # freqs_list.append(freqs)
        # d_kt_ft_list.append(d_kt_ft)

    
        # N = len(d_kt)
        # ax1.plot(2*np.pi*freqs[:N//2], dx * np.abs(d_kt_ft[:N//2])*2, label=r'${:.1f}$'.format(k_nk*t_ref))

    # ax1.plot(2*np.pi*freqs[:N//2], 2.5/(2*np.pi*freqs[:N//2]),'k--', label=r'$Uu{-1}$')

    # ax1.legend(bbox_to_anchor=(1.05, 1.0), title=r'$kt_{{\rm ref}}$')
    # # ax1.set_xlabel(r'$\tilde\omega$')
    # # ax1.set_ylabel(r'$|\tilde{D}(\tilde\omega)|^2$')
    # ax1.set_xlabel(r'$u$')
    # ax1.set_ylabel(r'$|\tilde{D}(u)|^2$')
    # title_str = sim_code + ', ' + uetc_t.st._get_title_string().lstrip('_') + ', $\\tau_{{\\rm ref}} = {:.0f}$'.format(t_ref_orig) + ', ' + corr_type
    # extra1 = ''
    
    # ax1.set_title(title_str + extra1)
    # return ax1, np.array(kt_ref_list), np.array(kt_list), np.array(d_kt_list), np.array(freqs_list), np.array(d_kt_ft_list)
    return np.array(kt_ref_list), np.array(kt_list), np.array(d_kt_list)

def get_decoh(uetc, etc1, etc2=None, decoh_norm='geomean'):

    
    u = uetc.etcf_mean().get_etc()
    e1 = etc1.etcf_mean().get_etc()
    if etc2 is not None:
        e2 = etc2.etcf_mean().get_etc()
    else:
        e2 = e1
        
    e1_tref = e1.loc[0,:]
    

    if decoh_norm == 'sqrt' or decoh_norm == 'geomean':
        d = u/np.sqrt(e1_tref*e2)
    elif decoh_norm == 'initial':
        d = u/e1_tref
    else:
        raise ValueError('plot_fit: decoh_model = ["sqrt"/"geomean" | "initial" ]')
    
    k = uetc.k_df
    t_uetc = uetc.etcf_mean().etc['t_uetc']

    return d, k, t_uetc

def adjust_lightness(color, amount=0.5):
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], max(0, min(1, amount * c[1])), c[2])
