#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jun  6 17:13:42 2021

@author: hindmars
"""

import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import newton_krylov
import scipy.optimize.nonlin

class quartic_potential:
    """ Quartic potential class. Creates a quartic potential object according to 
    two parameters (see v function) 
    (minumum value of potential is always zero, always an extremum at phi = 0)
    lam - quartic coupling, default 1
    phi_b - broken phase field value, default 1
    """
    
    def __init__(self, lam=1.0, phi_b = 1.0):
        self.lam = lam
        self.phi_b = phi_b
        self.lam_phi_b_4 = lam * phi_b**4

    def v(self, phi):
        return 0.25*self.lam_phi_b_4*(phi**2 - 1)**2
    
    def v_prime(self, phi):
        return self.lam_phi_b_4*(phi**2 - 1)*phi
    
    def v_prime_prime(self, phi):
        return self.lam_phi_b_4*(3*phi**2 - 1)

    
class lattice_1d:
    """ 1D lattice class. Contains n x values betwen 0 and R, at half dx intervals. 
    Also values x_minus (shifted lattice).  dim is numer of dimensions in differential equation, 
    not lattice dimension. 
    """
    
    def __init__(self, n, R, dim=3):
        self.n = n
        self.R = R
        self.dx = R/n
        self.dx2 = self.dx**2
        self.dim = dim
    
        x_min = 0.5*self.dx
        x_max = R - x_min
    
        self.x = np.linspace(x_min, x_max, n)
    
        self.x_minus = np.roll(self.x,1)
        self.x_minus[0] = -self.x[0]
    

def field_eqn(fields, pot, lat):
    """
    Abelian Higgs field equation in 1 radial dimension, with canonical kinetic term.
    """
    
    fields_plus = np.roll(fields, -1, axis=0)
    fields_minus = np.roll(fields, +1, axis=0)
    
    fields_plus[-1] = fields_plus[-2]
    fields[0] = 0
    fields[-1] = 1
    fields_minus[0] = fields[1]
    
    fields_prime_plus = (fields_plus - fields)/lat.dx
    fields_prime_minus = (fields - fields_minus)/lat.dx
        
    phi_2_prime = (fields_prime_plus - (lat.x_minus/lat.x) * fields_prime_minus)/lat.dx
    
    phi_field_eqn = phi_2_prime -  fields/lat.x**2 - pot.v_prime(fields)

    return phi_field_eqn


def energy_densities(fields, lat, pot):
    """
    Gradient, Potential, Magnetic, Total energy densities
    """
    fields_plus = np.roll(fields, -1, axis=0)
    fields_plus[-1] = fields_plus[-2]
    fields_prime_plus = (fields_plus - fields)/lat.dx

    f = fields
    f_prime = fields_prime_plus  
    
    e_dh = 0.5*f_prime**2 
    e_da = 0.5*f**2/lat.x**2
    e_v = pot.v(f) 
    e_tot = e_dh + e_da + e_v

    return e_dh, e_da, e_v, e_tot


def energies(fields, lat, pot, weight=None):
    """
    Gradient, Potential, Magnetic, Total energies.
    Note factor of 1/2 in definition of energy in onsim
    """
    e_dh, e_da, e_v, e_tot = energy_densities(fields, lat, pot)

    if not weight is None:
        if weight == "pot":
            wt = e_v
        elif weight == "lag":
            wt = e_tot
        wt_sum = np.pi*np.trapz(wt, lat.x**2)
    else:
        wt = np.ones_like(e_tot)
        wt_sum = 1.
    
    # e_d_sum = np.sum( lat.dx*lat.x * e_d * wt)/wt_sum
    # e_v_sum = np.sum( lat.dx*lat.x * e_v * wt)/wt_sum
    e_dh_sum =  np.pi*np.trapz( e_dh * wt, lat.x**2)/wt_sum
    e_da_sum = np.pi*np.trapz( e_da * wt, lat.x**2)/wt_sum
    e_v_sum = np.pi*np.trapz( e_v * wt, lat.x**2)/wt_sum
    e_tot_sum = e_dh_sum + e_da_sum + e_v_sum

    return e_dh_sum, e_da_sum, e_v_sum, e_tot_sum, wt_sum


def initial_condition(lat, pot):
    """
    Initial guess
    """
    m2 = pot.v_prime_prime(pot.phi_b)
    k = np.sqrt(m2/2)

    phi_init = np.tanh(k*lat.x)

    return phi_init


def krylov_string(lam, lat_pars=(200,20), plot_fields=False, plot_residual=False):
    """
    Apply Krylov solver to find string solution, for 
    quartic potential (with phi_b = 1) and coupling lam
    """
        
    lat = lattice_1d(*lat_pars)
    pot = quartic_potential(lam)
    
    fields_init = initial_condition(lat, pot)
    
    def field_eqn_fix(fields):
        return field_eqn(fields, pot, lat)

    try:
        fields = newton_krylov(field_eqn_fix, fields_init)
    except scipy.optimize.nonlin.NoConvergence as e:
        fields = e.args[0]
        print('No Convergence')

    if plot_fields:
        ax = do_plot_fields(fields_init, lat, ls='--', labels=[r'Initial $f$','Initial $a$'])
        do_plot_fields(fields, lat, ax=ax)
        
    if plot_residual:
        residual = field_eqn_fix(fields)      
        do_plot_fields(residual, lat)

    return fields, lat, pot 


def string_energies(lam, lat_pars=(200,20), weight=None ):
    """
    Gradient, Potential, Magnetic, Total
    """
    fields, lat, pot = krylov_string(lam, lat_pars=lat_pars)
    return np.array(energies(fields, lat, pot, weight=weight))
    

def field_moments(fields, lat, pot):

    f = fields
    
    modPhi1_1 = np.pi*np.trapz(f-1,lat.x**2)
    modPhi2_1 = np.pi*np.trapz(f**2-1,lat.x**2)

    return modPhi1_1, modPhi2_1


def do_plot_fields(fields, lat, pot, ls=None, labels=None, ax=None):

    if ls is None:
        ls = '-'
    if labels is None:
        labels = [r'$f$', r'$e_{tot}$', r'$e_{Dh}$', r'$e_{Da}$', r'$e_{V}$']    
    if ax is None:
        fig, ax = plt.subplots()

    e_dh, e_da, e_v, e_tot = energy_densities(fields, lat, pot)

    m2 = pot.v_prime_prime(pot.phi_b)
    m = np.sqrt(m2)

    ax.plot(lat.x[1:]*m, fields[1:], color='b', ls=ls, label=labels[0] )
    ax.plot(lat.x[1:]*m, e_tot[1:], color='k', ls='-', label=labels[1] )
    ax.plot(lat.x[1:]*m, e_dh[1:], color='r', ls='-', label=labels[2] )
    ax.plot(lat.x[1:]*m, e_da[1:], color='m', ls='-', label=labels[3] )
    ax.plot(lat.x[1:]*m, e_v[1:], color='g', ls='-', label=labels[4] )
    ax.set_xlabel(r'$rm_h$')
    ax.set_ylabel('field')
    ax.set_xlim(0,lat.R*m)
    ax.legend()
    ax.grid(True)

    return ax
