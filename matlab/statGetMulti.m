% extract a stat from statsFile
% 2013 Version for comparing runs at different resolution
%
%Usage: stat = statGetMulti(statName,id,run,simpars,path,verbosity)
%
% path = cell array of paths to file, including final '/'
%        (if omitted gets paths from gpath global variable)

function [stat, t] = statGetMulti(statName,id,run,simpars,inPath,verbosity)

if nargin==0; 
  help statGet
  return
end

global gpath

if ~exist('verbosity','var'); verbosity='verbose'; end 

if ~exist('inPath','var'); inPath={}; end 

if numel(inPath)>0; 
  if ~iscell(inPath) 
      disp('inPath must be a cell array')
      return
  end
  path=inPath; 
else
  if numel(gpath)>0
  if ~iscell(gpath) 
      disp('gpath must be a cell array')
      return
  end
    path=gpath;
  else
    disp(['Please set gpath global variable to default path(s)'...
	  ' or specify path(s) in function call'])
    return
  end
end

% %%% simpars overrides
if isa(simpars,'containers.Map')
    if (strcmp(verbosity,'verbose'))
        disp('simpars is a container')
    end
    if isKey(simpars,'dx'); dx = simpars('dx'); end
    if isKey(simpars,'N'); N = simpars('N'); end
    if isKey(simpars,'lambda'); lambda = simpars('lambda'); end
    if isKey(simpars,'q'); q = simpars('q'); beta = 2.*lambda/q^2; end
    if isKey(simpars,'beta'); beta = simpars('beta'); end  % if specified directly
    if isKey(simpars,'sB'); sB = simpars('sB'); end
else
    if (strcmp(verbosity,'verbose'))
        disp('simpars is a vector')
    end
    if numel(simpars)>3
        sB = simpars(4);
    end
    if numel(simpars)>2
        beta = simpars(3);
    end
    if numel(simpars)>1
        N = simpars(2);
    end
    if numel(simpars)>0
        dx = simpars(1);
    end
end


% if (strcmp(verbosity,'verbose'))
%     disp(['Current working path: ' path])
% end

%======================
%Form num2str ID string
%======================
s=strfind(id,'%');
if numel(s) > 0
    ID=[id(1:s+1) '.' id(s+1) 'i' id(s+2:end)];
else
    ID = id;
end
    
    
%==============
%Loop over runs
%==============
for i=1:numel(path)

  %=========
  %Read file
  %=========
  if numel(ID) > 0
      file=[path{i} 'statsFile' num2str(run,ID) '.dat'];
  else
      file=[path{i} 'statsFile' ID '.dat'];
  end
  if (strcmp(verbosity,'verbose'))
      disp(' ')
      disp(['Loading file: ' file]); 
  end
  stats=load(file);

%  disp(['File size: ' num2str(size(stats))])
  
  sizes=size(stats);
  if (strcmp(verbosity,'verbose'))
      disp(['File size: ' num2str(sizes  )])
  end

  if ~exist('stat','var')
      stat = zeros(numel(path),sizes(1));
  end
  
  if sizes(2)==21
%====================
%New LAH files
%====================

      t=stats(:,1);
      a=stats(:,2);
      q=stats(:,3);
      modPhi=stats(:,4);
      modPi=stats(:,5);
      modFij=stats(:,6);
      modF0i=stats(:,7);
      gaussCD=stats(:,8);
      gaussDE=stats(:,9);
      eF0i=stats(:,10);
      eF0i_lag=stats(:,11);
      eFij=stats(:,12);
      eFij_lag=stats(:,13);
      ePi=stats(:,14);
      ePi_lag=stats(:,15);
      eDjPhi=stats(:,16);
      eDjPhi_lag=stats(:,17);
      eV=stats(:,18);
      eV_lag=stats(:,19);
      SLexp=stats(:,20);
      SLwind=stats(:,21); 
      
      
  elseif sizes(2)==16

  %====================
  %Extract data columns
  %====================
      t=stats(:,1);
      a=stats(:,2);
      q=stats(:,3);
      modPhi=stats(:,4);
      modPi=stats(:,5);
      modFij=stats(:,6);
      modF0i=stats(:,7);
      gaussCD=stats(:,8);
      gaussDE=stats(:,9);
      eF0i=stats(:,10);
      eFij=stats(:,11);
      ePi=stats(:,12);
      eDjPhi=stats(:,13);
      eV=stats(:,14);
      SLexp=stats(:,15);
      SLwind=stats(:,16);
  end
  
  %===============================
  %Perform Additional Calculations
  %===============================

  % Get data

    if sizes(2)==13 | sizes(2)==17
        e=ePi+eDjPhi+eV;    %Energy density
        L=ePi-eDjPhi-eV;    %Lag. density
        p=ePi - eDjPhi/3 - eV; % Pressure
    else
        e=eF0i+eFij+ePi+eDjPhi+eV;    %Energy density
        L=eF0i-eFij+ePi-eDjPhi-eV;    %Lag. density
        p=eF0i/3+eFij/3+ePi - eDjPhi/3 - eV; % Pressure
    end;

    % Calculate derived string parameters
    %
    rs = (a(end)*q(end)./(q.*a)); % string width relative to end
    beta = (a(end).*q(end)).^2/2; % Recall that q is now sqrt(lambda), and assume that gauge coupling = 1.

    if ( sizes(2)==21 || sizes(2)==16 )
        mu = 2*pi*beta^0.195;    %Abelian Higgs continuum String energy per unit length Hill Hodges Turner 88
        mu_lag = 1.12*2*pi*beta^0.7./rs.^2;  %AH from semloc.f (rough, only good at beta=1). 
                                  %Also measured in 3D at dx = 0.125, beta = 1; mu = 7.0234
    else % sizes(2) == 16
        mu = pi;    %for global defects, real fields.Definition of mu (needs checking)
        %     mu_lag = 0.84*pi./rs.^2; % Lag-weighted, Calculated with relaxation code global_string.f
        mu_lag = 0.284*pi./rs.^2; % V-weighted, Calculated with relaxation code global_string.f
    end

    % Get more data
    if sizes(2)~=13 && sizes(2)~=17
        T0iEst_gauge = sqrt(eF0i.*eFij); % Estimate of T0i gauge
        T0iEst_scalar = sqrt(ePi.*eDjPhi); % Estimate of T0i scalar
        T0iEst = sqrt(eF0i.*eFij) + sqrt(ePi.*eDjPhi); % Estimate of T0i
    end;

    if sizes(2)==13 || sizes(2)==17
        T0iEst_scalar = sqrt(ePi.*eDjPhi); % Estimate of T0i scalar
    end;
  
    if sizes(2)==21

      e_lag=eF0i_lag+eFij_lag+ePi_lag+eDjPhi_lag+eV_lag;    %Energy density
      L_lag=eF0i_lag-eFij_lag+ePi_lag-eDjPhi_lag-eV_lag;    %Lag. density
      p_lag=eF0i_lag/3+eFij_lag/3+ePi_lag - eDjPhi_lag/3 - eV_lag; % Pressure

      T0iEst_gauge_lag = sqrt(eF0i_lag.*eFij_lag); % Estimate of T0i gauge lag weighted
      T0iEst_scalar_lag = sqrt(ePi_lag.*eDjPhi_lag); % Estimate of T0i lag wighted
      T0iEst_lag = sqrt(eF0i_lag.*eFij_lag+ePi_lag.*eDjPhi_lag); % Estimate of T0i lag wighted
  
    end

    if sizes(2) == 13 || sizes(2)==17
        e_lag=(ePi_lag+eDjPhi_lag+eV_lag);    %Energy density
        L_lag=(ePi_lag-eDjPhi_lag-eV_lag);    %Lag. density
        p_lag=(ePi_lag - eDjPhi_lag/3 - eV_lag); % Pressure

        T0iEst_scalar_lag = sqrt(ePi_lag.*eDjPhi_lag); % Estimate of T0i lag wighted
    end

    if sizes(2)==21
        velGauge = (eF0i_lag) ./ (eFij_lag);
        Rgrad = (ePi_lag)./(eDjPhi_lag); %201509 correction
        velGrad = 2*Rgrad./(1 + Rgrad);  %201509 correction
        velEOS = 0.5*(1 + 3*p_lag./e_lag);
        xiInv = sqrt(-mu*(1-velEOS)./L);
        xiInvLag = sqrt(-mu*(1-velEOS)./L_lag);
    end

    if sizes(2)==13 || sizes(2)==17
        Rgrad = (ePi_lag)./(eDjPhi_lag); %201509 correction
        velGrad = 2*Rgrad./(1 + Rgrad);  %201509 correction
        velEOS = 0.5*(1 + 3*p_lag./e_lag);
        xiInv = sqrt(-mu*(1-velEOS)./L);
        xiInvLag = sqrt(-mu*(1-velEOS)./L_lag);
    end;

    velLag = 1. + L./e;
    velLag_lag = 1. + L_lag./e_lag;
    xi_e_lag = 1./sqrt(-e_lag./mu_lag);
    xiWind = 1./sqrt(SLwind/(N*dx)^3);
    
switch statName
    case 'modPhi'
        stat(i,:)=stats(:,4);
    case 'modPi'
        stat(i,:)=stats(:,5);
    case 'modFij'
        stat(i,:)=stats(:,6);
    case 'modF0i'
        stat(i,:)=stats(:,7);
    case 'gaussCD'
        stat(i,:)=stats(:,8);
    case 'gaussDE'
        stat(i,:)=stats(:,9);
    case 'eF0i'
        stat(i,:)=stats(:,10);
    case 'eF0i_lag'
        stat(i,:)=stats(:,11);
    case 'eFij'
        stat(i,:)=stats(:,12);
    case 'eFij_lag'
        stat(i,:)=stats(:,13);
    case 'ePi'
        stat(i,:)=stats(:,14);
    case 'ePi_lag'
        stat(i,:)=stats(:,15);
    case 'eDjPhi'
        stat(i,:)=stats(:,16);
    case 'eDjPhi_lag'
        stat(i,:)=stats(:,17);
    case 'eV'
        stat(i,:)=stats(:,18);
    case 'eV_lag'
        stat(i,:)=stats(:,19);
    case 'SLexp'
        stat(i,:)=stats(:,20);
    case 'SLwind'
        stat(i,:)=stats(:,21); 
    case 'e'
        stat(i,:)=e; 
    case 'p'
        stat(i,:)=p; 
    case 'L'
        stat(i,:)=L; 
    case 'xiLag'
        stat(i,:)=xiLag; 
    case 'e_lag'
        stat(i,:)=e_lag; 
    case 'p_lag'
        stat(i,:)=p_lag; 
    case 'L_lag'
        stat(i,:)=L_lag; 
    case 'xiWind'
        stat(i,:)=sqrt(1./SLwind); 
    case 'velGauge'
        stat(i,:) = velGauge;
    case 'velGrad'
        stat(i,:) = velGrad;
    case 'velEOS'
        stat(i,:) = velEOS;
    case 'velLag'
        stat(i,:) = velLag;
    case 'velLag_lag'
        stat(i,:) = velLag_lag;
    case 'xi_e_lag'
        stat(i,:) = xi_e_lag; 
    case 'xiInv'
        stat(i,:) = xiInv;
    case 'xiInvLag'
        stat(i,:) = xiInvLag;
    otherwise
        disp('Unknown stat')
end

end