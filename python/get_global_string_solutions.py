#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 28 10:55:32 2021

@author: hindmars
"""

import numpy as np
import matplotlib.pyplot as plt

import global_string_sol as ss

R = 5
N = 10000

lam = 2
# Get potential-weighted Abelian Higgs string energies at lam = 1, complexx scalar vev 1)
fields, lat, pot = ss.krylov_string(lam, lat_pars=(N,R))

e_arr = ss.energies(fields, lat, pot)
e_wt_arr = ss.energies(fields, lat, pot, weight="pot")

print("Max radius = ", R, " number of points = ", N)

print("Gradient energy higgs:                 ", e_arr[0])
print("Gradient energy axion: (log-divergent) ", e_arr[1])
print("Potential energy:                      ", e_arr[2])
print("Higgs energy:                          ", e_arr[0] + e_arr[2])
print("Total energy: (log-divergent)          ", e_arr[3])
print("Potential fraction f_V =               ", e_arr[2]/e_arr[3])
print("Higgs fraction f_Dh + f_V =            ", (e_arr[0] + e_arr[2])/e_arr[3])
print()
print("Potential-wtd Gradient energy higgs: ", e_wt_arr[0])
print("Potential-wtd Gradient energy axion: ", e_wt_arr[1])
print("Potential-weighted potential energy: ", e_wt_arr[2])
print("Potential-weighted Total energy:     ", e_wt_arr[3])
print("Weight sum:                          ", e_wt_arr[4])
print("Potential  fraction f_V_wt =         ", e_wt_arr[2]/e_wt_arr[3])
print("Higgs  fraction f_Dh_wt + f_V_wt =   ", (e_wt_arr[0] + e_wt_arr[2])/e_wt_arr[3])
print()
print("Unnormalised weighted total energy : ", e_wt_arr[4]*e_wt_arr[3])
print()
print("Quantities for defect_analyse.defect_solution_pars():")
print("mu defined as Higgs Total energy:                       ", (e_arr[0] + e_arr[2])/2)
print("mup equivalent to Potential-weighted Total energy:      ", e_wt_arr[3]/2)
print("mup_V equivalent to Potential-weighted potential energy:", e_wt_arr[2]/2)
print("mu_wt equivalent to un-normalised weighted total energy:", e_wt_arr[4]*e_wt_arr[3]/2)
print("Potential fraction f_V_wt:                              ", e_wt_arr[2]/e_wt_arr[3])
print()


      
# Plot the scalar and azimuthal gauge field magnitudes with dashed lines
ax = ss.do_plot_fields(fields, lat, pot, ls='--')
ax.set_title(r"Global string, $\lambda = {}$".format(lam))

mom_arr = ss.field_moments(fields, lat, pot)
print("Field moments $\int d^2x(|\phi| - 1)$, (log-divergent)    ", mom_arr[0])
print("Field moments $\int d^2 x(|\phi|^2 - 1)$, (log-divergent) ", mom_arr[1])
